package beer.hapibrew.beerxml2proto;

public class NonFatalException extends Exception {
    public NonFatalException(String message) {
        super(message);
    }

    public NonFatalException(String message, Throwable cause) {
        super(message, cause);
    }

    public NonFatalException(Throwable cause) {
        super(cause);
    }
}
