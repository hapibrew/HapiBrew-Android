package beer.hapibrew.app.data

data class KegTempData(val index:Int, val temperature:String)