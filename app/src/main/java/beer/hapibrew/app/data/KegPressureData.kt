package beer.hapibrew.app.data

data class KegPressureData(val index:Int, val psi:String, val bar:String)