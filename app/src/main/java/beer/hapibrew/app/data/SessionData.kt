package beer.hapibrew.app.data

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import beer.hapibrew.app.R
import beer.hapibrew.app.dialog.StepDurationDialog
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.view.CountDownTextView
import beer.hapibrew.beerxml2proto.proto.Fermentation
import beer.hapibrew.beerxml2proto.proto.Mash
import beer.hapibrew.beerxml2proto.proto.MashStep
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.squareup.wire.Wire
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

data class SessionData(val id:Long,
                       var name:String,
                       val batch:Int,
                       val mashWater:Liters,
                       val spargeWater:Liters,
                       val startEpoch:Long,
                       var endEpoch:Long,
                       var og:Sg,
                       var fg:Sg,
                       var phase: Phase,
                       var step:Int,
                       var stepStartEpoch:Long,
                       var stepPauseEpoch:Long,
                       var stepEndEpoch:Long,
                       val recipe: Recipe,
                       var notes:String) : Parcelable {

    enum class Phase {
        PRE_MASH,
        MASH,
        SPARGE,
        BOIL,
        PRE_FERMENTATION,
        FERMENTATION,
        COMPLETED;

//        companion object {
//            private val VALS = values()
//        }
//        val next: Phase get() = VALS[ordinal+1]
//        val previous: Phase get() = VALS[ordinal-1]
    }

    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readInt(),
            source.readDouble(),
            source.readDouble(),
            source.readLong(),
            source.readLong(),
            source.readDouble(),
            source.readDouble(),
            Phase.valueOf(source.readString()),
            source.readInt(),
            source.readLong(),
            source.readLong(),
            source.readLong(),
            source.readParcelable(Recipe::class.java.classLoader),
            source.readString()
    )

    fun setTimer(context: Context, isStepCompleted:Boolean, countDownTextView: CountDownTextView, button: Button?, onTick:(() -> Unit)?) {
        if (isStepCompleted) {
            countDownTextView.text = context.getString(R.string.session_step_time_done)
            countDownTextView.stop()
            countDownTextView.setOnClickListener(null)
        } else {
            if (stepStartEpoch == 0L) {
                countDownTextView.showTime(getStepDuration(), phase)
            } else {
                if (stepPauseEpoch != 0L) {
                    countDownTextView.showTime(stepEndEpoch - stepPauseEpoch, phase)
                } else {
                    startTimer(stepEndEpoch - currentEpoch(), countDownTextView, button, onTick = onTick)
                }
            }
        }
    }

    fun setStepDurationCallback(activity:AppCompatActivity, countDownTextView: CountDownTextView, callback: StepDurationDialog.OnStepDurationDialogListener) {
        countDownTextView.setOnClickListener {
            val ft = activity.supportFragmentManager.beginTransaction()
            val prev = activity.supportFragmentManager.findFragmentByTag("StepDurationDialog")

            if (prev != null && StepDurationDialog::class.java.isInstance(prev)) {
                ft.remove(prev)
            }

            val f:StepDurationDialog = if (phase == SessionData.Phase.FERMENTATION)
                StepDurationDialog.newInstance(countDownTextView.getRemainingDays(), true)
            else
                StepDurationDialog.newInstance(countDownTextView.getRemainingMinutes(), false)
            f.setCallback(callback)
            f.show(ft, "StepDurationDialog")
        }
    }

    fun startTimer(duration:Long, countDownTextView: CountDownTextView, button: Button?, onTick:(() -> Unit)?) {
        countDownTextView.start(duration, forceMinSec = (phase != SessionData.Phase.FERMENTATION), onFinish={
            button?.isEnabled = false
        }, onTick = onTick)
    }

    fun getStepDuration(duration:Long = -1L):Long {
        if (phase == SessionData.Phase.MASH) {
//        return 15*1000
            if (duration != -1L) {
                return duration * 60 * 1000L
            }
            recipe.mash?.let {
                val mashStep = it.steps[step]
                return Wire.get(mashStep.step_time, MashStep.DEFAULT_STEP_TIME)!!.toLong() * 60 * 1000L
            }
        } else if (phase == SessionData.Phase.BOIL) {
//        return 15*1000
            if (duration != -1L) {
                return duration * 60 * 1000L
            }
            return Wire.get(recipe.boil_time, Recipe.DEFAULT_BOIL_TIME)!! * 60 * 1000L
        } else if (phase == SessionData.Phase.FERMENTATION) {
//        return 15*1000
            if (duration != -1L) {
                return duration * 24 * 60 * 60 * 1000L
            }
            val fermentation = recipe.fermentations[step]
            return Wire.get(fermentation.days, Fermentation.DEFAULT_DAYS)!!.toLong() * 24 * 60 * 60 * 1000L
        }

        return -1L
    }

    fun getSessionDurationString(context: Context):String {
        return context.getString(R.string.session_step_completed_subtitle,
                getDateAsString(startEpoch), getDateAsString(endEpoch))
    }
    //    val dateFormatter = DateTimeFormatterBuilder()
//            .appendDayOfMonth(1)
//            .appendMonthOfYearText()
//            .appendLiteral(' ')
//            .appendYear(4, 4)
//            .toFormatter()
    private val dateFormatter: DateTimeFormatter = DateTimeFormat.mediumDate()
    private fun getDateAsString(epoch:Long):String {
        if (epoch == 0L)
            return ""
        return dateFormatter.print(epoch)
    }

    fun getAlarmTag():String {
        return "SESSION#$id"
    }

    fun shouldSetAlarm():Boolean {
        return stepEndEpoch > 0L && stepPauseEpoch == 0L
    }

    fun buildRecipeStep(ctx: Context, phase:SessionData.Phase = this.phase, index:Int = this.step):RecipeStep {
        return when (phase) {
            SessionData.Phase.PRE_MASH -> {
                RecipeStep(
                        ctx.getString(R.string.session_step_premash_title),
                        ctx.getString(R.string.session_step_premash_subtitle, recipe.mash!!.steps[0].displayInfuseTemperature(ctx, true)),
                        SessionData.Phase.PRE_MASH, 0)
            }
            SessionData.Phase.MASH -> {
                val mashStep = recipe.mash!!.steps[index]
                RecipeStep(
                        ctx.getString(R.string.session_step_mash_title, Wire.get(mashStep.name, MashStep.DEFAULT_NAME)!!),
                        ctx.getString(R.string.session_step_mash_subtitle, mashStep.displayTemperature(ctx, true), mashStep.displayTime(ctx, true),
                                displayVolume(ctx, mashWater, true)),
                        SessionData.Phase.MASH, index
                )
            }
            SessionData.Phase.SPARGE -> {
                RecipeStep(ctx.getString(R.string.session_step_sparge_title),
                        ctx.getString(R.string.session_step_sparge_subtitle, recipe.mash!!.displaySpargeTemperature(ctx), displayVolume(ctx, spargeWater, true)),
                        SessionData.Phase.SPARGE, 0)
            }
            SessionData.Phase.BOIL -> {
                RecipeStep(ctx.getString(R.string.session_step_boil_title),
                        ctx.getString(R.string.session_step_boil_subtitle, recipe.displayBoilTime(ctx)),
                        SessionData.Phase.BOIL, 0)
            }
            SessionData.Phase.PRE_FERMENTATION -> {
                RecipeStep(ctx.getString(R.string.session_step_preferment_title),
                        ctx.getString(R.string.session_step_preferment_subtitle),
                        SessionData.Phase.PRE_FERMENTATION, 0)
            }
            SessionData.Phase.FERMENTATION -> {
                val fermentation = recipe.fermentations[index]
                RecipeStep(
                        ctx.getString(R.string.session_step_ferment_title, fermentation.displayName(ctx)),
                        ctx.getString(R.string.session_step_ferment_subtitle, fermentation.displayTime(ctx, true), fermentation.displayTemperature(ctx, true)),
                        SessionData.Phase.FERMENTATION, index)
            }
            SessionData.Phase.COMPLETED -> {
                RecipeStep(
                        ctx.getString(R.string.session_step_completed_title),
                        getSessionDurationString(ctx),
                        SessionData.Phase.COMPLETED, 0)
            }
        }
    }

    fun buildRecipeSteps(ctx: Context):List<RecipeStep> {
        val recipeSteps = ArrayList<RecipeStep>()

        recipe.mash?.let { mash: Mash ->
            recipeSteps.add(buildRecipeStep(ctx, SessionData.Phase.PRE_MASH, 0))

            mash.steps.indices.mapTo(recipeSteps) { buildRecipeStep(ctx, SessionData.Phase.MASH, it) }

            recipeSteps.add(buildRecipeStep(ctx, SessionData.Phase.SPARGE, 0))
        }

        recipeSteps.add(buildRecipeStep(ctx, SessionData.Phase.BOIL, 0))

        recipeSteps.add(buildRecipeStep(ctx, SessionData.Phase.PRE_FERMENTATION, 0))

        recipe.fermentations.indices.mapTo(recipeSteps) { buildRecipeStep(ctx, SessionData.Phase.FERMENTATION, it) }

        recipeSteps.add(buildRecipeStep(ctx, SessionData.Phase.COMPLETED, 0))

        return recipeSteps
    }

    fun getStepProgress():Double {
        if (stepStartEpoch == 0L)
            return 0.0

        val progressTime = if (stepPauseEpoch == 0L) currentEpoch() else stepPauseEpoch
        val progress = (progressTime - stepStartEpoch).toDouble() / (stepEndEpoch - stepStartEpoch).toDouble()
        return minOf(maxOf(progress, 0.0), 1.0)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.let { it::class.java } != this::class.java) return false

        other as RecipeData

        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
        dest.writeInt(batch)
        dest.writeDouble(mashWater)
        dest.writeDouble(spargeWater)
        dest.writeLong(startEpoch)
        dest.writeLong(endEpoch)
        dest.writeDouble(og)
        dest.writeDouble(fg)
        dest.writeString(phase.name)
        dest.writeInt(step)
        dest.writeLong(stepStartEpoch)
        dest.writeLong(stepPauseEpoch)
        dest.writeLong(stepEndEpoch)
        dest.writeParcelable(recipe, flags)
        dest.writeString(notes)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SessionData> = object : Parcelable.Creator<SessionData> {
            override fun createFromParcel(source: Parcel): SessionData = SessionData(source)
            override fun newArray(size: Int): Array<SessionData?> = arrayOfNulls(size)
        }
    }
}