package beer.hapibrew.app.data

import android.os.Parcel
import android.os.Parcelable
import beer.hapibrew.app.calculators.calculateIbu
import beer.hapibrew.app.calculators.calculateRecipeAbv
import beer.hapibrew.app.calculators.calculateSrm
import beer.hapibrew.app.extensions.lToGal
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.squareup.wire.Wire
import java.util.*

data class RecipeData(val id:Long,
                      val name: String,
                      var og: Double,
                      var fg: Double,
                      var srm: Double,
                      var abv: Double,
                      var ibu: Double,
                      val sessions:Int,
                      val logs:Int,
                      var proto: Recipe) : Parcelable
{
    constructor(index:Int, recipe: Recipe) : this(
            index.toLong(),
            Wire.get(recipe.name, Recipe.DEFAULT_NAME)!!,
            Wire.get(recipe.est_og, Recipe.DEFAULT_EST_OG)!!,
            Wire.get(recipe.est_fg, Recipe.DEFAULT_EST_FG)!!,
            Wire.get(recipe.est_srm, Recipe.DEFAULT_EST_SRM)!!,
            Wire.get(recipe.est_abv, Recipe.DEFAULT_EST_ABV)!!,
            Wire.get(recipe.est_ibu, Recipe.DEFAULT_EST_IBU)!!,
            0, 0,
            recipe) {

        if (og == Recipe.DEFAULT_EST_OG || fg == Recipe.DEFAULT_EST_FG || abv == Recipe.DEFAULT_EST_ABV) {
            val data = calculateRecipeAbv(
                    Wire.get(recipe.batch_size, Recipe.DEFAULT_BATCH_SIZE)!!.lToGal(),
                    Wire.get(recipe.efficiency, Recipe.DEFAULT_EFFICIENCY)!!,
                    Wire.get(recipe.fermentables, emptyList()),
                    Wire.get(recipe.yeasts, emptyList()))

            og = data.og
            fg = data.fg
            abv = data.abv
        }

        if (srm == Recipe.DEFAULT_EST_SRM) {
            srm = calculateSrm(
                    Wire.get(recipe.batch_size, Recipe.DEFAULT_BATCH_SIZE)!!.lToGal(),
                    Wire.get(recipe.fermentables, emptyList()))
        }

        if (ibu == Recipe.DEFAULT_EST_IBU) {
            ibu = calculateIbu(
                    Wire.get(recipe.batch_size, Recipe.DEFAULT_BATCH_SIZE)!!.lToGal(),
                    Wire.get(recipe.boil_size, Recipe.DEFAULT_BOIL_SIZE)!!.lToGal(),
                    og,
                    Wire.get(recipe.hops, emptyList()))
        }
    }

    fun getSessionName():String {
        return "%s (%d)".format(Locale.US, name, sessions)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<RecipeData> = object : Parcelable.Creator<RecipeData> {
            override fun createFromParcel(source: Parcel): RecipeData = RecipeData(source)
            override fun newArray(size: Int): Array<RecipeData?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readDouble(),
            source.readDouble(),
            source.readDouble(),
            source.readDouble(),
            source.readDouble(),
            source.readInt(),
            source.readInt(),
            source.readParcelable(Recipe::class.java.classLoader)

    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.let { it::class.java } != this::class.java) return false

        other as RecipeData

        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(id)
        dest?.writeString(name)
        dest?.writeDouble(og)
        dest?.writeDouble(fg)
        dest?.writeDouble(srm)
        dest?.writeDouble(abv)
        dest?.writeDouble(ibu)
        dest?.writeInt(sessions)
        dest?.writeInt(logs)
        dest?.writeParcelable(proto, flags)
    }
}