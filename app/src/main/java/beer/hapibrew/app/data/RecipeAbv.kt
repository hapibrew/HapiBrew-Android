package beer.hapibrew.app.data

data class RecipeAbv(val og:Double, val fg:Double, val abv:Double)