package beer.hapibrew.app.data

data class RecipeStep(val title:String, val subtitle:String, val phase:SessionData.Phase, val step:Int)