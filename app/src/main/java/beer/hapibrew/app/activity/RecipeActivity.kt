package beer.hapibrew.app.activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.lovibondToHex
import beer.hapibrew.app.controllers.recipe.RecipeController
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.extensions.BUNDLE_HIDE_FAB
import beer.hapibrew.app.extensions.BUNDLE_RECIPE_DATA
import beer.hapibrew.app.extensions.getDarkVersion
import beer.hapibrew.app.extensions.getLightVersion
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.activity_recipe.*


class RecipeActivity : AppCompatActivity() {

    var router: Router? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val recipeData = intent.getParcelableExtra<RecipeData>(BUNDLE_RECIPE_DATA)

        toolbar_layout.title = recipeData.name
        val color = Color.parseColor(lovibondToHex(recipeData.srm))
        if (Build.VERSION.SDK_INT >= 21) {
            window.statusBarColor = getDarkVersion(color)
        }
        toolbar_layout.setBackgroundColor(color)
        toolbar_layout.setStatusBarScrimColor(color)
        toolbar_layout.setContentScrimColor(color)

        if (intent.getBooleanExtra(BUNDLE_HIDE_FAB, false)) {
            fab.visibility = View.GONE
        } else {
            fab.setOnClickListener {
                recipeData?.let {
                    val intent = Intent(this@RecipeActivity, NewSessionActivity::class.java)
                    intent.putExtra(BUNDLE_RECIPE_DATA, it)
                    startActivity(intent)
                    finish()
                }
            }
            fab.visibility = View.VISIBLE
        }

        bubblesView.bubbleColor = getLightVersion(color)
        bubblesView.post {
            bubblesView.start()
        }

        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!(router?.hasRootController() ?: false)) {
            router?.setRoot(RouterTransaction.with(RecipeController(recipeData)))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0
        if (id == android.R.id.home) {
            if (!(router?.handleBack() ?: false)) {
                super.onBackPressed()
            } else {
                NavUtils.navigateUpFromSameTask(this)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!(router?.handleBack() ?: false)) {
            super.onBackPressed()
        }
    }
}
