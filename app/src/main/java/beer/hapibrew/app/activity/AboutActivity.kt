package beer.hapibrew.app.activity

import android.content.Context
import android.content.pm.PackageManager
import beer.hapibrew.app.R
import com.danielstone.materialaboutlibrary.MaterialAboutActivity
import com.danielstone.materialaboutlibrary.model.MaterialAboutActionItem
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard
import com.danielstone.materialaboutlibrary.model.MaterialAboutList
import com.danielstone.materialaboutlibrary.model.MaterialAboutTitleItem


class AboutActivity : MaterialAboutActivity() {

    override fun getActivityTitle(): CharSequence {
        return getString(R.string.about)
    }

    override fun getMaterialAboutList(c: Context?): MaterialAboutList {
        val appCardBuilder = MaterialAboutCard.Builder()

        appCardBuilder.addItem(MaterialAboutTitleItem.Builder()
                .text(R.string.app_name)
                .icon(R.mipmap.ic_launcher)
                .build())

        try {
            val pInfo = c?.packageManager?.getPackageInfo(c.packageName, 0)
            val versionName = pInfo?.versionName ?: ""
            appCardBuilder.addItem(
//                    ConvenienceBuilder.createVersionActionItem(c,
//                    ContextCompat.getDrawable(c, R.drawable.ic_info),
//                    getString(R.string.version),
//                    false)
                    MaterialAboutActionItem.Builder()
                            .text(getString(R.string.version))
                            .subText(versionName)
                            .icon(R.drawable.ic_info)
                            .build()
            )

        } catch (e: PackageManager.NameNotFoundException) {
        }

        val authorCardBuilder = MaterialAboutCard.Builder()
        authorCardBuilder.title(R.string.author)

        authorCardBuilder.addItem(MaterialAboutActionItem.Builder()
                .text("Zoran Smilevski")
                .subText("Slovenia")
                .icon(R.drawable.ic_user)
                .build())

        return MaterialAboutList(appCardBuilder.build(), authorCardBuilder.build())
    }

}