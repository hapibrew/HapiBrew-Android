package beer.hapibrew.app.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.view.MenuItem
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.FermentationItem
import beer.hapibrew.app.adapter.items.recipe.FermentationItemHeader
import beer.hapibrew.app.adapter.items.recipe.MashStepItem
import beer.hapibrew.app.adapter.items.recipe.MashStepItemHeader
import beer.hapibrew.app.calculators.calculateMashWater
import beer.hapibrew.app.calculators.calculateSpargeWater
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.database.RecipeDatabase
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.view.NestedLinearLayoutManager
import beer.hapibrew.beerxml2proto.proto.Fermentation
import beer.hapibrew.beerxml2proto.proto.Mash
import beer.hapibrew.beerxml2proto.proto.MashStep
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.DividerItemDecoration
import kotlinx.android.synthetic.main.activity_new_session.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.sdk25.coroutines.onClick

class NewSessionActivity : AppCompatActivity() {
    var recipeData: RecipeData? = null
    var mashStepsAdapter: FlexibleAdapter<MashStepItem>? = null
    var fermentationAdapter: FlexibleAdapter<FermentationItem>? = null

    var mashWaterAmount: Double = 0.0
    var spargeWaterAmount: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_session)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recipeData = intent.getParcelableExtra<RecipeData>(BUNDLE_RECIPE_DATA)
        if (recipeData == null) {
            finish()
            return
        }

        title = recipeData?.name

        recipeData?.let {
            fixProto(it)
            batchVolume.text = it.proto.displayBatchVolume(ctx)
            calculateWaterAmount(it)
            showFermentations(Wire.get(it.proto.fermentations, emptyList()))
            showMash(it.proto.mash!!)
        }

        startSession.onClick {
            val intent = Intent(this@NewSessionActivity, SessionActivity::class.java)
            intent.putExtra(BUNDLE_SESSION_ID, createNewSessionData())
            startActivity(intent)
            finish()
        }
    }

    private fun createNewSessionData():Long {
        RecipeDatabase.increaseSessions(ctx, recipeData!!.id)
        val newRecipe = RecipeDatabase.getRecipe(ctx, recipeData!!.id) ?: recipeData!!

        val sessionData = SessionData(-1, newRecipe.getSessionName(), newRecipe.sessions,
                mashWaterAmount, spargeWaterAmount, currentEpoch(), 0, 0.0, 0.0,
                SessionData.Phase.PRE_MASH, 0, 0, 0, 0, recipeData!!.proto, "") // proto from old recipeData is fixed

        return SessionDatabase.saveNewSession(ctx, sessionData)
    }

    private fun fixProto(recipeData: RecipeData) {

        val recipeBuilder = recipeData.proto.newBuilder()

        var anyChange:Boolean = false

        val mash = Wire.get(recipeData.proto.mash, null)
        val mashBuilder: Mash.Builder = if (mash == null) Mash.Builder() else mash.newBuilder()

        val spargeTemp = if (mash == null) null else Wire.get(mash.sparge_temp, null)
        if (spargeTemp == null) {
            mashBuilder.sparge_temp(75.0)
            anyChange = true
        }

        val mashSteps = if (mash == null) null else Wire.get(mash.steps, null)
        if (mashSteps == null || mashSteps.isEmpty()) {
            mashBuilder.steps(buildDefaultMashSteps()).build()
            anyChange = true
        }
        if (anyChange)
            recipeBuilder.mash(mashBuilder.build())

        val fermentations = Wire.get(recipeData.proto.fermentations, emptyList())
        if (fermentations.isEmpty()) {
            recipeBuilder.fermentations(buildDefaultFermentationSteps())
            anyChange = true
        }

        val og = Wire.get(recipeData.proto.est_og, 0.0)
        if (og == 0.0) {
            recipeBuilder.est_og(recipeData.og)
            anyChange = true
        }
        val fg = Wire.get(recipeData.proto.est_fg, 0.0)
        if (fg == 0.0) {
            recipeBuilder.est_fg(recipeData.fg)
            anyChange = true
        }

        if (anyChange)
            recipeData.proto = recipeBuilder.build()
    }

    private fun buildDefaultMashSteps(): List<MashStep> {
        return listOf(
                MashStep.Builder()
                        .name("Mash In")
                        .step_temp(66.0)
                        .step_time(60.0)
                        .build(),
                MashStep.Builder()
                        .name("Mash Out")
                        .step_temp(75.0)
                        .step_time(10.0)
                        .build()
        )
    }

    private fun buildDefaultFermentationSteps(): List<Fermentation> {
        return listOf(
                Fermentation.Builder()
                        .step(1)
                        .temperature(20.0)
                        .days(14.0)
                        .is_aging(false)
                        .build()
        )
    }

    private fun calculateWaterAmount(recipeData: RecipeData) {
        mashWaterAmount = calculateMashWater(Wire.get(recipeData.proto.fermentables, emptyList()))

        spargeWaterAmount = calculateSpargeWater(Wire.get(recipeData.proto.batch_size, Recipe.DEFAULT_BATCH_SIZE)!!,
                Wire.get(recipeData.proto.boil_time, Recipe.DEFAULT_BOIL_TIME)!!,
                mashWaterAmount, Wire.get(recipeData.proto.fermentables, emptyList()))

        mashWater.text = displayVolume(this@NewSessionActivity, mashWaterAmount, true, forceLarge = true)
        spargeWater.text = displayVolume(this@NewSessionActivity, spargeWaterAmount, true, forceLarge = true)
    }

    private fun showMash(mash: Mash) {
        val mashSteps = Wire.get(mash.steps, emptyList())
        mashProfileSpargeTemp.text = mash.displaySpargeTemperature(ctx)
        mashRecyclerView.itemAnimator = DefaultItemAnimator()
        mashRecyclerView.layoutManager = NestedLinearLayoutManager(ctx)
        mashRecyclerView.addItemDecoration(DividerItemDecoration(ctx, 0, 16))
        val header = MashStepItemHeader()
        mashStepsAdapter = FlexibleAdapter<MashStepItem>(mashSteps.map {
            MashStepItem(it, header)
        })
        mashStepsAdapter?.setDisplayHeadersAtStartUp(true)
        mashRecyclerView.adapter = mashStepsAdapter
    }


    private fun showFermentations(fermentations: List<Fermentation>) {
        fermentationsRecyclerView.itemAnimator = DefaultItemAnimator()
        fermentationsRecyclerView.layoutManager = NestedLinearLayoutManager(ctx)
        fermentationsRecyclerView.addItemDecoration(DividerItemDecoration(ctx, 0, 16))
        val header = FermentationItemHeader()
        fermentationAdapter = FlexibleAdapter<FermentationItem>(fermentations.map {
            FermentationItem(it, header)
        })
        fermentationAdapter?.setDisplayHeadersAtStartUp(true)
        fermentationsRecyclerView.adapter = fermentationAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mashStepsAdapter?.onSaveInstanceState(outState)
        fermentationAdapter?.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        mashStepsAdapter?.onRestoreInstanceState(savedInstanceState)
        fermentationAdapter?.onRestoreInstanceState(savedInstanceState)
    }
}
