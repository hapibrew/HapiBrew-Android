package beer.hapibrew.app.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import beer.hapibrew.app.BuildConfig
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.MainBaseController
import beer.hapibrew.app.controllers.main.RecipesController
import beer.hapibrew.app.controllers.main.SessionControllers
import beer.hapibrew.app.controllers.main.ToolsController
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.database.RecipeDatabase
import beer.hapibrew.app.prefs
import beer.hapibrew.beerxml2proto.BeerXmlParser
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.itemsSequence


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener{

    var router: Router? = null

    val sessionControllers: SessionControllers
        get() = SessionControllers()
    val recipesController: RecipesController
        get() = RecipesController()
    val toolsController: ToolsController
        get() = ToolsController()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        fab.setOnClickListener {
            startActivity(Intent(this@MainActivity, ImportActivity::class.java))
        }

        if (!prefs.hasInitialImport) {
            importBrewdogRecipes()
            prefs.hasInitialImport = true
        }

        setupBottomNavigation()

        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!(router?.hasRootController() ?: false)) {
            selectMenuItem(0)
            bottomNavigation.setCurrentItem(0, false)
        }
    }

    private fun setupBottomNavigation() {
        val session = AHBottomNavigationItem(getString(R.string.sessions), R.drawable.ic_sessions)
        val recipes = AHBottomNavigationItem(getString(R.string.recipes), R.drawable.ic_recipes)
        val tools = AHBottomNavigationItem(getString(R.string.tools), R.drawable.ic_tools)

        bottomNavigation.addItem(session)
        bottomNavigation.addItem(recipes)
        bottomNavigation.addItem(tools)

        bottomNavigation.accentColor = ContextCompat.getColor(ctx, R.color.tab_selected_color)
        bottomNavigation.inactiveColor = ContextCompat.getColor(ctx, R.color.textColorSecondary)

        bottomNavigation.defaultBackgroundColor = ContextCompat.getColor(ctx, R.color.white)

        bottomNavigation.isBehaviorTranslationEnabled = false
        bottomNavigation.manageFloatingActionButtonBehavior(fab)

        bottomNavigation.setOnTabSelectedListener(AHBottomNavigation.OnTabSelectedListener { position, wasSelected ->
            if (wasSelected)
                return@OnTabSelectedListener true

            if (position != AHBottomNavigation.CURRENT_ITEM_NONE)
                selectMenuItem(position)

            true
        })
    }

    private fun selectMenuItem(position:Int) {
        deselectDrawerNavigationItem()
        when (position) {
            0 -> switchController(sessionControllers)
            1 -> switchController(recipesController)
            2 -> switchController(toolsController)
        }
    }

    fun switchController(controller: MainBaseController) {
        router?.setRoot(RouterTransaction
                .with(controller)
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler())
        )
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (!(router?.handleBack() ?: false)) {
                super.onBackPressed()
            }
        }
    }

    private fun deselectDrawerNavigationItem() {
        navView.menu.itemsSequence().forEach { menuItem ->
            menuItem.isChecked = false
        }
    }
    private fun deselectBottomNavigationItem() {
        bottomNavigation.currentItem = AHBottomNavigation.CURRENT_ITEM_NONE
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.nav_inventory -> {
                item.isChecked = true
                deselectBottomNavigationItem()
            }
            R.id.nav_brew_logs -> {
                item.isChecked = true
                deselectBottomNavigationItem()
            }
            R.id.nav_share -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Try Hapi Brew app! https://play.google.com/store/apps/details?id=beer.hapibrew.app")
                val chooserIntent = Intent.createChooser(shareIntent, "Share Hapi Brew")
                chooserIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                if (chooserIntent.resolveActivity(packageManager) != null) {
                    startActivity(chooserIntent)
                }
            }
            R.id.nav_github_issues -> {
                val url = "https://github.com/zokipirlo/HapiBrew-Android/issues"
                val uri = Uri.parse(url)
//                val builder = CustomTabsIntent.Builder()
//                builder.setToolbarColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))
//                val customTabsIntent = builder.build()
//                customTabsIntent.launchUrl(this, Uri.parse(url))
                val browserIntent = Intent(Intent.ACTION_VIEW, uri)
                if (browserIntent.resolveActivity(packageManager) != null) {
                    startActivity(browserIntent)
                }
            }
            R.id.nav_send_feedback -> {
                val emailIntent = Intent(android.content.Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:")
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, arrayOf("support@hapibrew.beer"))
                if (emailIntent.resolveActivity(packageManager) != null) {
                    startActivity(Intent.createChooser(emailIntent, "Send feedback"))
                }
            }
            R.id.nav_settings -> {
                startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            }
            R.id.nav_about -> {
                startActivity(Intent(this@MainActivity, AboutActivity::class.java))
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (BuildConfig.DEBUG) {
            menuInflater.inflate(R.menu.menu_import, menu)
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0

        if (id == R.id.action_save) {
            importBrewdogRecipes()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun importBrewdogRecipes() {
        doAsync {
            try {
                val inputStream = assets.open("brewdog.xml")
                val recipes: List<Recipe>? = BeerXmlParser().parse(inputStream)
                if (recipes != null && recipes.isNotEmpty()) {
                    RecipeDatabase.saveRecipes(ctx, recipes.mapIndexed(::RecipeData))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
