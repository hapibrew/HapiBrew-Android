package beer.hapibrew.app.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.session.SessionController
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.ringer
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.activity_session.*
import timber.log.Timber

class SessionActivity : AppCompatActivity() {
    var router: Router? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Timber.d("SessionActivity", "onCreate")
        val sessionId = intent.getLongExtra(BUNDLE_SESSION_ID, -1L)

        if (sessionId == -1L) {
            Timber.d("SessionActivity", "onCreate: no session data")
            finish()
            return
        }

        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!(router?.hasRootController() ?: false)) {
            router?.setRoot(RouterTransaction.with(SessionController(sessionId)))
        }
    }

    override fun onNewIntent(intent: Intent) {
        Timber.d("SessionActivity", "onNewIntent")
        val sessionId = intent.getLongExtra(BUNDLE_SESSION_ID, -1L)
        if (sessionId != -1L) {
            Timber.d("SessionActivity", "onNewIntent: got session id")
            router?.setRoot(RouterTransaction.with(SessionController(sessionId)))
        }
    }

    override fun onStart() {
        super.onStart()

        ringer.stopRing()
    }

    override fun onStop() {


        super.onStop()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0
        if (id == android.R.id.home) {
            if (!(router?.handleBack() ?: false)) {
                super.onBackPressed()
            } else {
                NavUtils.navigateUpFromSameTask(this)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!(router?.handleBack() ?: false)) {
            super.onBackPressed()
        }
    }
}
