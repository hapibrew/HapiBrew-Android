package beer.hapibrew.app.activity

import beer.hapibrew.app.R
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import beer.hapibrew.app.settings.AdvancedEquipmentPreferenceFragment
import beer.hapibrew.app.settings.RootPreferenceFragment
import kotlinx.android.synthetic.main.activity_settings.*


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(fragmentHolder.id, RootPreferenceFragment(), RootPreferenceFragment::class.java.name)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0
        if (id == android.R.id.home) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                onBackPressed()
            } else {
                NavUtils.navigateUpFromSameTask(this)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun openFragment(fragmentTag: String) {
        val fragment:Fragment
        when (fragmentTag) {
            "beer.hapibrew.app.settings.AdvancedEquipmentPreferenceFragment" ->
                fragment = AdvancedEquipmentPreferenceFragment()
            else -> return
        }
        supportFragmentManager
                .beginTransaction()
                .replace(fragmentHolder.id, fragment, fragmentTag)
                .addToBackStack(null)
                .commit()
    }
}
