package beer.hapibrew.app.activity

import beer.hapibrew.app.R
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class NewRecipeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_recipe)
    }
}
