package beer.hapibrew.app.activity

import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Spinner
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.keg.KegTemperatureItem
import beer.hapibrew.app.adapter.items.keg.KegVolumeItem
import beer.hapibrew.app.calculators.kegPressure
import beer.hapibrew.app.data.KegPressureData
import beer.hapibrew.app.data.KegTempData
import beer.hapibrew.app.extensions.*
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.activity_keg_pressure.*
import kotlinx.android.synthetic.main.keg_pressure_item_view.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onItemSelectedListener
import org.jetbrains.anko.uiThread
import java.util.*


class KegPressureActivity : AppCompatActivity() {

    val METRIC_TEMPS = 26
    val IMPERIAL_TEMPS = 36
    val volumes = listOf(1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2,
            2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2) //19

    var pressureAdapter : UnitAdapter? = null
    var tempAdapter : FlexibleAdapter<KegTemperatureItem>? = null
    var volumesAdapter : FlexibleAdapter<KegVolumeItem>? = null

    val pressuresCelsius = ArrayList<KegPressureData>(METRIC_TEMPS * volumes.size)
    val pressuresFahrenheit = ArrayList<KegPressureData>(IMPERIAL_TEMPS * volumes.size)

    val tempCelsius = ArrayList<KegTemperatureItem>(METRIC_TEMPS)
    val tempFahrenheit = ArrayList<KegTemperatureItem>(IMPERIAL_TEMPS)

    var volumesLayoutManager : LinearLayoutManager? = null
    var tempsLayoutManager : LinearLayoutManager? = null
    var pressureLayoutManager : beer.hapibrew.app.view.FixedGridLayoutManager? = null

    var showBar:Boolean = true
    var showCelsius:Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keg_pressure)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        volumesLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        volumesRecyclerView.layoutManager = volumesLayoutManager
        volumesRecyclerView.setHasFixedSize(true)
        volumesRecyclerView.addOnItemTouchListener(disableTouches)

        tempsLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        temperatureRecyclerView.layoutManager = tempsLayoutManager
        temperatureRecyclerView.setHasFixedSize(true)
        volumesRecyclerView.addOnItemTouchListener(disableTouches)


        pressureRecyclerView.itemAnimator = DefaultItemAnimator()
        pressureRecyclerView.setHasFixedSize(true)
        pressureRecyclerView.addOnScrollListener(scrollListener)

        doAsync {
            val tempImperial = 30..(30+IMPERIAL_TEMPS)
            tempImperial.forEachIndexed { i, fahrenheit ->
                tempFahrenheit.add(KegTemperatureItem(KegTempData(i, fahrenheit.toNoDecimalsFormat())))
                volumes.forEachIndexed { j, volume ->
                    val psi = kegPressure(fahrenheit.toDouble(), volume)
                    val index = i * 36 + j
                    val data = KegPressureData(index, psi.toPsiFormat(), psi.psiToBar().toBarFormat())
                    pressuresFahrenheit.add(data)
                }
            }
            val tempMetric = 1..METRIC_TEMPS
            tempMetric.forEachIndexed { i, celsius ->
                tempCelsius.add(KegTemperatureItem(KegTempData(i, celsius.toNoDecimalsFormat())))
                volumes.forEachIndexed { j, volume ->
                    val psi = kegPressure(celsius.toDouble().cToF(), volume)
                    val index = i * 36 + j
                    val data = KegPressureData(index, psi.toPsiFormat(), psi.psiToBar().toBarFormat())
                    pressuresCelsius.add(data)
                }
            }

            volumesAdapter = FlexibleAdapter<KegVolumeItem>(volumes.map { KegVolumeItem(it.toOneDecimalFormat()) })
            if (showCelsius) {
                tempAdapter = FlexibleAdapter<KegTemperatureItem>(tempCelsius)
                pressureAdapter = UnitAdapter(pressuresCelsius, showBar)
            }
            else {
                tempAdapter = FlexibleAdapter<KegTemperatureItem>(tempFahrenheit)
                pressureAdapter = UnitAdapter(pressuresFahrenheit, showBar)
            }

            uiThread {
                showTempUnit()
                volumesRecyclerView.adapter = volumesAdapter
                temperatureRecyclerView.adapter = tempAdapter

                pressureLayoutManager = beer.hapibrew.app.view.FixedGridLayoutManager()
                pressureLayoutManager?.setTotalColumnCount(volumes.size)
                pressureRecyclerView.layoutManager = pressureLayoutManager
                pressureRecyclerView.adapter = pressureAdapter
            }
        }
    }

    val disableTouches = object : RecyclerView.OnItemTouchListener {
        override fun onTouchEvent(rv: RecyclerView?, e: MotionEvent?) {
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

        override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
            return true
        }
    }

    val scrollListener = object : RecyclerView.OnScrollListener() {
        var state:Int = RecyclerView.SCROLL_STATE_IDLE
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (state == RecyclerView.SCROLL_STATE_IDLE) {
                return
            }

            val firstVisibleItem = pressureLayoutManager?.getChildAt(0)
            firstVisibleItem?.let {
                val firstRow = pressureLayoutManager?.firstVisibleRow ?: 0
                val firstColumn = pressureLayoutManager?.firstVisibleColumn ?: 0

                val decoratedX = pressureLayoutManager?.getDecoratedLeft(firstVisibleItem)
                val decoratedY = pressureLayoutManager?.getDecoratedTop(firstVisibleItem)

                decoratedX?.let {
                    volumesLayoutManager?.scrollToPositionWithOffset(firstColumn, decoratedX)
                }
                decoratedY?.let {
                    tempsLayoutManager?.scrollToPositionWithOffset(firstRow, decoratedY)
                }
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            state = newState
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_keg_pressure, menu)
        val pressure = menu.findItem(R.id.action_pressure).actionView as Spinner
        pressure.onItemSelectedListener {
            onItemSelected { _, _, i, _ ->
                val isBar = i == 0
                if (showBar == isBar)
                    return@onItemSelected

                switchPressure()
            }
        }
//        pressure.title = if (showBar) getString(R.string.unit_psi) else getString(R.string.unit_bar)
        val temperature = menu.findItem(R.id.action_temperature).actionView as Spinner
        temperature.onItemSelectedListener {
            onItemSelected { _, _, i, _ ->
                val isCelsius = i == 0
                if (showCelsius == isCelsius)
                    return@onItemSelected

                switchTemperature()
            }
        }
//        temperature.title = if (showCelsius) getString(R.string.unit_fahrenheit) else getString(R.string.unit_celsius)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        /*if (id == R.id.action_pressure) {
            switchPressure()
            return true
        } else if (id == R.id.action_temperature) {
            switchTemperature()
            return true
        } else */if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showTempUnit() {
        unitTemperature.text = getString(if (showCelsius) R.string.unit_celsius else R.string.unit_fahrenheit)
    }

    private fun switchPressure() {
        showBar = !showBar
        pressureAdapter?.switchUnits(showBar)
//        supportInvalidateOptionsMenu()
    }

    private fun switchTemperature() {
        showCelsius = !showCelsius
        if (showCelsius) {
            pressureAdapter?.updateDataSet(pressuresCelsius, false)
            tempAdapter?.updateDataSet(tempCelsius, false)

        }
        else {
            pressureAdapter?.updateDataSet(pressuresFahrenheit, false)
            tempAdapter?.updateDataSet(tempFahrenheit, false)
        }
        showTempUnit()
//        supportInvalidateOptionsMenu()
    }

    class UnitAdapter(var items: ArrayList<KegPressureData>, var showMetrics:Boolean = true) : RecyclerView.Adapter<UnitAdapter.ViewHolder>() {
        fun updateDataSet(items: ArrayList<KegPressureData>, animate:Boolean) {
            this.items = items
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.keg_pressure_item_view, parent, false)
            return ViewHolder(v, this)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindData(items[position])
            holder.itemView.setAlternateBackground(position)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        fun switchUnits(useMetrics: Boolean) {
            showMetrics = useMetrics
            notifyDataSetChanged()
        }

        class ViewHolder(v: View, val adapter:UnitAdapter) : RecyclerView.ViewHolder(v) {
            fun bindData(kegPressureData: KegPressureData) {
                itemView.kegPressureText.text = if (adapter.showMetrics) {
                    kegPressureData.bar
                } else {
                    kegPressureData.psi
                }
            }
        }
    }
}
