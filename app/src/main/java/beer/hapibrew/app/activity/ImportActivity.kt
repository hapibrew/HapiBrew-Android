package beer.hapibrew.app.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.support.design.widget.Snackbar
import android.support.v4.app.NavUtils
import android.support.v4.app.ShareCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.RecipeItem
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.database.RecipeDatabase
import beer.hapibrew.app.extensions.BUNDLE_HIDE_FAB
import beer.hapibrew.app.extensions.BUNDLE_RECIPE_DATA
import beer.hapibrew.beerxml2proto.BeerXmlParser
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.nononsenseapps.filepicker.FilePickerActivity
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.activity_import.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import timber.log.Timber
import java.util.*


class ImportActivity : AppCompatActivity() {

    private val REQUEST_CODE_SAF = 1
    private val REQUEST_CODE_SAF_MUTLIPLE = 2
    private val REQUEST_CODE_FP = 3
    val recipes: ArrayList<RecipeData> = ArrayList()
    var adapter: FlexibleAdapter<RecipeItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_import)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = getString(R.string.import_recipe)

        if (Intent.ACTION_SEND == intent.action && intent.type != null ||
                Intent.ACTION_SEND_MULTIPLE == intent.action && intent.type != null) {
            handleSendIntent()
        } else if (intent != null && Intent.ACTION_VIEW == intent.action) {
            handleViewIntent(intent)
        }

        instructionsHolder.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
//                intent.addCategory(Intent.CATEGORY_OPENABLE)
//                intent.type = "*/*"

                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.type = "text/xml"
                if (intent.resolveActivity(packageManager) != null) {
                    startActivityForResult(intent, REQUEST_CODE_SAF_MUTLIPLE)
                }
            } else {
                val i = Intent(this@ImportActivity, FilePickerActivity::class.java)
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true)
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false)
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE)
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().path)
                if (intent.resolveActivity(packageManager) != null) {
                    startActivityForResult(i, REQUEST_CODE_FP)
                }
            }
        }
    }

    private fun showStartImporting() {
        instructionsHolder.visibility = View.VISIBLE
        recipesHolder.visibility = View.GONE
        importLoading.visibility = View.GONE
    }

    private fun showSpinner() {
        instructionsHolder.visibility = View.GONE
        recipesHolder.visibility = View.GONE
        importLoading.visibility = View.VISIBLE
    }

    private fun showImportedRecipes() {
        if (recipes.isNotEmpty()) {
            instructionsHolder.visibility = View.GONE
            importLoading.visibility = View.GONE
            recipesHolder.visibility = View.VISIBLE

            recyclerView.itemAnimator = DefaultItemAnimator()

            recyclerView.layoutManager = GridLayoutManager(ctx, 2)
            adapter = FlexibleAdapter<RecipeItem>(recipes.map(::RecipeItem),
                    FlexibleAdapter.OnItemClickListener { position : Int ->
                        val intent = Intent(ctx, RecipeActivity::class.java)
                        intent.putExtra(BUNDLE_RECIPE_DATA, recipes[position])
                        intent.putExtra(BUNDLE_HIDE_FAB, true)
                        startActivity(intent)
                        true
                    })
            recyclerView.adapter = adapter
        } else {
            showStartImporting()
        }
    }

    private fun handleSendIntent() {
        val intentReader = ShareCompat.IntentReader.from(this)
        if (intentReader.streamCount > 0) {
            importUris(
                    (0..intentReader.streamCount - 1)
                        .map { intentReader.getStream(it) }
            )
        }
    }

    private fun handleViewIntent(data:Intent) {
        val clip = data.clipData
        if (clip != null) {
            importUris(
                    (0..clip.itemCount - 1)
                            .map { clip.getItemAt(it).uri }
            )
        } else {
            val uri = data.data
            if (uri != null)
                importUris(listOf(uri))
        }
    }

    private fun importUris(uris:List<Uri>) {
        recipes.clear()
        showSpinner()

        doAsync {
            uris.forEach {
                importRecipe(it)
            }
            uiThread {
                invalidateOptionsMenu()
                showImportedRecipes()
            }
        }
    }
    private fun importRecipe(uri: Uri) {
        try {
            val inputStream = contentResolver.openInputStream(uri)
            val _recipes: List<Recipe>? = BeerXmlParser().parse(inputStream)
            if (_recipes != null && _recipes.isNotEmpty()) {
                recipes.addAll(_recipes.mapIndexed(::RecipeData))
            } else {
                Snackbar.make(rootLayout, getString(R.string.import_error, uri.lastPathSegment), Snackbar.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            Timber.e(e, "Error importing ")
            Snackbar.make(rootLayout, getString(R.string.import_error, uri.lastPathSegment), Snackbar.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_import, menu)
        val saveMenuItem = menu.findItem(R.id.action_save)
        val isEnabled = recipes.isNotEmpty()
        saveMenuItem.isEnabled = recipes.isNotEmpty()
        saveMenuItem.icon.alpha = if (isEnabled) 255 else 50
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0

        if (id == R.id.action_save) {
            saveRecipes()
            return true
        } else if (id == android.R.id.home) {
            val upIntent = NavUtils.getParentActivityIntent(this) ?: return true
            if (NavUtils.shouldUpRecreateTask(this, upIntent) || isTaskRoot) {
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(upIntent)
                        .startActivities()
            } else {
                NavUtils.navigateUpTo(this, upIntent)
            }
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun saveRecipes() {
        doAsync {
            RecipeDatabase.saveRecipes(ctx, recipes)

            uiThread {
                Snackbar.make(rootLayout, R.string.recipes_saved, Snackbar.LENGTH_LONG).show()
                recipes.clear()
                invalidateOptionsMenu()
                showImportedRecipes()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK || data === null) {
            return
        }
        if (requestCode == REQUEST_CODE_FP) {
            handleViewIntent(data)
        } else if (requestCode == REQUEST_CODE_SAF) {
            val uri = data.data
            if (uri !== null)
                checkFileMetaData(uri)
        } else if (requestCode == REQUEST_CODE_SAF_MUTLIPLE) {
            val clip = data.clipData
            if (clip != null) {
                importUris(
                        (0..clip.itemCount - 1)
                                .map { clip.getItemAt(it).uri }
                )
            } else {
                val uri = data.data
                if (uri != null)
                    importUris(listOf(uri))
            }
        }
    }


    private fun checkFileMetaData(uri: Uri) {
        val cursor = contentResolver.query(uri, null, null, null, null, null)
        var size: Long = -1
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                val sizeIndDb: String?
                if (!cursor.isNull(sizeIndex)) {
                    sizeIndDb = cursor.getString(sizeIndex)
                } else {
                    sizeIndDb = null
                }
                try {
                    size = java.lang.Long.parseLong(sizeIndDb, 10)
                } catch (e: NumberFormatException) {
                    size = -1
                }

            }
            cursor.close()
        }

        if (size > 0) {
            importUris(listOf(uri))
        }
    }
}
