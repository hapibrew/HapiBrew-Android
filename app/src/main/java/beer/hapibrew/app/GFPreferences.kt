package beer.hapibrew.app

import beer.hapibrew.app.extensions.Liters
import android.content.Context
import android.content.SharedPreferences

class GFPreferences(context: Context) {
    private val GF_WTG_RATIO = "gf_wtg_ratio"
    private val GF_WTG_BIG_RATIO = "gf_wtg_big_ratio"
    private val GF_USE_BIG_RATIO = "gf_use_big_ratio"
    private val GF_WTG_BIG_FACTOR = "gf_wtg_big_factor"
    private val GF_MASH_ADD_WATER = "gf_mash_add_water"
    private val GF_EVAPORATION = "gf_evaporation"
    private val GF_TRUB_LOSS = "gf_trub_loss"
    private val GF_SPARGE_FACTOR = "gf_sparge_factor"

    private val _prefs: SharedPreferences = context.getSharedPreferences("grainfather", Context.MODE_PRIVATE)

    var gfWTGRatio: Double
        get() = _prefs.getFloat(GF_WTG_RATIO, 2.7f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_WTG_RATIO, value.toFloat()).apply()
    var gfWTGBigRatio: Double
        get() = _prefs.getFloat(GF_WTG_BIG_RATIO, 2.3f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_WTG_BIG_RATIO, value.toFloat()).apply()

    var gfUseBigWTGRatio: Boolean
        get() = _prefs.getBoolean(GF_USE_BIG_RATIO, true)
        set(value) = _prefs.edit().putBoolean(GF_USE_BIG_RATIO, value).apply()
    var gfWTGBigFactor: Double
        get() = _prefs.getFloat(GF_WTG_BIG_FACTOR, 7.6f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_WTG_BIG_FACTOR, value.toFloat()).apply()

    var gfMashWater: Liters
        get() = _prefs.getFloat(GF_MASH_ADD_WATER, 3.5f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_MASH_ADD_WATER, value.toFloat()).apply()

    var gfEvaporation: Liters
        get() = _prefs.getFloat(GF_EVAPORATION, if (prefs.isGF220V) 3f else 2f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_EVAPORATION, value.toFloat()).apply()

    var gfTrubLoss: Liters
        get() = _prefs.getFloat(GF_TRUB_LOSS, 2f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_TRUB_LOSS, value.toFloat()).apply()

    var gfSpargeFactor: Double
        get() = _prefs.getFloat(GF_SPARGE_FACTOR, 0.8f).toDouble()
        set(value) = _prefs.edit().putFloat(GF_SPARGE_FACTOR, value.toFloat()).apply()

    fun resetGrainfatherSettings() {
        _prefs.edit().clear().apply()
    }
}