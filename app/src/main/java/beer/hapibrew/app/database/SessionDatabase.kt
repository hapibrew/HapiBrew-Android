package beer.hapibrew.app.database

import android.content.Context
import android.provider.BaseColumns
import beer.hapibrew.app.data.SessionData
import org.jetbrains.anko.db.*

object SessionDatabase {
    fun getSession(ctx: Context?, id: Long): SessionData? {
        return ctx?.sessionDB?.use {
            select(SessionEntry.TABLE_NAME, *SessionEntry.ALL_COLUMNS())
                    .whereSimple("${BaseColumns._ID} = $id")
                    .parseOpt(SessionEntry)
        }
    }

    fun getSessions(ctx: Context?): List<SessionData> {
        val sessions = ctx?.sessionDB?.use {
            select(SessionEntry.TABLE_NAME, *SessionEntry.ALL_COLUMNS())
//                    .orderBy(SessionEntry.COLUMN_PHASE, SqlOrderDirection.ASC)
                    .parseList(SessionEntry)
        }
        return sessions ?: emptyList()
    }

    fun saveNewSession(ctx: Context?, session: SessionData): Long {
        var id: Long = -1
        ctx?.sessionDB?.use {
            id = insert(SessionEntry.TABLE_NAME, *SessionEntry.MAP_ALL(session, false))
        }

        markChange(ctx)

        return id
    }

    fun updateSession(ctx: Context?, session: SessionData) {
        ctx?.sessionDB?.use {
            update(SessionEntry.TABLE_NAME, *SessionEntry.MAP_ALL(session, false))
                    .whereSimple("${BaseColumns._ID} = ${session.id}")
                    .exec()
        }

        markChange(ctx)
    }

    fun restoreSessions(ctx: Context?, sessions: List<SessionData>) {
        ctx?.sessionDB?.use {
            transaction {
                sessions.forEach {
                    insert(SessionEntry.TABLE_NAME, *SessionEntry.MAP_ALL(it, true))
                }
            }
        }
    }

    fun saveSessions(ctx: Context?, sessions: List<SessionData>) {
        ctx?.sessionDB?.use {
            transaction {
                sessions.forEach {
                    insert(SessionEntry.TABLE_NAME, *SessionEntry.MAP_ALL(it, false))
                }
            }
        }

        markChange(ctx)
    }

    fun getNotes(ctx: Context?, id: Long): String? {
        return ctx?.sessionDB?.use {
            select(SessionEntry.TABLE_NAME, SessionEntry.COLUMN_NOTES)
                    .whereSimple("${BaseColumns._ID} = $id")
                    .exec {
                        if (moveToFirst())
                            getString(0)
                        else
                            null
                    }
        }
    }

    fun updateNotes(ctx: Context?, id: Long, notes: String) {
        ctx?.sessionDB?.use {
            update(SessionEntry.TABLE_NAME, SessionEntry.COLUMN_NOTES to notes)
                    .whereSimple("${BaseColumns._ID} = $id")
                    .exec()
        }

        markChange(ctx)
    }

    fun deleteSessions(ctx: Context?, sessions: List<SessionData>) {
        ctx?.sessionDB?.use {
            transaction {
                sessions.forEach {
                    delete(SessionEntry.TABLE_NAME,
                            "${BaseColumns._ID} = ${it.id}")
                }
            }
        }

        markChange(ctx)
    }

    fun deleteAllSessions(ctx: Context?) {
        ctx?.sessionDB?.use {
            delete(SessionEntry.TABLE_NAME)
        }
    }

    private fun markChange(ctx: Context?) {
        ctx?.sessionDB?.hasModified = true
    }
}