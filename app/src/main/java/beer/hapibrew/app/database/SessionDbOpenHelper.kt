package beer.hapibrew.app.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import org.jetbrains.anko.db.*

class SessionDbOpenHelper(context: Context) : ManagedSQLiteOpenHelper(context, "Session.db", null, 1) {

    companion object {
        private var instance: SessionDbOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): SessionDbOpenHelper {
            if (instance == null) {
                instance = SessionDbOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    var hasModified:Boolean = false;

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(SessionEntry.TABLE_NAME, true,
                BaseColumns._ID to INTEGER + PRIMARY_KEY + UNIQUE,
                SessionEntry.COLUMN_NAME to TEXT,
                SessionEntry.COLUMN_BATCH to INTEGER,
                SessionEntry.COLUMN_MASH to REAL,
                SessionEntry.COLUMN_SPAGE to REAL,
                SessionEntry.COLUMN_START to INTEGER,
                SessionEntry.COLUMN_END to INTEGER,
                SessionEntry.COLUMN_OG to REAL,
                SessionEntry.COLUMN_FG to REAL,
                SessionEntry.COLUMN_PHASE to TEXT,
                SessionEntry.COLUMN_STEP to INTEGER,
                SessionEntry.COLUMN_STEP_START to INTEGER,
                SessionEntry.COLUMN_STEP_PAUSE to INTEGER,
                SessionEntry.COLUMN_STEP_END to INTEGER,
                SessionEntry.COLUMN_RECIPE to BLOB,
                SessionEntry.COLUMN_NOTES to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
}

val Context.sessionDB: SessionDbOpenHelper
    get() = SessionDbOpenHelper.getInstance(applicationContext)