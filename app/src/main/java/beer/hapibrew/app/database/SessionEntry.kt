package beer.hapibrew.app.database

import beer.hapibrew.beerxml2proto.proto.Recipe
import beer.hapibrew.app.data.SessionData
import android.provider.BaseColumns
import org.jetbrains.anko.db.RowParser

object SessionEntry : BaseColumns, RowParser<SessionData> {
    val TABLE_NAME = "session"
    val COLUMN_NAME = "name" // name
    val COLUMN_BATCH = "batch" // batch number
    val COLUMN_MASH = "mash" // mash water in l
    val COLUMN_SPAGE = "sparge" // sparge water in l
    val COLUMN_START = "startEpoch" // start recipe epoch
    val COLUMN_END = "endEpoch" // done recipe epoch
    val COLUMN_OG = "og" // og in sg format
    val COLUMN_FG = "fg" // fg in sg format
    val COLUMN_PHASE = "phase" //SessionData.PHASE
    val COLUMN_STEP = "step" //0, 1, 2, ...
    val COLUMN_STEP_START = "stepStart" //start epoch
    val COLUMN_STEP_PAUSE = "stepPause" //pause epoch
    val COLUMN_STEP_END = "stepEnd" //end epoch
    val COLUMN_RECIPE = "recipe"
    val COLUMN_NOTES = "notes"

    fun ALL_COLUMNS(): Array<String> {
        return arrayOf(
                BaseColumns._ID,
                SessionEntry.COLUMN_NAME,
                SessionEntry.COLUMN_BATCH,
                SessionEntry.COLUMN_MASH,
                SessionEntry.COLUMN_SPAGE,
                SessionEntry.COLUMN_START,
                SessionEntry.COLUMN_END,
                SessionEntry.COLUMN_OG,
                SessionEntry.COLUMN_FG,
                SessionEntry.COLUMN_PHASE,
                SessionEntry.COLUMN_STEP,
                SessionEntry.COLUMN_STEP_START,
                SessionEntry.COLUMN_STEP_PAUSE,
                SessionEntry.COLUMN_STEP_END,
                SessionEntry.COLUMN_RECIPE,
                SessionEntry.COLUMN_NOTES
        )
    }

    fun MAP_ALL(data: SessionData, withId: Boolean): Array<Pair<String, Any?>> {
        if (withId) {
            return arrayOf(
                    BaseColumns._ID to data.id,
                    SessionEntry.COLUMN_NAME to data.name,
                    SessionEntry.COLUMN_BATCH to data.batch,
                    SessionEntry.COLUMN_MASH to data.mashWater,
                    SessionEntry.COLUMN_SPAGE to data.spargeWater,
                    SessionEntry.COLUMN_START to data.startEpoch,
                    SessionEntry.COLUMN_END to data.endEpoch,
                    SessionEntry.COLUMN_OG to data.og,
                    SessionEntry.COLUMN_FG to data.fg,
                    SessionEntry.COLUMN_PHASE to data.phase.name,
                    SessionEntry.COLUMN_STEP to data.step,
                    SessionEntry.COLUMN_STEP_START to data.stepStartEpoch,
                    SessionEntry.COLUMN_STEP_PAUSE to data.stepPauseEpoch,
                    SessionEntry.COLUMN_STEP_END to data.stepEndEpoch,
                    SessionEntry.COLUMN_RECIPE to data.recipe.encode(),
                    SessionEntry.COLUMN_NOTES to data.notes
            )
        } else {
            return arrayOf(
                    SessionEntry.COLUMN_NAME to data.name,
                    SessionEntry.COLUMN_BATCH to data.batch,
                    SessionEntry.COLUMN_MASH to data.mashWater,
                    SessionEntry.COLUMN_SPAGE to data.spargeWater,
                    SessionEntry.COLUMN_START to data.startEpoch,
                    SessionEntry.COLUMN_END to data.endEpoch,
                    SessionEntry.COLUMN_OG to data.og,
                    SessionEntry.COLUMN_FG to data.fg,
                    SessionEntry.COLUMN_PHASE to data.phase.name,
                    SessionEntry.COLUMN_STEP to data.step,
                    SessionEntry.COLUMN_STEP_START to data.stepStartEpoch,
                    SessionEntry.COLUMN_STEP_PAUSE to data.stepPauseEpoch,
                    SessionEntry.COLUMN_STEP_END to data.stepEndEpoch,
                    SessionEntry.COLUMN_RECIPE to data.recipe.encode(),
                    SessionEntry.COLUMN_NOTES to data.notes
            )
        }
    }

    override fun parseRow(columns: Array<Any?>): SessionData {
        return SessionData(
                columns[0] as Long,
                columns[1] as String,
                (columns[2] as Long).toInt(),
                columns[3] as Double,
                columns[4] as Double,
                columns[5] as Long,
                columns[6] as Long,
                columns[7] as Double,
                columns[8] as Double,
                SessionData.Phase.valueOf(columns[9] as String),
                (columns[10] as Long).toInt(),
                columns[11] as Long,
                columns[12] as Long,
                columns[13] as Long,
                Recipe.ADAPTER.decode(columns[14] as ByteArray),
                columns[15] as String
        )
    }
}