package beer.hapibrew.app.database

import android.content.Context
import android.provider.BaseColumns
import beer.hapibrew.app.data.RecipeData
import org.jetbrains.anko.db.*

object RecipeDatabase {
    fun getRecipe(ctx: Context?, id: Long): RecipeData? {
        return ctx?.recipeDB?.use {
            select(RecipeEntry.TABLE_NAME, *RecipeEntry.ALL_COLUMNS())
                    .whereSimple("${BaseColumns._ID} = $id")
                    .parseOpt(RecipeEntry)
        }
    }

    fun getRecipes(ctx: Context?): List<RecipeData> {
        val recipes = ctx?.recipeDB?.use {
            select(RecipeEntry.TABLE_NAME, *RecipeEntry.ALL_COLUMNS())
                    .orderBy(RecipeEntry.COLUMN_NAME, SqlOrderDirection.ASC)
                    .parseList(RecipeEntry)
        }
        return recipes ?: emptyList()
    }

    fun restoreRecipes(ctx: Context?, recipes: List<RecipeData>) {
        ctx?.recipeDB?.use {
            transaction {
                recipes.forEach {
                    insert(RecipeEntry.TABLE_NAME, *RecipeEntry.MAP_ALL(it, true))
                }
            }
        }
    }

    fun saveRecipes(ctx: Context?, recipes: List<RecipeData>) {
        ctx?.recipeDB?.use {
            transaction {
                recipes.forEach {
                    insert(RecipeEntry.TABLE_NAME, *RecipeEntry.MAP_ALL(it, false))
                }
            }
        }
    }

    fun increaseSessions(ctx: Context?, id: Long) {
        ctx?.recipeDB?.use {
            execSQL("UPDATE ${RecipeEntry.TABLE_NAME} SET " +
                    "${RecipeEntry.COLUMN_SESSIONS} = ${RecipeEntry.COLUMN_SESSIONS} + 1 " +
                    "WHERE ${BaseColumns._ID} = $id")
        }
    }

    fun deleteRecipes(ctx: Context?, recipes: List<RecipeData>) {
        ctx?.recipeDB?.use {
            transaction {
                recipes.forEach {
                    delete(RecipeEntry.TABLE_NAME,
                            "${BaseColumns._ID} = ${it.id}")
                }
            }
        }
    }

    fun deleteAllRecipes(ctx: Context?) {
        ctx?.recipeDB?.use {
            delete(RecipeEntry.TABLE_NAME)
        }
    }
}