package beer.hapibrew.app.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import org.jetbrains.anko.db.*

class RecipeDbOpenHelper(context: Context) : ManagedSQLiteOpenHelper(context, "Recipe.db", null, 1) {

    companion object {
        private var instance: RecipeDbOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): RecipeDbOpenHelper {
            if (instance == null) {
                instance = RecipeDbOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(RecipeEntry.TABLE_NAME, true,
                BaseColumns._ID to INTEGER + PRIMARY_KEY + UNIQUE,
                RecipeEntry.COLUMN_NAME to TEXT,
                RecipeEntry.COLUMN_OG to REAL,
                RecipeEntry.COLUMN_FG to REAL,
                RecipeEntry.COLUMN_SRM to REAL,
                RecipeEntry.COLUMN_ABV to REAL,
                RecipeEntry.COLUMN_IBU to REAL,
                RecipeEntry.COLUMN_SESSIONS to INTEGER + DEFAULT("0"),
                RecipeEntry.COLUMN_LOGS to INTEGER + DEFAULT("0"),
                RecipeEntry.COLUMN_PROTO to BLOB)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
}

val Context.recipeDB: RecipeDbOpenHelper
    get() = RecipeDbOpenHelper.getInstance(applicationContext)