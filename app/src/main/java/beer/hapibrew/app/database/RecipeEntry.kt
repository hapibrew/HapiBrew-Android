package beer.hapibrew.app.database

import android.provider.BaseColumns
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.beerxml2proto.proto.Recipe
import org.jetbrains.anko.db.RowParser

object RecipeEntry : BaseColumns, RowParser<RecipeData> {
    val TABLE_NAME = "recipe"
    val COLUMN_NAME = "name"
    val COLUMN_OG = "og"
    val COLUMN_FG = "fg"
    val COLUMN_SRM = "srm"
    val COLUMN_ABV = "abv"
    val COLUMN_IBU = "ibu"
    val COLUMN_SESSIONS = "sessions"
    val COLUMN_LOGS = "logs"
    val COLUMN_PROTO = "proto"

    fun ALL_COLUMNS(): Array<String> {
        return arrayOf(
                BaseColumns._ID,
                RecipeEntry.COLUMN_NAME,
                RecipeEntry.COLUMN_OG,
                RecipeEntry.COLUMN_FG,
                RecipeEntry.COLUMN_SRM,
                RecipeEntry.COLUMN_ABV,
                RecipeEntry.COLUMN_IBU,
                RecipeEntry.COLUMN_SESSIONS,
                RecipeEntry.COLUMN_LOGS,
                RecipeEntry.COLUMN_PROTO
        )
    }

    fun MAP_ALL(data: RecipeData, withId: Boolean): Array<Pair<String, Any?>> {
        if (withId) {
            return arrayOf(
                    BaseColumns._ID to data.id,
                    RecipeEntry.COLUMN_NAME to data.name,
                    RecipeEntry.COLUMN_OG to data.og,
                    RecipeEntry.COLUMN_FG to data.fg,
                    RecipeEntry.COLUMN_SRM to data.srm,
                    RecipeEntry.COLUMN_ABV to data.abv,
                    RecipeEntry.COLUMN_IBU to data.ibu,
                    RecipeEntry.COLUMN_SESSIONS to data.sessions,
                    RecipeEntry.COLUMN_LOGS to data.logs,
                    RecipeEntry.COLUMN_PROTO to data.proto.encode()
            )
        } else {
            return arrayOf(
                    RecipeEntry.COLUMN_NAME to data.name,
                    RecipeEntry.COLUMN_OG to data.og,
                    RecipeEntry.COLUMN_FG to data.fg,
                    RecipeEntry.COLUMN_SRM to data.srm,
                    RecipeEntry.COLUMN_ABV to data.abv,
                    RecipeEntry.COLUMN_IBU to data.ibu,
                    RecipeEntry.COLUMN_SESSIONS to data.sessions,
                    RecipeEntry.COLUMN_LOGS to data.logs,
                    RecipeEntry.COLUMN_PROTO to data.proto.encode()
            )
        }
    }

    override fun parseRow(columns: Array<Any?>): RecipeData {
        return RecipeData(
                columns[0] as Long,
                columns[1] as String,
                columns[2] as Double,
                columns[3] as Double,
                columns[4] as Double,
                columns[5] as Double,
                columns[6] as Double,
                (columns[7] as Long).toInt(),
                (columns[8] as Long).toInt(),
                Recipe.ADAPTER.decode(columns[9] as ByteArray)
        )
    }
}