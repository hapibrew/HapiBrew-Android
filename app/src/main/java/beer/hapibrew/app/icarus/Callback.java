package beer.hapibrew.app.icarus;

/**
 * Created by wscn on 16/4/23.
 */
public interface Callback {
    public void run(String params);
}
