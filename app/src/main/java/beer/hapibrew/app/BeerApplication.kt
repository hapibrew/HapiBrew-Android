package beer.hapibrew.app

//import com.squareup.leakcanary.LeakCanary
//import com.squareup.leakcanary.RefWatcher
import android.app.Application
import android.util.Log
import beer.hapibrew.app.job.NotificationJobCreator
import beer.hapibrew.app.service.Ringer
import com.crashlytics.android.Crashlytics
import com.evernote.android.job.JobManager
import com.evernote.android.job.util.JobCat
import io.fabric.sdk.android.Fabric
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import timber.log.Timber.DebugTree

val prefs: MainPreferences by lazy {
    BeerApplication.Companion._prefs!!
}

val gfPrefs: GFPreferences by lazy {
    BeerApplication.Companion._gfPrefs!!
}

val ringer: Ringer by lazy {
    BeerApplication.Companion._ringer!!
}

class BeerApplication : Application() {

    companion object {
//        var refWatcher: RefWatcher? = null
        var _prefs: MainPreferences? = null
        var _gfPrefs: GFPreferences? = null
        var _ringer: Ringer? = null
    }

    override fun onCreate() {
        super.onCreate()

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return
//        }
//        refWatcher = LeakCanary.install(this)
        JodaTimeAndroid.init(this)

        JobCat.setLogcatEnabled(BuildConfig.DEBUG)
        JobManager.create(this)
                .addJobCreator(NotificationJobCreator)

        Fabric.with(this, Crashlytics())

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
//            Timber.plant(CrashReportingTree())
        } else {
            Timber.plant(CrashReportingTree())
        }


        _prefs = beer.hapibrew.app.MainPreferences(applicationContext)
        _gfPrefs = GFPreferences(applicationContext)
        _ringer = Ringer(applicationContext)
    }

    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return
            }

//                Timber.d("Reporting message")
            Crashlytics.log(priority, tag, message)

            if (t != null) {
//                Timber.d("Reporting exception")
                Crashlytics.logException(t)
            }
//            if (message != null)
//                FirebaseCrash.log(message)
//
//            if (t != null) {
//                FirebaseCrash.report(t)
//            }
        }
    }
}