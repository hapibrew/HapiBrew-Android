package beer.hapibrew.app.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import beer.hapibrew.app.R
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.gfPrefs
import beer.hapibrew.app.prefs
import org.jetbrains.anko.AnkoLogger
import rikka.materialpreference.EditTextPreference
import rikka.materialpreference.Preference
import rikka.materialpreference.PreferenceFragment
import rikka.materialpreference.SwitchPreference
import timber.log.Timber

class AdvancedEquipmentPreferenceFragment : PreferenceFragment(), SharedPreferences.OnSharedPreferenceChangeListener, AnkoLogger {
    var gfWTGRatio: EditTextPreference? = null
    var gfWTGBigRatio: EditTextPreference? = null
    var gfBigRatioSwitch: SwitchPreference? = null
    var gfWTGBigFactor: EditTextPreference? = null

    var gfMashAdd: EditTextPreference? = null
    var gfEvaporation: EditTextPreference? = null
    var gfTrubLoss: EditTextPreference? = null
    var gfSpargeFactor: EditTextPreference? = null

    var mashNormalEquation: Preference? = null
    var mashBigEquation: Preference? = null
    var spargeEquation: Preference? = null

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            getString(R.string.pref_advanced_use_wtgr_big) ->
                gfPrefs.gfUseBigWTGRatio = gfBigRatioSwitch?.isChecked ?: true
        }
        updateEquations()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        with(preferenceManager) {
            defaultPackages = arrayOf("beer.hapibrew.app.")
            sharedPreferencesName = "settings"
            sharedPreferencesMode = Context.MODE_PRIVATE
        }
        addPreferencesFromResource(R.xml.prefs_advanced_equipment)

        gfWTGRatio = findPreference(getString(R.string.pref_advanced_wtgr)) as EditTextPreference
        gfWTGRatio?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfWTGRatio = if (prefs.useMetric) value else value.galPerLbToLPerKg()
                updateWTGSummary(gfWTGRatio, gfPrefs.gfWTGRatio)
                true
            }
            false
        }
        gfWTGBigRatio = findPreference(getString(R.string.pref_advanced_wtgr_big)) as EditTextPreference
        gfWTGBigRatio?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfWTGBigRatio = if (prefs.useMetric) value else value.galToL()
                updateWTGSummary(gfWTGBigRatio, gfPrefs.gfWTGBigRatio)
                true
            }
            false
        }
        gfBigRatioSwitch = findPreference(getString(R.string.pref_advanced_use_wtgr_big)) as SwitchPreference
        gfBigRatioSwitch?.isChecked = gfPrefs.gfUseBigWTGRatio

        gfWTGBigFactor = findPreference(getString(R.string.pref_advanced_wtgr_big_factor)) as EditTextPreference
        gfWTGBigFactor?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfWTGBigFactor = if (prefs.useMetric) value else value.lbToKg()
                updateWTGBigFactorSummary()
                true
            }
            false
        }

        gfMashAdd = findPreference(getString(R.string.pref_advanced_mash_add)) as EditTextPreference
        gfMashAdd?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfMashWater = if (prefs.useMetric) value else value.galToL()
                updateMashAddSummary()
                true
            }
            false
        }
        gfEvaporation = findPreference(getString(R.string.pref_advanced_evaporation)) as EditTextPreference
        gfEvaporation?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfEvaporation = if (prefs.useMetric) value else value.galToL()
                updateEvaporationSummary()
                true
            }
            false
        }
        gfTrubLoss = findPreference(getString(R.string.pref_advanced_trub_loss)) as EditTextPreference
        gfTrubLoss?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfTrubLoss = if (prefs.useMetric) value else value.galToL()
                updateTrubLossSummary()
                true
            }
            false
        }
        gfSpargeFactor = findPreference(getString(R.string.pref_advanced_sparge_grain)) as EditTextPreference
        gfSpargeFactor?.setOnPreferenceChangeListener { _, any ->
            getValueAsDouble(any)?.let { value ->
                gfPrefs.gfSpargeFactor = if (prefs.useMetric) value else value.galPerLbToLPerKg()
                updateWTGSummary(gfSpargeFactor, gfPrefs.gfSpargeFactor)
                true
            }
            false
        }

        mashNormalEquation = findPreference(getString(R.string.pref_advanced_mash_equation))
        mashBigEquation = findPreference(getString(R.string.pref_advanced_mash_big_equation))
        spargeEquation = findPreference(getString(R.string.pref_advanced_sparge_equation))

        findPreference(getString(R.string.pref_advanced_reset)).setOnPreferenceClickListener {
            reset()
            true
        }

        setDialogMessages()
        updateAll()
    }

    private fun getValueAsDouble(any: Any): Double? {
        try {
            return (any as String).toDouble()
        } catch (e: NumberFormatException) {
            Timber.e(e)
        }

        return null
    }

    private fun updateWTGSummary(preference: EditTextPreference?, value: Double) {
        preference?.run {
            val formatted: String
            val unit: String
            if (prefs.useMetric) {
                formatted = value.toTwoDecimalsFormat()
                unit = context.getString(R.string.unit_short_l_per_kg)
            } else {
                formatted = value.lPerKgToGalPerLb().toTwoDecimalsFormat()
                unit = context.getString(R.string.unit_short_gal_per_lb)
            }
            text = formatted
            summary = unitAsText(formatted, unit)
        }
    }

    private fun updateWTGBigFactorSummary() {
        gfWTGBigFactor?.run {
            val formatted = displayWeight(context, gfPrefs.gfWTGBigFactor, false, forceLarge = true)
            text = formatted
            val unit: String = if (prefs.useMetric) {
                context.getString(R.string.unit_short_kilograms)
            } else {
                context.getString(R.string.unit_short_pounds)
            }
            summary = unitAsText(formatted, unit)
        }
    }

    private fun updateMashAddSummary() {
        gfMashAdd?.run {
            val formatted = displayVolume(context, gfPrefs.gfMashWater, false, forceLarge = true)
            text = formatted
            val unit: String = if (prefs.useMetric) {
                context.getString(R.string.unit_short_liters)
            } else {
                context.getString(R.string.unit_short_gallons)
            }
            summary = unitAsText(formatted, unit)
        }
    }

    private fun updateEvaporationSummary() {
        gfEvaporation?.run {
            val formatted = displayVolume(context, gfPrefs.gfEvaporation, false, forceLarge = true)
            val unit = if (prefs.useMetric)
                context.getString(R.string.unit_short_l_per_h)
            else
                context.getString(R.string.unit_short_gal_per_h)
            text = formatted
            summary = unitAsText(formatted, unit)
        }
    }

    private fun updateTrubLossSummary() {
        gfTrubLoss?.run {
            val formatted = displayVolume(context, gfPrefs.gfTrubLoss, false, forceLarge = true)
            text = formatted
            val unit: String = if (prefs.useMetric) {
                context.getString(R.string.unit_short_liters)
            } else {
                context.getString(R.string.unit_short_gallons)
            }
            summary = unitAsText(formatted, unit)
        }
    }

    private fun updateEquations() {
        val mashNormal: String
        val sparge: String

        if (prefs.useMetric) {
            mashNormal = "mash_in_l = grains_in_kg * " +
                    "[water_to_grain = ${gfPrefs.gfWTGRatio.toTwoDecimalsFormat()}] + " +
                    "[mash_water_addition = ${gfPrefs.gfMashWater.toTwoDecimalsFormat()}]"

            sparge = "preboil_in_l = batch_in_liters + " +
                    "[trub_loss = ${gfPrefs.gfTrubLoss.toTwoDecimalsFormat()}] + " +
                    "[evaporation_l/h = ${gfPrefs.gfEvaporation.toTwoDecimalsFormat()}] * boil_h\n" +
                    "sparge_in_l = (preboil - mash) + (grains_in_kg * " +
                    "[sparge_grain_factor = ${gfPrefs.gfSpargeFactor.toTwoDecimalsFormat()}]"
        } else {
            mashNormal = "mash_in_gal = grains_in_lb * " +
                    "[water_to_grain = ${gfPrefs.gfWTGRatio.lPerKgToGalPerLb().toTwoDecimalsFormat()}] + " +
                    "[mash_water_addition = ${gfPrefs.gfMashWater.lToGal().toTwoDecimalsFormat()}]"

            sparge = "preboil_in_gal = batch_in_gal + " +
                    "[trub_loss = ${gfPrefs.gfTrubLoss.lToGal().toTwoDecimalsFormat()}] + " +
                    "[evaporation_gal/h = ${gfPrefs.gfEvaporation.lToGal().toTwoDecimalsFormat()}] * boil_h\n" +
                    "sparge_in_gal = (preboil - mash) + (grains_in_lb * " +
                    "[sparge_grain_factor = ${gfPrefs.gfSpargeFactor.lPerKgToGalPerLb().toTwoDecimalsFormat()}]"
        }

        mashNormalEquation?.summary = mashNormal
        spargeEquation?.summary = sparge

        if (gfPrefs.gfUseBigWTGRatio) {
            val mashBig: String
            if (prefs.useMetric) {
                mashBig = "mash_in_l = grains_in_kg * " +
                        "[water-to-grain = ${gfPrefs.gfWTGBigRatio.toTwoDecimalsFormat()}] + " +
                        "[mash_water_addition = ${gfPrefs.gfMashWater.toTwoDecimalsFormat()}]"
            } else {
                mashBig = "mash_in_gal = grains_in_lb * " +
                        "[water_to_grain = ${gfPrefs.gfWTGBigRatio.lPerKgToGalPerLb().toTwoDecimalsFormat()}] + " +
                        "[mash_water_addition = ${gfPrefs.gfMashWater.lToGal().toTwoDecimalsFormat()}]"
            }
            mashBigEquation?.summary = mashBig
        }
    }

    private fun updateAll() {
        updateWTGSummary(gfWTGRatio, gfPrefs.gfWTGRatio)
        updateWTGSummary(gfWTGBigRatio, gfPrefs.gfWTGBigRatio)
        updateWTGBigFactorSummary()

        updateMashAddSummary()
        updateEvaporationSummary()
        updateTrubLossSummary()
        updateWTGSummary(gfSpargeFactor, gfPrefs.gfSpargeFactor)

        updateEquations()
    }

    private fun reset() {
        gfPrefs.resetGrainfatherSettings()

        updateAll()
    }

    private fun setDialogMessages() {
        if (prefs.useMetric) {
            gfWTGRatio?.setDialogMessage(R.string.settings_equipment_wtgr_explain_metric)
            gfWTGBigRatio?.setDialogMessage(R.string.settings_equipment_wtgr_explain_metric)
            gfWTGBigFactor?.setDialogMessage(R.string.settings_equipment_wtgr_big_factor_explain_metric)
            gfMashAdd?.setDialogMessage(R.string.settings_equipment_mash_water_add_explain_metric)
            gfEvaporation?.setDialogMessage(R.string.settings_equipment_evaporation_explain_metric)
            gfTrubLoss?.setDialogMessage(R.string.settings_equipment_trub_loss_explain_metric)
            gfSpargeFactor?.setDialogMessage(R.string.settings_equipment_wtgr_explain_metric)
        } else {
            gfWTGRatio?.setDialogMessage(R.string.settings_equipment_wtgr_explain_imperial)
            gfWTGBigRatio?.setDialogMessage(R.string.settings_equipment_wtgr_explain_imperial)
            gfWTGBigFactor?.setDialogMessage(R.string.settings_equipment_wtgr_big_factor_explain_imperial)
            gfMashAdd?.setDialogMessage(R.string.settings_equipment_mash_water_add_explain_imperial)
            gfEvaporation?.setDialogMessage(R.string.settings_equipment_evaporation_explain_imperial)
            gfTrubLoss?.setDialogMessage(R.string.settings_equipment_trub_loss_explain_imperial)
            gfSpargeFactor?.setDialogMessage(R.string.settings_equipment_wtgr_explain_imperial)
        }
    }

    override fun onPause() {
        preferenceScreen.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences
                .registerOnSharedPreferenceChangeListener(this)
    }
}