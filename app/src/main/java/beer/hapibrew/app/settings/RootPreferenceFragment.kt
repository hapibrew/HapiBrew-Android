package beer.hapibrew.app.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import beer.hapibrew.app.R
import beer.hapibrew.app.activity.SettingsActivity
import beer.hapibrew.app.dialog.RingtoneDialog
import beer.hapibrew.app.prefs
import beer.hapibrew.app.service.RingtoneHelper
import org.jetbrains.anko.AnkoLogger
import rikka.materialpreference.ListPreference
import rikka.materialpreference.PreferenceFragment


class RootPreferenceFragment : PreferenceFragment(), SharedPreferences.OnSharedPreferenceChangeListener, AnkoLogger {
    var defaultUnits: ListPreference? = null
    var primaryEquipment: ListPreference? = null

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            getString(R.string.pref_default_unit) -> prefs.useMetric = defaultUnits?.value == "0"
            getString(R.string.pref_primary_equipment) -> prefs.isGF220V = primaryEquipment?.value == "0"
        }
    }

    override fun onCreateItemDecoration(): PreferenceFragment.DividerDecoration? {
        return CategoryDivideDividerDecoration()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        with(preferenceManager) {
//            defaultPackages = arrayOf("beer.hapibrew.app.")
            sharedPreferencesName = "settings"
            sharedPreferencesMode = Context.MODE_PRIVATE
        }
        addPreferencesFromResource(R.xml.prefs_root)

        defaultUnits = findPreference(getString(R.string.pref_default_unit)) as ListPreference
        defaultUnits?.setValueIndex(if (prefs.useMetric) 0 else 1)

        primaryEquipment = findPreference(getString(R.string.pref_primary_equipment)) as ListPreference
        primaryEquipment?.setValueIndex(if (prefs.isGF220V) 0 else 1)

        findPreference(getString(R.string.pref_equipment_advanced)).setOnPreferenceClickListener {
            (activity as SettingsActivity).openFragment(it.fragment)
            true
        }

        val alarmPreference = findPreference(getString(R.string.pref_notification_alarm))
        alarmPreference.summary = RingtoneHelper.getSavedRingtoneName(activity!!)
        alarmPreference.setOnPreferenceClickListener {
            RingtoneDialog(activity, RingtoneDialog.OnRingtoneChangedListener { name -> alarmPreference.summary = name }).show()
            true
        }
    }

    override fun onPause() {
        preferenceScreen.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences
                .registerOnSharedPreferenceChangeListener(this)
    }
}