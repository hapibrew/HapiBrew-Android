package beer.hapibrew.app.controllers.session

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.RefWatchingController
import beer.hapibrew.app.data.RecipeStep
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.dialog.StepDurationDialog
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.job.AlarmJob
import beer.hapibrew.app.ringer
import beer.hapibrew.app.service.NotificationService
import beer.hapibrew.app.session.*
import beer.hapibrew.app.verticalstepperform.VerticalStepperFormLayout
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperForm
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import kotlinx.android.synthetic.main.controller_session_steps.view.*
import timber.log.Timber

class SessionStepController : RefWatchingController, VerticalStepperForm, StepDurationDialog.OnStepDurationDialogListener {
    val sessionData:SessionData
    lateinit var ctx: Context
    lateinit var verticalStepper: VerticalStepperFormLayout
    val recipeSteps = ArrayList<RecipeStep>()
    val PROGRESS_RESOLUTION:Int = 1000

    constructor(sessionData:SessionData) : this(Bundle {
        putParcelable(BUNDLE_SESSION_DATA, sessionData)
    })
    constructor(args: Bundle) : super(args) {
        sessionData = args.getParcelable<SessionData>(BUNDLE_SESSION_DATA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        ctx = inflater.context
        return inflater.inflate(R.layout.controller_session_steps, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        verticalStepper = view.verticalStepper

        val colorPrimary = ContextCompat.getColor(ctx, R.color.colorPrimary)
        val colorPrimaryDark = ContextCompat.getColor(ctx, R.color.colorPrimaryDark)

        generateSteps()
        val initialStep = getStepNumberFromSession()
        VerticalStepperFormLayout.Builder.newInstance(verticalStepper, recipeSteps.map { it.title }.toTypedArray(), this, activity)
                .primaryColor(colorPrimary)
                .primaryDarkColor(colorPrimaryDark)
                .displayBottomNavigation(showProgress(recipeSteps[initialStep].phase))
                .materialDesignInDisabledSteps(true)
                .stepsSubtitles(recipeSteps.map { it.subtitle }.toTypedArray())
                .initialStep(initialStep)
                .init()

        verticalStepper.setProgressMax(PROGRESS_RESOLUTION)
        val isCompleted = recipeSteps[initialStep].phase == SessionData.Phase.COMPLETED
        if (!isCompleted) {
            updateProgress()
            verticalStepper.registerListeners(null, {
                verticalStepper.setProgressDone(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        activeStepDone()
                    }
                })
            })
        }
    }

    override fun onDetach(view: View) {
        super.onDetach(view)

        if (sessionData.phase.ordinal < SessionData.Phase.FERMENTATION.ordinal) {
            showNotification()
        }
    }

    private fun showNotification() {
        val serviceIntent = Intent(NotificationService.ACTION_SHOW_NOTIFICATION, null, activity, NotificationService::class.java)
        serviceIntent.putExtra(BUNDLE_SESSION_ID, sessionData.id)
        activity?.startService(serviceIntent)
    }

    private fun generateSteps() {
        recipeSteps.clear()
        recipeSteps.addAll(sessionData.buildRecipeSteps(ctx))
    }

    fun startStep():Long {
        val duration = sessionData.getStepDuration()
        sessionData.stepStartEpoch = currentEpoch()
        sessionData.stepEndEpoch = sessionData.stepStartEpoch + duration
        saveSessionDataChanges()

        updateProgress()

        setReminder()

        return duration
    }

    fun updateProgress() {
        verticalStepper.setProgressValue((sessionData.getStepProgress() * PROGRESS_RESOLUTION).toInt())
    }

    fun pauseStep() {
        sessionData.stepPauseEpoch = currentEpoch()
        saveSessionDataChanges()

        cancelReminder()
    }

    fun resumeStep():Long {
        val diff = currentEpoch() - sessionData.stepPauseEpoch
        sessionData.stepStartEpoch += diff
        sessionData.stepEndEpoch += diff
        sessionData.stepPauseEpoch = 0L

        updateProgress()

        saveSessionDataChanges()

        setReminder()

        return sessionData.stepEndEpoch - currentEpoch()
    }

    fun updateGravityValues(og:Sg = 0.0, fg:Sg = 0.0) {
        sessionData.og = og
        sessionData.fg = fg
        saveSessionDataChanges()
    }

    override fun createStep(stepNumber: Int): VerticalStepperFormStep {
        return when (recipeSteps[stepNumber].phase) {
            SessionData.Phase.PRE_MASH -> FormPreMashStep(this)
            SessionData.Phase.MASH -> FormMashStep(this)
            SessionData.Phase.SPARGE -> FormSpargeStep(this)
            SessionData.Phase.BOIL -> FormBoilStep(this)
            SessionData.Phase.PRE_FERMENTATION -> FormPreFermentStep(this)
            SessionData.Phase.FERMENTATION -> FormFermentStep(this)
            SessionData.Phase.COMPLETED -> FormCompletedStep(this)
        }
    }

    override fun onStepOpening(stepNumber: Int) {
    }

    private fun showProgress(phase:SessionData.Phase):Boolean {
        return when (phase) {
            SessionData.Phase.PRE_MASH,
            SessionData.Phase.SPARGE,
            SessionData.Phase.PRE_FERMENTATION,
            SessionData.Phase.COMPLETED
            -> false
            SessionData.Phase.MASH,
            SessionData.Phase.BOIL,
            SessionData.Phase.FERMENTATION
            -> true
        }
    }

    private fun getStepNumberFromSession():Int {
        sessionData.let {
            val currentPhase = it.phase
            val currentStep = it.step
            val index = recipeSteps.indexOfFirst { it.phase == currentPhase && it.step == currentStep }
            return if (index == -1) 0 else index
        }
    }

    fun isStepCompleted(stepNumber: Int): Boolean {
        return verticalStepper.isStepCompleted(stepNumber)
    }

    fun activeStepDone() {
        stepDone(verticalStepper.activeStepNumber)
    }
    fun stepDone(stepNumber: Int) {
        if (verticalStepper.isStepCompleted(stepNumber))
            return

        verticalStepper.setStepAsCompleted(stepNumber)

        ringer.stopRing()

        cancelReminder()
        goToNextStep(stepNumber)
    }

    private fun goToNextStep(currentStep:Int) {
        val nextStep = currentStep + 1

        if (nextStep >= recipeSteps.size) {
            Timber.e("Want to switch to step %d of %d", nextStep, recipeSteps.size)
            return
        }

        sessionData.let {
            val nextRecipeStep = recipeSteps[nextStep]
            goToNextPhase(nextRecipeStep)
        }
    }

    private fun goToNextPhase(nextStep:RecipeStep) {
        if (nextStep.phase == SessionData.Phase.COMPLETED) {
            sessionData.endEpoch = sessionData.stepEndEpoch

            verticalStepper.setStepSubtitle(verticalStepper.activeStepNumber + 1, sessionData.getSessionDurationString(ctx))
        }

        sessionData.let {
            it.phase = nextStep.phase
            it.step = nextStep.step
            it.stepStartEpoch = 0
            it.stepPauseEpoch = 0
            it.stepEndEpoch = 0
        }
        saveSessionDataChanges()

        updateProgress()

        verticalStepper.goToNextStep()
        if (showProgress(nextStep.phase))
            verticalStepper.showBottomNavigation()
        else
            verticalStepper.hideBottomNavigation()
    }

    override fun updateDuration(value: Int) {
        val duration = sessionData.getStepDuration(value.toLong())

        sessionData.stepStartEpoch = currentEpoch()
        sessionData.stepPauseEpoch = 0
        sessionData.stepEndEpoch = currentEpoch() + duration
        saveSessionDataChanges()

        updateProgress()

        setReminder()

        verticalStepper.goToStep(verticalStepper.activeStepNumber, true)
    }

    private fun saveSessionDataChanges() {
        SessionDatabase.updateSession(ctx, sessionData)
    }

    private fun setReminder() {
        AlarmJob.scheduleJob(sessionData)
    }

    private fun cancelReminder() {
        AlarmJob.cancelJobs(sessionData)
    }
}
