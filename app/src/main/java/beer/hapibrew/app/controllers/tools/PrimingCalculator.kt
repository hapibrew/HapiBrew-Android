package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.*
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.prefs
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.calculator_priming.view.*
import kotlinx.android.synthetic.main.calculator_priming_buttons.view.*
import org.jetbrains.anko.sdk25.coroutines.textChangedListener

class PrimingCalculator(val rootView: View) {
    var useMetric:Boolean = prefs.useMetric

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            switchPrimingUnits.setOnClickListener {
                useMetric = !useMetric
                setUnitButtonText(switchPrimingUnits, primingAmount, primingTemp)
                convertAmountUnit(primingAmountValue, primingAmount, useMetric)
                convertTempUnit(primingTempValue, primingTemp, useMetric)
            }

            primingAmountValue.textChangedListener {
                afterTextChanged {
                    if (isValidAmount(rootView.context, primingAmountValue.toDouble(), useMetric, primingAmount))
                        extractValues()
                }
            }
            primingVolumesValue.textChangedListener {
                afterTextChanged {
                    if (isValidVolume(rootView.context, primingVolumesValue.toDouble(), primingVolumes))
                        extractValues()
                }
            }
            primingTempValue.textChangedListener {
                afterTextChanged {
                    if (isValidTemperature(rootView.context, primingTempValue.toDouble(), useMetric, primingTemp))
                        extractValues()
                }
            }
        }
    }

    fun setDefaultValues() {
        with (rootView) {
            val amount:Double
            val temp:String
            if (useMetric) {
                amount = 19.0
                temp = "20"
            } else {
                amount = 5.0
                temp = "68"
            }
            primingAmountValue.setText(amount.toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            primingVolumesValue.setText(2.0.toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            primingTempValue.setText(temp, TextView.BufferType.EDITABLE)

            setUnitButtonText(switchPrimingUnits, primingAmount, primingTemp)


            extractValues()
        }
    }

    private fun setUnitButtonText(button: TextView, hintVolume:TextInputLayout, hintTemp:TextInputLayout) {
        val buttonUnit = if (useMetric) R.string.unit_imperial else R.string.unit_metric
        val volumeUnit = if (useMetric) R.string.unit_liter else R.string.unit_gallon
        val tempUnit = if (useMetric) R.string.unit_celsius else R.string.unit_fahrenheit
        button.text = button.context.getString(R.string.switch_to_unit, button.context.getString(buttonUnit))
        hintVolume.hint = hintVolume.context.getString(R.string.bottling_kegging_volume, hintVolume.context.getString(volumeUnit))
        hintTemp.hint = hintTemp.context.getString(R.string.priming_temperature, hintTemp.context.getString(tempUnit))
    }

    private fun extractValues() {
        with (rootView) {
            val amount:Liters = if (useMetric) primingAmountValue.toDouble() else primingAmountValue.toDouble().galToL()
            val volumes = primingVolumesValue.toDouble()
            val temp:Fahrenheit = if (useMetric) primingTempValue.toDouble().cToF() else primingTempValue.toDouble()

            if (isValidAmount(context, amount, true, primingAmount) &&
                    isValidVolume(context, volumes, primingVolumes) &&
                    isValidTemperature(context, temp, false, primingTemp))
                calculatePriming(temp, amount, volumes)
        }
    }

    private fun calculatePriming(temp:Fahrenheit, batchSize:Liters, volumes:Double) {
        with (rootView) {
            val (sucrose, dextrose, dme) = priming(temp, batchSize, volumes)
            val sucroseText:String; val dextroseText:String; val dmeText:String
            val unitText:String
            if (useMetric) {
                sucroseText = sucrose.toOneDecimalFormat()
                dextroseText = dextrose.toOneDecimalFormat()
                dmeText = dme.toOneDecimalFormat()
                unitText = context.getString(R.string.unit_short_grams)
            } else {
                sucroseText = sucrose.gToOz().toOneDecimalFormat()
                dextroseText = dextrose.gToOz().toOneDecimalFormat()
                dmeText = dme.gToOz().toOneDecimalFormat()
                unitText = context.getString(R.string.unit_short_ounces)
            }
            primingTableSugar.text = unitAsText(sucroseText, unitText)
            primingCornSugar.text = unitAsText(dextroseText, unitText)
            primingDme.text = unitAsText(dmeText, unitText)
        }
    }

    private fun convertTempUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toMetric:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidTemperature(rootView.context, currentValue, !toMetric, wrapperView)) {
            if (toMetric) {
                valueView.setText(fahrenheitToCelsius(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(celsiusToFahrenheit(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }

    private fun convertAmountUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toMetric:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidAmount(rootView.context, currentValue, !toMetric, wrapperView)) {
            if (toMetric) {
                valueView.setText(gallonsToLiters(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(litersToGallons(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }
}