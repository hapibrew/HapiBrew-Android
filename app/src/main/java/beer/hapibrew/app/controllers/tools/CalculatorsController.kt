package beer.hapibrew.app.controllers.tools

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.ToolsBaseController
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CalculatorsController : ToolsBaseController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_calculators, container, false)

        doAsync {
            val refractometerCalculator = RefractometerCalculator(v)
            val KegPressureCalculator = KegPressureCalculator(v)
            val PrimingCalculator = PrimingCalculator(v)

            uiThread {
                refractometerCalculator.setDefaultValues()
                KegPressureCalculator.setDefaultValues()
                PrimingCalculator.setDefaultValues()
            }
        }

        return v
    }
}