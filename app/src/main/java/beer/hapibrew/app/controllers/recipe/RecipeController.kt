package beer.hapibrew.app.controllers.recipe

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.*
import beer.hapibrew.app.controllers.base.RefWatchingController
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.view.NestedLinearLayoutManager
import beer.hapibrew.beerxml2proto.proto.*
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.DividerItemDecoration
import kotlinx.android.synthetic.main.content_recipe.view.*
import kotlinx.android.synthetic.main.recipe_mash_card.view.*

class RecipeController : RefWatchingController {

    val recipeData:RecipeData
    constructor(recipeData:RecipeData) : this(Bundle {
        putParcelable(BUNDLE_RECIPE_DATA, recipeData)
    })
    constructor(args:Bundle) : super(args) {
        recipeData = args.getParcelable<RecipeData>(BUNDLE_RECIPE_DATA)
    }

    var fermentableAdapter: FlexibleAdapter<FermentableItem>? = null
    var hopAdapter: FlexibleAdapter<HopItem>? = null
    var yeastAdapter: FlexibleAdapter<YeastItem>? = null
    var mashStepsAdapter: FlexibleAdapter<MashStepItem>? = null
    var fermentationAdapter: FlexibleAdapter<FermentationItem>? = null
    var miscAdapter: FlexibleAdapter<MiscItem>? = null

    lateinit var ctx: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        ctx = inflater.context
        return inflater.inflate(R.layout.content_recipe, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        showRecipeNotes(view, recipeData)
        showRecipeDetails(view, recipeData)
        showFermentables(view, Wire.get(recipeData.proto.fermentables, emptyList()))
        showHops(view, Wire.get(recipeData.proto.hops, emptyList()))
        showYeasts(view, Wire.get(recipeData.proto.yeasts, emptyList()))
        showFermentations(view, Wire.get(recipeData.proto.fermentations, emptyList()))
        showMash(view, Wire.get(recipeData.proto.mash, null))
        showMiscs(view, Wire.get(recipeData.proto.miscs, emptyList()))
        //TODO: show water profile (similar to mash)
    }

    private fun showRecipeNotes(view: View, recipe: RecipeData) {
        val notes = Wire.get(recipe.proto.notes, Recipe.DEFAULT_NOTES)
        if (notes.isNullOrBlank()) {
            view.recipeNotesCard.visibility = View.GONE
        } else {
            view.recipeNotes.text = notes
            view.recipeNotes.post {
                if (view.recipeNotes.isTextTruncated()) {
                    view.recipeNotesMore.visibility = View.VISIBLE
                } else {
                    view.recipeNotesMore.visibility = View.GONE
                }

            }
            view.recipeNotesCard.visibility = View.VISIBLE
//            view.recipeNotes.setOnClickListener { showFullNotes(view) }
            view.recipeNotesMore.setOnClickListener { showFullNotes(view) }
        }
    }

    private fun showFullNotes(view: View) {
        if (view.recipeNotes.isExpanded) {
            view.recipeNotesMore.setText(R.string.notes_show_more)
        } else {
            view.recipeNotesMore.setText(R.string.notes_show_less)
        }
        view.recipeNotes.toggle()
    }

    private fun showRecipeDetails(view: View, recipe: RecipeData) {
        view.batchVolume.text = recipe.proto.displayBatchVolume(ctx)
        view.recipeDetailedGraph.setData(recipe.og, recipe.fg, recipe.ibu, recipe.srm, recipe.abv, recipe.proto.style)
    }

    private fun showFermentables(view: View, fermentables: List<Fermentable>) {
        if (fermentables.isNotEmpty()) {
            val header = FermentableItemHeader()
            fermentableAdapter = FlexibleAdapter<FermentableItem>(fermentables.map {
                FermentableItem(it, header)
            })
            fermentableAdapter?.setDisplayHeadersAtStartUp(true)

            with (view.fermentablesRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = fermentableAdapter
            }
        } else {
            view.fermentablesSectionTitle.visibility = View.GONE
            view.fermentablesSectionCard.visibility = View.GONE
        }
    }

    private fun showHops(view: View, hops: List<Hop>) {
        if (hops.isNotEmpty()) {
            val hopHeaders = HopUse.values().map(::HopItemHeader)
            hopAdapter = FlexibleAdapter<HopItem>(hops.map {
                HopItem(it, hopHeaders[Wire.get(it.use, Hop.DEFAULT_USE)!!.ordinal])
            })
            hopAdapter?.setDisplayHeadersAtStartUp(true)

            with (view.hopsRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = hopAdapter
            }
        } else {
            view.hopsSectionTitle.visibility = View.GONE
            view.hopsSectionCard.visibility = View.GONE
        }
    }

    private fun showYeasts(view: View, yeasts: List<Yeast>) {
        if (yeasts.isNotEmpty()) {
            val header = YeastItemHeader()
            yeastAdapter = FlexibleAdapter<YeastItem>(yeasts.map {
                YeastItem(it, header)
            })
            yeastAdapter?.setDisplayHeadersAtStartUp(true)

            with (view.yeastsRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = yeastAdapter
            }
        } else {
            view.yeastsSectionTitle.visibility = View.GONE
            view.yeastsSectionCard.visibility = View.GONE
        }
    }

    private fun showMash(view: View, mash: Mash?) {
        if (mash != null) {
            view.mashProfileName.text = Wire.get(mash.name, Mash.DEFAULT_NAME)
            view.mashProfilePh.text = mash.displayPh(ctx)
            view.mashProfileSpargeTemp.text = mash.displaySpargeTemperature(ctx)
            val mashSteps = Wire.get(mash.steps, emptyList())
            val header = MashStepItemHeader()
            mashStepsAdapter = FlexibleAdapter<MashStepItem>(mashSteps.map {
                MashStepItem(it, header)
            })
            mashStepsAdapter?.setDisplayHeadersAtStartUp(true)

            with (view.mashRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = mashStepsAdapter
            }
        } else {
            view.mashSectionTitle.visibility = View.GONE
            view.mashSectionCard.visibility = View.GONE
        }
    }

    private fun showFermentations(view: View, fermentations: List<Fermentation>) {
        if (fermentations.isNotEmpty()) {
            val header = FermentationItemHeader()
            fermentationAdapter = FlexibleAdapter<FermentationItem>(fermentations.map {
                FermentationItem(it, header)
            })
            fermentationAdapter?.setDisplayHeadersAtStartUp(true)
            with (view.fermentationsRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = fermentationAdapter
            }
        } else {
            view.fermentationsSectionTitle.visibility = View.GONE
            view.fermentationsSectionCard.visibility = View.GONE
        }
    }

    private fun showMiscs(view: View, miscs: List<Misc>) {
        if (miscs.isNotEmpty()) {
            val miscHeaders = MiscUse.values().map(::MiscItemHeader)
            miscAdapter = FlexibleAdapter<MiscItem>(miscs.map {
                MiscItem(it, miscHeaders[Wire.get(it.use, Misc.DEFAULT_USE)!!.ordinal])
            })
            miscAdapter?.setDisplayHeadersAtStartUp(true)
            with (view.miscsRecyclerView) {
                itemAnimator = DefaultItemAnimator()
                layoutManager = NestedLinearLayoutManager(ctx)
                addItemDecoration(DividerItemDecoration(ctx, 0, 16))
                adapter = miscAdapter
            }
        } else {
            view.miscsSectionTitle.visibility = View.GONE
            view.miscsSectionCard.visibility = View.GONE
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        fermentableAdapter?.onSaveInstanceState(outState)
        hopAdapter?.onSaveInstanceState(outState)
        yeastAdapter?.onSaveInstanceState(outState)
        mashStepsAdapter?.onSaveInstanceState(outState)
        fermentationAdapter?.onSaveInstanceState(outState)
        miscAdapter?.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        fermentableAdapter?.onRestoreInstanceState(savedInstanceState)
        hopAdapter?.onRestoreInstanceState(savedInstanceState)
        yeastAdapter?.onRestoreInstanceState(savedInstanceState)
        mashStepsAdapter?.onRestoreInstanceState(savedInstanceState)
        fermentationAdapter?.onRestoreInstanceState(savedInstanceState)
        miscAdapter?.onRestoreInstanceState(savedInstanceState)
    }
}