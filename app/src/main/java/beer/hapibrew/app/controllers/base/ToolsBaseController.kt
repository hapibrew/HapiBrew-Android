package beer.hapibrew.app.controllers.base

import beer.hapibrew.app.R

abstract class ToolsBaseController : MainBaseController() {

    override fun getTitle(): String {
        return activity?.getString(R.string.tools) ?: ""
    }

    override fun displayTabLayout(): Boolean {
        return true
    }
}