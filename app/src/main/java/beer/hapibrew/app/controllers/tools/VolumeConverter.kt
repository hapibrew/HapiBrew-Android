package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.extensions.*
import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.converter_volume.view.*

class VolumeConverter(val rootView: View) {

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            val lTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitLitersValue.toDouble()
                    unitGallonsValue.setText(v.lToGal().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }
            val galTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitGallonsValue.toDouble()
                    unitLitersValue.setText(v.galToL().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }

            unitLitersValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(lTextChangedListener)
                    else
                        v.removeTextChangedListener(lTextChangedListener)
                }
            }
            unitGallonsValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(galTextChangedListener)
                    else
                        v.removeTextChangedListener(galTextChangedListener)
                }
            }
        }
    }
}