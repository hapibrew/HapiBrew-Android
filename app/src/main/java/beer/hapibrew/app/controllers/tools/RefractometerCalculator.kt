package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.brixToSg
import beer.hapibrew.app.calculators.fixRefractometer
import beer.hapibrew.app.calculators.sgToBrix
import beer.hapibrew.app.extensions.*
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.calculator_refractometer.view.*
import kotlinx.android.synthetic.main.calculator_refractometer_buttons.view.*
import org.jetbrains.anko.sdk25.coroutines.textChangedListener

class RefractometerCalculator(val rootView:View) {
    var useSg:Boolean = true
    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            switchRefractometerUnits.setOnClickListener {
                useSg = !useSg
                setUnitButtonText(switchRefractometerUnits, refractometerOg, refractometerFg)
                convertGravityUnit(refractometerFgValue, refractometerFg, useSg)
                convertGravityUnit(refractometerOgValue, refractometerOg, useSg)
            }

            refractometerOgValue.textChangedListener {
                afterTextChanged {
                    if (isValidGravity(rootView.context, refractometerOgValue.toDouble(), useSg, refractometerOg))
                        extractValues()
                }
            }
            refractometerFgValue.textChangedListener {
                afterTextChanged {
                    if (isValidGravity(rootView.context, refractometerFgValue.toDouble(), useSg, refractometerFg))
                        extractValues()
                }
            }
            refractometerFactorValue.textChangedListener {
                afterTextChanged {
                    if (isValidFactor(rootView.context, refractometerFactorValue.toDouble(), refractometerFactor))
                        extractValues()
                }
            }
        }
    }

    fun setDefaultValues() {
        with (rootView) {
            refractometerOgValue.setText(1.068.toSgFormat(), TextView.BufferType.EDITABLE)
            refractometerFgValue.setText(1.032.toSgFormat(), TextView.BufferType.EDITABLE)
            refractometerFactorValue.setText(1.04.toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)

            setUnitButtonText(switchRefractometerUnits, refractometerOg, refractometerFg)

            extractValues()
        }
    }

    private fun setUnitButtonText(button: TextView, hintOg:TextInputLayout, hintFg:TextInputLayout) {
        val currentUnit = if (useSg) R.string.unit_sg else R.string.unit_brix
        val inverseUnit = if (useSg) R.string.unit_brix else R.string.unit_sg
        button.text = button.context.getString(R.string.switch_to_unit, button.context.getString(inverseUnit))
        hintOg.hint = hintOg.context.getString(R.string.original_gravity, hintOg.context.getString(currentUnit))
        hintFg.hint = hintFg.context.getString(R.string.final_gravity, hintFg.context.getString(currentUnit))
    }

    private fun extractValues() {
        with (rootView) {
            val og = if (useSg) refractometerOgValue.toDouble() else refractometerOgValue.toDouble().bxToSg()
            val fg = if (useSg) refractometerFgValue.toDouble() else refractometerFgValue.toDouble().bxToSg()

            val factor = refractometerFactorValue.toDouble()
            if (isValidGravity(context, og, true, refractometerOg) &&
                    isValidGravity(context, fg, true, refractometerFg) &&
                    isValidFactor(context, factor, refractometerFactor))
                calculateRefractometer(og, fg, factor)
        }
    }

    private fun calculateRefractometer(og:Sg, fg:Sg, factor:Double) {
        with (rootView) {
            val (cog, cfg, abv) = fixRefractometer(og, fg, factor)
            refractometerCorrectedOgSg.text = unitAsText(cog.toSgFormat(), context.getString(R.string.unit_sg))
            refractometerCorrectedOgBx.text = unitAsText(sgToBrix(cog).toBrixFormat(), context.getString(R.string.unit_brix))
            refractometerCorrectedFgSg.text = unitAsText(cfg.toSgFormat(), context.getString(R.string.unit_sg))
            refractometerCorrectedFgBx.text = unitAsText(sgToBrix(cfg).toBrixFormat(), context.getString(R.string.unit_brix))
            refractometerAbv.text = unitAsText(abv.toOneDecimalFormat(), "%")
        }
    }

    private fun convertGravityUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toSg:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidGravity(rootView.context, currentValue, !toSg, wrapperView)) {
            if (toSg) {
                valueView.setText(brixToSg(currentValue).toSgFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(sgToBrix(currentValue).toBrixFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }
}