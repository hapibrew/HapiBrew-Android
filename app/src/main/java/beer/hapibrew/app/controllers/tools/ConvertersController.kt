package beer.hapibrew.app.controllers.tools

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.ToolsBaseController
import org.jetbrains.anko.doAsync

class ConvertersController : ToolsBaseController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_converters, container, false)

        doAsync {
            GravityConverter(v)
            VolumeConverter(v)
            WeightConverter(v)
            TemperatureConverter(v)
        }

        return v
    }

}