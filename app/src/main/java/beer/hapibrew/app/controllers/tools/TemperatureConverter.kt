package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.extensions.*
import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.converter_temperature.view.*
import kotlinx.android.synthetic.main.converter_volume.view.*

class TemperatureConverter(val rootView: View) {

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            val cTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitCelsiusValue.toDouble()
                    unitFahrenheitValue.setText(v.cToF().toOneDecimalFormat(), TextView.BufferType.EDITABLE)
                }
            }
            val fTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitFahrenheitValue.toDouble()
                    unitCelsiusValue.setText(v.fToC().toOneDecimalFormat(), TextView.BufferType.EDITABLE)
                }
            }

            unitCelsiusValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(cTextChangedListener)
                    else
                        v.removeTextChangedListener(cTextChangedListener)
                }
            }
            unitFahrenheitValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(fTextChangedListener)
                    else
                        v.removeTextChangedListener(fTextChangedListener)
                }
            }
        }
    }
}