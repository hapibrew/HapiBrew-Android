package beer.hapibrew.app.controllers.main

import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.ToolsBaseController
import beer.hapibrew.app.controllers.tools.CalculatorsController
import beer.hapibrew.app.controllers.tools.ConvertersController
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import kotlinx.android.synthetic.main.controller_tools.view.*

class ToolsController : ToolsBaseController() {

    var viewPager:ViewPager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_tools, container, false)

        viewPager = v.viewPager

        val pagerAdapter = object : RouterPagerAdapter(this) {
            override fun configureRouter(router: Router, position: Int) {
                if (!router.hasRootController()) {
                    router.setRoot(RouterTransaction.with(getItem(position)))
                }
            }

            private fun getItem(position: Int): Controller {
                if (position == 0)
                    return CalculatorsController()
                else
                    return ConvertersController()
            }

            override fun getCount(): Int {
                return 2
            }

            override fun getPageTitle(position: Int): CharSequence {
                if (position == 0)
                    return activity?.getString(R.string.calculators) ?: ""
                else
                    return activity?.getString(R.string.converters) ?: ""
            }

        }

        viewPager?.adapter = pagerAdapter
        getTabLayout().setupWithViewPager(viewPager)
        return v
    }

    override fun onDestroyView(view: View) {
        if (!(activity?.isChangingConfigurations ?: false)) {
            viewPager?.adapter = null
        }
        super.onDestroyView(view)
    }
}