package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.extensions.*
import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.converter_gravity.view.*

class GravityConverter(val rootView: View) {

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            val sgTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = gravitySgValue.toDouble()
                    if (isValidGravity(rootView.context, v, true, gravitySg)) {
                        gravityBrixValue.setText(v.sgToBx().toBrixFormat(), TextView.BufferType.EDITABLE)
                        gravityPlatoValue.setText(v.sgToPlato().toPlatoFormat(), TextView.BufferType.EDITABLE)
                    }
                }
            }
            val bxTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = gravityBrixValue.toDouble()
                    if (isValidGravity(rootView.context, v, false, gravityBrix)) {
                        gravitySgValue.setText(v.bxToSg().toSgFormat(), TextView.BufferType.EDITABLE)
                        gravityPlatoValue.setText(v.bxToPlato().toPlatoFormat(), TextView.BufferType.EDITABLE)
                    }
                }
            }
            val platoTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = gravityPlatoValue.toDouble()
                    if (isValidGravity(rootView.context, v, false, gravityPlato)) {
                        gravitySgValue.setText(v.platoToSg().toSgFormat(), TextView.BufferType.EDITABLE)
                        gravityBrixValue.setText(v.platoToBx().toBrixFormat(), TextView.BufferType.EDITABLE)
                    }
                }
            }

            gravitySgValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(sgTextChangedListener)
                    else
                        v.removeTextChangedListener(sgTextChangedListener)
                }
            }
            gravityBrixValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(bxTextChangedListener)
                    else
                        v.removeTextChangedListener(bxTextChangedListener)
                }
            }
            gravityPlatoValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(platoTextChangedListener)
                    else
                        v.removeTextChangedListener(platoTextChangedListener)
                }
            }
        }
    }
}