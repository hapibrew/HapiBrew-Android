package beer.hapibrew.app.controllers.session

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.base.RefWatchingController
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.extensions.Bundle
import kotlinx.android.synthetic.main.controller_notes.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class NotesController : RefWatchingController {

    val sessionId:Long

    constructor(sessionId:Long) : this(Bundle {
        putLong(BUNDLE_SESSION_ID, sessionId)
    })
    constructor(args: Bundle) : super(args) {
        sessionId = args.getLong(BUNDLE_SESSION_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.controller_notes, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

//        mapOf(
//                view.action_h1 to EditorTextStyle.H1,
//                view.action_h2 to EditorTextStyle.H2,
//                view.action_h3 to EditorTextStyle.H3,
//                view.action_bold to EditorTextStyle.BOLD,
//                view.action_italic to EditorTextStyle.ITALIC,
//                view.action_indent to EditorTextStyle.INDENT,
//                view.action_outdent to EditorTextStyle.OUTDENT
//        ).forEach { view, tag ->
//            view.tag = tag
//            view.typeface = iconFont
//            view.setOnClickListener(tagClickListener)
//        }
//
//        view.action_bulleted.typeface = iconFont
//        view.action_bulleted.setOnClickListener {
//            view.notesEditor.InsertList(false)
//        }
//
//        view.action_unordered_numbered.typeface = iconFont
//        view.action_unordered_numbered.setOnClickListener {
//            view.notesEditor.InsertList(true)
//        }
//
//        view.action_hr.typeface = iconFont
//        view.action_hr.setOnClickListener {
//            view.notesEditor.InsertDivider()
//        }
//
//        view.notesEditor.setDividerLayout(R.layout.tmpl_divider_layout)
//        view.notesEditor.setListItemLayout(R.layout.tmpl_list_item)
//
//        view.notesEditor.setFontFace(fontFace)
//        val notes = getNotes()
//        if (notes.isNullOrBlank())
//            view.notesEditor.Render()
//        else
//            view.notesEditor.Render(notes)

        doAsync {
            val fontFace = R.string.icarus_font_face
            val iconFont = Typeface.createFromAsset(view.context.assets, view.context.getString(fontFace))
            val notes = getNotes()
            uiThread {
                view.editText.typeface = iconFont
                view.editText.setText(notes, TextView.BufferType.EDITABLE)
            }
        }
    }

//    private val tagClickListener = View.OnClickListener { v ->
//        view?.notesEditor?.UpdateTextStyle(v.tag as EditorTextStyle)
//    }

    override fun onDetach(view: View) {
        updateNotes()

        super.onDetach(view)
    }

    private fun getNotes():String {
        return SessionDatabase.getNotes(activity, sessionId) ?: ""
    }

    private fun updateNotes() {
//        val editor = view?.notesEditor ?: return
        val editor = view?.editText ?: return
        SessionDatabase.updateNotes(activity, sessionId, editor.text.toString())
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
    }
}