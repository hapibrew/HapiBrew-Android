package beer.hapibrew.app.controllers.base

import android.os.Bundle
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType

abstract class RefWatchingController : Controller {
    private var hasExited: Boolean = false

    constructor() : super()
    constructor(args: Bundle?) : super(args)

    public override fun onDestroy() {
        super.onDestroy()

//        if (hasExited) {
//            BeerApplication.refWatcher?.watch(this)
//        }
    }

    override fun onChangeEnded(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        super.onChangeEnded(changeHandler, changeType)

        hasExited = !changeType.isEnter
//        if (isDestroyed) {
//            BeerApplication.refWatcher?.watch(this)
//        }
    }
}