package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.R
import beer.hapibrew.app.activity.KegPressureActivity
import beer.hapibrew.app.calculators.celsiusToFahrenheit
import beer.hapibrew.app.calculators.fahrenheitToCelsius
import beer.hapibrew.app.calculators.kegPressure
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.prefs
import android.content.Intent
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.calculator_keg_pressure.view.*
import kotlinx.android.synthetic.main.calculator_keg_pressure_buttons.view.*
import org.jetbrains.anko.sdk25.coroutines.textChangedListener

class KegPressureCalculator(val rootView: View) {
    var useCelisus:Boolean = prefs.useMetric

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            showKegChart.setOnClickListener {
                context.startActivity(Intent(context, KegPressureActivity::class.java))
            }

            switchDegrees.setOnClickListener {
                useCelisus = !useCelisus
                setUnitButtonText(switchDegrees, kegTemp)
                convertTempUnit(kegTempValue, kegTemp, useCelisus)
            }
            kegVolumesValue.textChangedListener {
                afterTextChanged {
                    if (isValidVolume(rootView.context, kegVolumesValue.toDouble(), kegVolumes))
                        extractValues()
                }
            }
            kegTempValue.textChangedListener {
                afterTextChanged {
                    if (isValidTemperature(rootView.context, kegTempValue.toDouble(), useCelisus, kegTemp))
                        extractValues()
                }
            }
        }
    }

    fun setDefaultValues() {
        with (rootView) {
            val temp:String
            if (useCelisus) {
                temp = "5"
            } else {
                temp = "36"
            }
            kegVolumesValue.setText(2.0.toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            kegTempValue.setText(temp, TextView.BufferType.EDITABLE)

            setUnitButtonText(switchDegrees, kegTemp)

            extractValues()
        }
    }

    private fun setUnitButtonText(button: TextView, hintLayout:TextInputLayout) {
        val currentUnit = if (useCelisus) R.string.unit_celsius else R.string.unit_fahrenheit
        val inverseUnit = if (useCelisus) R.string.unit_fahrenheit else R.string.unit_celsius
        button.text = button.context.getString(R.string.switch_to_unit, button.context.getString(inverseUnit))
        hintLayout.hint = hintLayout.context.getString(R.string.keg_temperature, hintLayout.context.getString(currentUnit))
    }

    private fun extractValues() {
        with (rootView) {
            val volumes = kegVolumesValue.toDouble()
            val temp:Fahrenheit = if (useCelisus) kegTempValue.toDouble().cToF() else kegTempValue.toDouble()

            if (isValidVolume(context, volumes, kegVolumes) &&
                    isValidTemperature(context, temp, false, kegTemp))
                calculatePressure(temp, volumes)
        }
    }

    private fun calculatePressure(temp:Fahrenheit, volumes:Double) {
        with (rootView) {
            val psi = kegPressure(temp, volumes)
            kegRegulatorPressureBar.text = unitAsText(
                    psi.psiToBar().toTwoDecimalsFormat(), context.getString(R.string.unit_bar))
            kegRegulatorPressurePsi.text = unitAsText(psi.toOneDecimalFormat(), context.getString(R.string.unit_psi))
        }
    }

    private fun convertTempUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toCelsius:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidTemperature(rootView.context, currentValue, !toCelsius, wrapperView)) {
            if (toCelsius) {
                valueView.setText(fahrenheitToCelsius(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(celsiusToFahrenheit(currentValue).toOneDecimalFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }
}