package beer.hapibrew.app.controllers.main

import android.app.Activity
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.*
import beer.hapibrew.app.R
import beer.hapibrew.app.activity.SessionActivity
import beer.hapibrew.app.adapter.items.session.SessionActiveItem
import beer.hapibrew.app.adapter.items.session.SessionCompletedItem
import beer.hapibrew.app.adapter.items.session.SessionItem
import beer.hapibrew.app.adapter.items.session.SessionItemHeader
import beer.hapibrew.app.controllers.base.MainBaseController
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.database.sessionDB
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.job.AlarmJob
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.SelectableAdapter
import eu.davidea.flexibleadapter.helpers.ActionModeHelper
import kotlinx.android.synthetic.main.controller_sessions.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SessionControllers : MainBaseController() {

    private var actionModeHelper: ActionModeHelper? = null

    val loadedSessions: MutableList<SessionData> = ArrayList()
    var adapter: eu.davidea.flexibleadapter.FlexibleAdapter<SessionItem>? = null

    var snackbarView:View? = null

    var undoSnackBar: Snackbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_sessions, container, false)

        snackbarView = v.rootView

        v.emptyView?.visibility = View.GONE
        v.recyclerView?.run {
            visibility = View.GONE
            itemAnimator = DefaultItemAnimator()
            layoutManager = GridLayoutManager(activity, 2)
            (layoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (adapter.getItemViewType(position)) {
                        R.layout.session_header_item -> 2
                        else -> 1
                    }
                }
            }
            addItemDecoration(beer.hapibrew.app.view.ItemOffsetDecoration(activity!!, R.dimen.recipe_item_offset))
        }

        loadData()

        return v
    }

    private fun loadData() {
        doAsync {
            val wasError = !loadSessions()

            uiThread {
                if (wasError && snackbarView != null) {
                    Snackbar.make(snackbarView!!, "Error loading sessions!", Snackbar.LENGTH_LONG)
                            .setAction("DELETE ALL", { SessionDatabase.deleteAllSessions(activity) })
//                            .setAction("DELETE ALL", { activity?.deleteDatabase("Session.db") })
                            .show()
                }
                if (loadedSessions.isNotEmpty()) {
                    val activeHeader = SessionItemHeader(activity!!.getString(R.string.active_sessions))
                    val completedHeader = SessionItemHeader(activity!!.getString(R.string.completed_sessions))
                    if (adapter == null) {
                        createAdapter(loadedSessions.map {
                            if (it.phase == SessionData.Phase.COMPLETED) {
                                SessionCompletedItem(it, completedHeader)
                            } else {
                                SessionActiveItem(it, activeHeader)
                            }
                        })
                        view?.recyclerView?.adapter = adapter
                    } else {
                        adapter?.updateDataSet(loadedSessions.map {
                            if (it.phase == SessionData.Phase.COMPLETED) {
                                SessionCompletedItem(it, completedHeader)
                            } else {
                                SessionActiveItem(it, activeHeader)
                            }
                        }, true)
                    }
                } else {
                    updateEmptyView(0)
                }
            }
        }
    }

    override fun onActivityResumed(activity: Activity) {
        super.onActivityResumed(activity)

        if (activity.sessionDB.hasModified) {
            loadData()
            activity.sessionDB.hasModified = false
        }
    }

    private fun loadSessions():Boolean {
        loadedSessions.clear()
        try {
            loadedSessions.addAll(SessionDatabase.getSessions(activity).sortedBy { it.phase.ordinal })
            return true
        } catch (e:Exception) {
            println(e)
        }

        return false
    }

    private fun createAdapter(items:List<SessionItem>) {
        val listeners = object : FlexibleAdapter.OnItemClickListener,
                FlexibleAdapter.OnItemLongClickListener,
                FlexibleAdapter.OnUpdateListener {
            override fun onUpdateEmptyView(size: Int) {
                updateEmptyView(size)
            }

            override fun onItemClick(position: Int): Boolean {
                if (adapter?.selectedItemCount == 0) {
                    val intent = Intent(activity, SessionActivity::class.java)
                    val item = adapter?.getItem(position)
                    if (item != null) {
                        intent.putExtra(BUNDLE_SESSION_ID, item.session.id)
                        startActivity(intent)
                    }
                    return false
                } else {
                    actionModeHelper?.toggleSelection(position)
                    return true
                }
            }

            override fun onItemLongClick(position: Int) {
                actionModeHelper?.onLongClick(activity as AppCompatActivity, position)
            }
        }
        adapter = FlexibleAdapter<SessionItem>(items, listeners, true)
        adapter?.isPermanentDelete = true
        adapter?.setDisplayHeadersAtStartUp(true)
        createActionModeHelper()
    }

    private fun createActionModeHelper() {
        val actionModeCallback = object : ActionMode.Callback {
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                if (item == null)
                    return true

                if (item.itemId == R.id.action_delete) {
                    deleteSessions()
                } else if (item.itemId == R.id.action_select_all) {
                    adapter?.selectAll()
                    actionModeHelper?.updateContextTitle(adapter?.selectedItemCount ?: 0)
                }

                return true
            }

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                adapter?.mode = SelectableAdapter.MODE_MULTI
                return true
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }

        }
        actionModeHelper = object : ActionModeHelper(adapter!!, R.menu.menu_recipes, actionModeCallback) {
            override fun updateContextTitle(count: Int) {
                super.updateContextTitle(count)
//                val title = resources.getQuantityString(R.plurals.chat_selected_actions_title, count, count)
//                mActionMode.title = title
            }
        }
    }

    private fun deleteSessions() {
        val selectedItemsPositions = adapter?.selectedPositions ?: emptyList()
        val selectedItems = selectedItemsPositions.map { adapter!!.getItem(it).session }
        if (selectedItems.isEmpty())
            return

        doAsync {
            SessionDatabase.deleteSessions(activity, selectedItems)

            selectedItems.forEach { AlarmJob.cancelJobs(it) }

//            JobManager.instance().getAllJobRequestsForTag()

            uiThread {
                actionModeHelper?.destroyActionModeIfCan()

                undoSnackBar?.dismiss()
                adapter?.removeItems(selectedItemsPositions)

                undoSnackBar = Snackbar.make(snackbarView!!, R.string.recipes_deleted, 6000)
                        .setAction(R.string.undo, {
                            doAsync {
                                SessionDatabase.restoreSessions(activity, selectedItems)
                                uiThread {
                                    val activeHeader = SessionItemHeader(activity!!.getString(R.string.active_sessions))
                                    val completedHeader = SessionItemHeader(activity!!.getString(R.string.completed_sessions))
                                    adapter?.updateDataSet(loadedSessions.map {
                                        if (it.phase == SessionData.Phase.COMPLETED) {
                                            SessionCompletedItem(it, completedHeader)
                                        } else {
                                            SessionActiveItem(it, activeHeader)
                                        }
                                    }, true)
                                }
                            }
                        })
                undoSnackBar?.show()
            }
        }
    }

    private fun updateEmptyView(size: Int) {
        if (size > 0) {
            view?.emptyView?.visibility = View.GONE
            view?.recyclerView?.visibility = View.VISIBLE
        } else {
            view?.emptyView?.visibility = View.VISIBLE
            view?.recyclerView?.visibility = View.GONE
        }
    }

    override fun onDestroyView(view: View) {
        actionModeHelper?.destroyActionModeIfCan()
        actionModeHelper = null
        undoSnackBar?.dismiss()
        undoSnackBar = null

        adapter = null

        super.onDestroyView(view)
    }

    override fun getTitle(): String {
        return activity?.getString(R.string.sessions) ?: ""
    }
}
