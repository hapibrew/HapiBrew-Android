package beer.hapibrew.app.controllers.tools

import beer.hapibrew.app.extensions.*
import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.converter_weight.view.*

class WeightConverter(val rootView: View) {

    init {
        initLayout()
    }

    private fun initLayout() {
        with (rootView) {
            val gTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitGramsValue.toDouble()
                    unitKilogramsValue.setText(v.gToKg().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitOuncesValue.setText(v.gToOz().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitPoundsValue.setText(v.gToLb().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }
            val kgTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitKilogramsValue.toDouble()
                    unitGramsValue.setText(v.kgToG().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitOuncesValue.setText(v.kgToOz().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitPoundsValue.setText(v.kgToLb().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }
            val ozTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitOuncesValue.toDouble()
                    unitGramsValue.setText(v.ozToG().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitKilogramsValue.setText(v.ozToKg().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitPoundsValue.setText(v.ozToLb().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }
            val lbTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    val v = unitPoundsValue.toDouble()
                    unitGramsValue.setText(v.lbToG().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitKilogramsValue.setText(v.lbToKg().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                    unitOuncesValue.setText(v.lbToOz().toTwoDecimalsFormat(), TextView.BufferType.EDITABLE)
                }
            }

            unitGramsValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(gTextChangedListener)
                    else
                        v.removeTextChangedListener(gTextChangedListener)
                }
            }
            unitKilogramsValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(kgTextChangedListener)
                    else
                        v.removeTextChangedListener(kgTextChangedListener)
                }
            }
            unitOuncesValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(ozTextChangedListener)
                    else
                        v.removeTextChangedListener(ozTextChangedListener)
                }
            }
            unitPoundsValue.setOnFocusChangeListener { v, hasFocus ->
                (v as TextInputEditText).apply {
                    if (hasFocus)
                        v.addTextChangedListener(lbTextChangedListener)
                    else
                        v.removeTextChangedListener(lbTextChangedListener)
                }
            }
        }
    }
}