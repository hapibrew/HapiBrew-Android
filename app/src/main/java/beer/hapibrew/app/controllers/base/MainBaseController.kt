package beer.hapibrew.app.controllers.base

import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.view.View
import beer.hapibrew.app.activity.MainActivity
import kotlinx.android.synthetic.main.app_bar_main.*

abstract class MainBaseController : RefWatchingController() {

    fun getTabLayout():TabLayout {
        return (activity as MainActivity).tabLayout
    }

    fun getFab():FloatingActionButton {
        return (activity as MainActivity).fab
    }

    override fun onAttach(view: View) {
        setTitle()
        showTabLayout()
        showFab()
        super.onAttach(view)
    }

    fun setTitle() {
        (activity as MainActivity).title = getTitle()
    }

    open fun getTitle():String {
        return ""
    }

    open fun displayTabLayout():Boolean {
        return false
    }
    fun showTabLayout() {
        getTabLayout().visibility = if (displayTabLayout())
            View.VISIBLE
        else
            View.GONE
    }

    open fun displayFab():Boolean {
        return false
    }
    fun showFab() {
        if (displayFab()) {
            getFab().show()
        } else {
            getFab().hide()
        }
    }
}