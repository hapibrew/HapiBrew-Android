package beer.hapibrew.app.controllers.main

import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.*
import beer.hapibrew.app.R
import beer.hapibrew.app.activity.RecipeActivity
import beer.hapibrew.app.adapter.items.recipe.RecipeItem
import beer.hapibrew.app.controllers.base.MainBaseController
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.database.RecipeDatabase
import beer.hapibrew.app.extensions.BUNDLE_RECIPE_DATA
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.SelectableAdapter
import eu.davidea.flexibleadapter.helpers.ActionModeHelper
import kotlinx.android.synthetic.main.controller_recipes.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class RecipesController : MainBaseController() {

    override fun getTitle(): String {
        return activity?.getString(R.string.recipes) ?: ""
    }

    override fun displayFab(): Boolean {
        return true
    }

    private var actionModeHelper: ActionModeHelper? = null

    val loadedRecipes: MutableList<RecipeData> = java.util.ArrayList()
    var adapter: eu.davidea.flexibleadapter.FlexibleAdapter<RecipeItem>? = null

    var snackbarView:View? = null

    var undoSnackBar:Snackbar? = null

//    var appBar:View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_recipes, container, false)!!

        snackbarView = v.rootView

        v.emptyView?.visibility = View.GONE
        v.recyclerView?.run {
            visibility = View.GONE
            itemAnimator = DefaultItemAnimator()
            layoutManager = GridLayoutManager(activity, 2)
            addItemDecoration(beer.hapibrew.app.view.ItemOffsetDecoration(activity!!, R.dimen.recipe_item_offset))
        }

//        appBar = (activity as MainActivity).app_bar
//        v.recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//                if (recyclerView == null)
//                    return
//                (recyclerView.layoutManager as GridLayoutManager).let {
//                    if (it.findFirstCompletelyVisibleItemPosition() > 0) {
//                        ViewCompat.setElevation(appBar, 6f)
//                    } else {
//                        ViewCompat.setElevation(appBar, 0f)
//                    }
//                }
//            }
//        })

        return v
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        doAsync {
            val wasError = !loadRecipes()

            uiThread {
                if (wasError && snackbarView != null) {
                    Snackbar.make(snackbarView!!, "Error loading recipes!", Snackbar.LENGTH_LONG)
                            .setAction("DELETE ALL", { RecipeDatabase.deleteAllRecipes(activity) })
                            .show()
                }
                if (loadedRecipes.isNotEmpty()) {
                    createAdapter(loadedRecipes.map(::RecipeItem))
                    view.recyclerView?.adapter = adapter
                } else {
                    updateEmptyView(0)
                }
            }
        }
    }

    private fun loadRecipes():Boolean {
        loadedRecipes.clear()
        try {
            loadedRecipes.addAll(RecipeDatabase.getRecipes(activity))
            return true
        } catch (e:Exception) {
            println(e)
        }

        return false
    }

    private fun createAdapter(items:List<RecipeItem>) {
        val listeners = object : FlexibleAdapter.OnItemClickListener,
                FlexibleAdapter.OnItemLongClickListener,
                FlexibleAdapter.OnUpdateListener {
            override fun onUpdateEmptyView(size: Int) {
                updateEmptyView(size)
            }

            override fun onItemClick(position: Int): Boolean {
                if (adapter?.selectedItemCount == 0) {
                    val intent = Intent(activity, RecipeActivity::class.java)
                    intent.putExtra(BUNDLE_RECIPE_DATA, adapter?.getItem(position)?.recipe)
                    startActivity(intent)
                    return false
                } else {
                    actionModeHelper?.toggleSelection(position)
                    return true
                }
            }

            override fun onItemLongClick(position: Int) {
                actionModeHelper?.onLongClick(activity as AppCompatActivity, position)

                if (adapter?.selectedItemCount != 0) {
                    showFab(false)
                }
            }
        }
        adapter = FlexibleAdapter<RecipeItem>(items, listeners, true)
        adapter?.isPermanentDelete = true
        createActionModeHelper()
    }

    private fun createActionModeHelper() {
        val actionModeCallback = object : ActionMode.Callback {
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                if (item == null)
                    return true

                if (item.itemId == R.id.action_delete) {
                    deleteRecipes()
                } else if (item.itemId == R.id.action_select_all) {
                    adapter?.selectAll()
                    actionModeHelper?.updateContextTitle(adapter?.selectedItemCount ?: 0)
                }

                return true
            }

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                adapter?.mode = SelectableAdapter.MODE_MULTI
                return true
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
                if (!isBeingDestroyed && !isDestroyed)
                    showFab(true)
            }
        }

        actionModeHelper = object : ActionModeHelper(adapter!!, R.menu.menu_recipes, actionModeCallback) {
            override fun updateContextTitle(count: Int) {
                super.updateContextTitle(count)
//                val title = resources.getQuantityString(R.plurals.chat_selected_actions_title, count, count)
//                mActionMode.title = title
            }
        }
    }

    private fun deleteRecipes() {
        val selectedItemsPositions = adapter?.selectedPositions ?: emptyList()
        val selectedItems = selectedItemsPositions.map { adapter!!.getItem(it).recipe }
        if (selectedItems.isEmpty())
            return

        doAsync {
            RecipeDatabase.deleteRecipes(activity, selectedItems)

            uiThread {
                actionModeHelper?.destroyActionModeIfCan()

                undoSnackBar?.dismiss()
                adapter?.removeItems(selectedItemsPositions)

                undoSnackBar = Snackbar.make(snackbarView!!, R.string.recipes_deleted, 6000)
                        .setAction(R.string.undo, {
                            doAsync {
                                RecipeDatabase.restoreRecipes(activity, selectedItems)
                                uiThread {
                                    adapter?.updateDataSet(loadedRecipes.map(::RecipeItem), true)
                                }
                            }
                        })
                undoSnackBar?.show()
            }
        }
    }

    private fun updateEmptyView(size: Int) {
        if (size > 0) {
            view?.emptyView?.visibility = View.GONE
            view?.recyclerView?.visibility = View.VISIBLE
        } else {
            view?.emptyView?.visibility = View.VISIBLE
            view?.recyclerView?.visibility = View.GONE
        }
    }

    override fun onDestroyView(view: View) {
        actionModeHelper?.destroyActionModeIfCan()
        actionModeHelper = null
        undoSnackBar?.dismiss()
        undoSnackBar = null

        adapter = null

        super.onDestroyView(view)
    }

    private fun showFab(show:Boolean) {
        if (show)
            getFab().show()
        else
            getFab().hide()
    }
}