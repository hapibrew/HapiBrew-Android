package beer.hapibrew.app.controllers.session

import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.activity.SessionActivity
import beer.hapibrew.app.controllers.base.RefWatchingController
import beer.hapibrew.app.controllers.recipe.RecipeController
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.extensions.Bundle
import beer.hapibrew.app.service.NotificationService
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import kotlinx.android.synthetic.main.activity_session.*
import kotlinx.android.synthetic.main.controller_tools.view.*
import timber.log.Timber


class SessionController : RefWatchingController {

    val sessionId: Long

    var pagerAdapter:RouterPagerAdapter? = null

    constructor(sessionId: Long) : this(Bundle {
        putLong(BUNDLE_SESSION_ID, sessionId)
    })

    constructor(args: Bundle) : super(args) {
        sessionId = args.getLong(BUNDLE_SESSION_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_session, container, false)

        val sessionData = SessionDatabase.getSession(activity, sessionId) ?: return v

        activity?.title = sessionData.name

        pagerAdapter = object : RouterPagerAdapter(this) {
            override fun configureRouter(router: Router, position: Int) {
                if (!router.hasRootController()) {
                    router.setRoot(RouterTransaction.with(getItem(position)).tag(position.toString()))
                }
            }

            private fun getItem(position: Int): Controller {
                return when (position) {
                    0 -> SessionStepController(sessionData)
                    1 -> NotesController(sessionId)
                    2 -> RecipeController(RecipeData(0, sessionData.recipe))
                    else -> SessionStepController(sessionData)
                }
            }

            override fun getCount(): Int {
                return 3
            }

            override fun getPageTitle(position: Int): CharSequence {
                return when (position) {
                    0 -> activity?.getString(R.string.session_tab_steps) ?: ""
                    1 -> activity?.getString(R.string.session_tab_notes) ?: ""
                    2 -> activity?.getString(R.string.session_tab_recipe) ?: ""
                    else -> ""
                }
            }

        }

        v.viewPager.adapter = pagerAdapter
        getTabLayout().setupWithViewPager(v.viewPager)
        return v
    }

    private fun getTabLayout(): TabLayout {
        return (activity as SessionActivity).tabLayout
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        removeNotification()
    }

    private fun removeNotification() {
        val serviceIntent = Intent(NotificationService.ACTION_REMOVE_NOTIFICATION, null, activity, NotificationService::class.java)
        serviceIntent.putExtra(BUNDLE_SESSION_ID, sessionId)
        activity?.startService(serviceIntent)
    }

//    private fun stepDone() {
//        val viewPager = view?.viewPager ?: return
//
//        try {
//            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//            val r = RingtoneManager.getRingtone(applicationContext, notification)
//            r.play()
//        } catch (e : Exception) {
//            Timber.e(e, "Can't play notification sound")
//        }
//        (pagerAdapter?.getRouter(0)?.getControllerWithTag("0") as? SessionStepController)?.activeStepDone()
//
//        if (viewPager.currentItem != 0) {
//            Snackbar.make(viewPager, "Step completed", Snackbar.LENGTH_INDEFINITE)
//                    .setAction("Show", {
//                        viewPager.currentItem = 0
//                        (pagerAdapter?.getRouter(0)?.getControllerWithTag("0") as? SessionStepController)?.activeStepDone()
//                    }).show()
//        }
//    }

    override fun onDestroyView(view: View) {
        if (!(activity?.isChangingConfigurations ?: false)) {
            view.viewPager.adapter = null
        }
        getTabLayout().setupWithViewPager(null)
        super.onDestroyView(view)
    }
}
