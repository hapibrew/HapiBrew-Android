/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package beer.hapibrew.app.service;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;

import java.io.IOException;

import beer.hapibrew.app.BuildConfig;
import timber.log.Timber;

public class Ringer {
    private static final boolean DBG = BuildConfig.DEBUG;

    private static final int PLAY_RING_ONCE = 1;
    private static final int STOP_RING = 3;

    private static final int VIBRATE_LENGTH = 1000; // ms
    private static final int PAUSE_LENGTH = 1000; // ms

    private MediaPlayer mLocalPlayer;

    Context mContext;
    AudioManager mAudioManager;

    Vibrator mVibrator;
    PowerManager mPowerManager;
    volatile boolean mContinueVibrating;
    VibratorThread mVibratorThread;
    private Worker mRingThread;
    private Handler mRingHandler;
    private long mFirstRingEventTime = -1;
    private long mFirstRingStartTime = -1;

    public Ringer(Context context) {
        mContext = context;
        mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    /**
     * @return true if we're playing a ringtone and/or vibrating
     * to indicate that there's an incoming call.
     * ("Ringing" here is used in the general sense.  If you literally
     * need to know if we're playing a ringtone or vibrating, use
     * isRingtonePlaying() or isVibrating() instead.)
     */
    public boolean isRinging() {
        synchronized (this) {
            return (isRingtonePlaying() || isVibrating());
        }
    }

    /**
     * @return true if the ringtone is playing
     */
    private boolean isRingtonePlaying() {
        synchronized (this) {
            return (mLocalPlayer != null && isPlaying()) ||
                    (mRingHandler != null && mRingHandler.hasMessages(PLAY_RING_ONCE));
        }
    }

    /**
     * @return true if we're vibrating in response to an incoming call
     */
    private boolean isVibrating() {
        synchronized (this) {
            return (mVibratorThread != null);
        }
    }

    /**
     * Starts the ringtone and/or vibrator
     */
    public void ring() {
        if (DBG) {
            log("ring()...");
        }

        synchronized (this) {
            if (isRinging())
                return;

            if (shouldVibrate() && mVibratorThread == null) {
                mContinueVibrating = true;
                mVibratorThread = new VibratorThread();
                if (DBG) {
                    log("- starting vibrator...");
                }
                mVibratorThread.start();
            }

            if (mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM) == 0) {
                if (DBG) {
                    log("skipping ring because volume is zero");
                }
                return;
            }

            makeLooper();
            if (mFirstRingEventTime < 0) {
                mFirstRingEventTime = SystemClock.elapsedRealtime();
                mRingHandler.sendEmptyMessage(PLAY_RING_ONCE);
            } else {
                // For repeat rings, figure out by how much to delay
                // the ring so that it happens the correct amount of
                // time after the previous ring
                if (mFirstRingStartTime > 0) {
                    // Delay subsequent rings by the delta between event
                    // and play time of the first ring
                    if (DBG) {
                        log("delaying ring by " + (mFirstRingStartTime - mFirstRingEventTime));
                    }
                    mRingHandler.sendEmptyMessageDelayed(PLAY_RING_ONCE,
                            mFirstRingStartTime - mFirstRingEventTime);
                } else {
                    // We've gotten two ring events so far, but the ring
                    // still hasn't started. Reset the event time to the
                    // time of this event to maintain correct spacing.
                    mFirstRingEventTime = SystemClock.elapsedRealtime();
                }
            }
        }
    }

    boolean shouldVibrate() {
        int ringerMode = mAudioManager.getRingerMode();
        if (getVibrateWhenRinging()) {
            return ringerMode != AudioManager.RINGER_MODE_SILENT;
        } else {
            return ringerMode == AudioManager.RINGER_MODE_VIBRATE;
        }
    }

    /**
     * Stops the ringtone and/or vibrator if any of these are actually
     * ringing/vibrating.
     */
    public void stopRing() {
        synchronized (this) {
            if (!isRinging())
                return;

            if (DBG) {
                log("stopRing()...");
            }

            if (mRingHandler != null) {
                mRingHandler.removeCallbacksAndMessages(null);
                Message msg = mRingHandler.obtainMessage(STOP_RING);
                mRingHandler.sendMessage(msg);
                mRingThread = null;
                mRingHandler = null;
                mFirstRingEventTime = -1;
                mFirstRingStartTime = -1;
            } else {
                if (DBG) {
                    log("- stopRing: null mRingHandler!");
                }
            }

            if (mVibratorThread != null) {
                if (DBG) {
                    log("- stopRing: cleaning up vibrator thread...");
                }
                mContinueVibrating = false;
                mVibratorThread = null;
            }
            // Also immediately cancel any vibration in progress.
            mVibrator.cancel();
        }
    }

    public void destroy() {
        stopRing();
    }

    private class VibratorThread extends Thread {
        public void run() {
            while (mContinueVibrating) {
                mVibrator.vibrate(VIBRATE_LENGTH);
                SystemClock.sleep(VIBRATE_LENGTH + PAUSE_LENGTH);
            }
        }
    }

    private class Worker implements Runnable {
        private final Object mLock = new Object();
        private Looper mLooper;

        Worker(String name) {
            Thread t = new Thread(null, this, name);
            t.start();
            synchronized (mLock) {
                while (mLooper == null) {
                    try {
                        mLock.wait();
                    } catch (InterruptedException ex) {
                    }
                }
            }
        }

        public Looper getLooper() {
            return mLooper;
        }

        public void run() {
            synchronized (mLock) {
                Looper.prepare();
                mLooper = Looper.myLooper();
                mLock.notifyAll();
            }
            Looper.loop();
        }

        public void quit() {
            mLooper.quit();
        }
    }

    boolean isPlaying() {
        try {
            return mLocalPlayer != null && mLocalPlayer.isPlaying();
        } catch (IllegalStateException e) {
            return false;
        }
    }

    void play() {
        // don't play if stream volume is 0
        if (mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM) == 0)
            return;
        preparePlayer();
        if (mLocalPlayer != null) {
            mLocalPlayer.start();
        }
    }

    void preparePlayer() {
        if (mLocalPlayer == null) {
            mLocalPlayer = new MediaPlayer();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AudioAttributes mAudioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build();
                mLocalPlayer.setAudioAttributes(mAudioAttributes);
            } else {
                mLocalPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            }
            mLocalPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    destroyLocalPlayer();
                    return false;
                }
            });
        } else {
            mLocalPlayer.reset();
        }

        mLocalPlayer.setLooping(true);
        try {
            mLocalPlayer.setDataSource(mContext, RingtoneHelper.INSTANCE.getSavedRingtoneUri(mContext));
            mLocalPlayer.prepare();
        } catch (SecurityException | IOException e) {
            Timber.d(e, "Failed to get ringtone");
            if (RingtoneHelper.INSTANCE.isSavedDefaultSystem(mContext)) {
                Timber.w("Can't play system default ringtone");
                RingtoneHelper.INSTANCE.switchToDefaultInternal(mContext);
//                mService.showRingtoneMissingNotification(RingtoneHelper.RINGTONE_MISSING_PERMISSION);
                trySetupAgain();
            } else if (RingtoneHelper.INSTANCE.isSavedRingtoneMissing(mContext)) {
                Timber.w("Missing saved inetrnal ringtone");
                RingtoneHelper.INSTANCE.switchToDefaultInternal(mContext);
//                mService.showRingtoneMissingNotification(RingtoneHelper.RINGTONE_MISSING_INTERNAL);
                trySetupAgain();
            } else {
                Timber.w("Something is wrong with ringtone");
                destroyLocalPlayer();
            }
        }
    }

    void trySetupAgain() {
        try {
            mLocalPlayer.setDataSource(mContext, RingtoneHelper.INSTANCE.getSavedRingtoneUri(mContext));
            mLocalPlayer.prepare();
        } catch (SecurityException | IOException e) {
            destroyLocalPlayer();
        }
    }

    void stop() {
        if (mLocalPlayer != null) {
            try {
                if (mLocalPlayer.isPlaying())
                    mLocalPlayer.stop();
            } catch (IllegalStateException e) {
            }
            destroyLocalPlayer();
        }
    }

    private void destroyLocalPlayer() {
        if (mLocalPlayer != null) {
            mLocalPlayer.reset();
            mLocalPlayer.release();
            mLocalPlayer = null;
        }
    }

    private void makeLooper() {
        if (mRingThread == null) {
            mRingThread = new Worker("ringer");
            mRingHandler = new Handler(mRingThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case PLAY_RING_ONCE:
                            if (DBG) {
                                log("mRingHandler: PLAY_RING_ONCE...");
                            }
                            if (!hasMessages(STOP_RING) && !isPlaying()) {
                                play();
                                synchronized (Ringer.this) {
                                    if (mFirstRingStartTime < 0) {
                                        mFirstRingStartTime = SystemClock.elapsedRealtime();
                                    }
                                }
                            }
                            break;
                        case STOP_RING:
                            if (DBG) {
                                log("mRingHandler: STOP_RING...");
                            }
                            stop();
                            getLooper().quit();
                            break;
                    }
                }
            };
        }
    }

    private static void log(String msg) {
        Timber.d(msg);
    }

    private boolean getVibrateWhenRinging() {
        if (mVibrator == null || !mVibrator.hasVibrator()) {
            return false;
        }
        return Settings.System.getInt(mContext.getContentResolver(), "vibrate_when_ringing", 0) != 0;
    }
}