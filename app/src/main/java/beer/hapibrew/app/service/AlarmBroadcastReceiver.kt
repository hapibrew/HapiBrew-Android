package beer.hapibrew.app.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.WakefulBroadcastReceiver

class AlarmBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            NotificationService.ACTION_ALARM -> {
                val serviceIntent = Intent(NotificationService.ACTION_ALARM, null, context, NotificationService::class.java)
                serviceIntent.putExtras(intent.extras)
                WakefulBroadcastReceiver.startWakefulService(context, serviceIntent)
            }
        }
    }
}