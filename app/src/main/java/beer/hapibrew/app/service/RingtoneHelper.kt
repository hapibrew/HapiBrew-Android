package beer.hapibrew.app.service

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.Settings
import android.text.TextUtils
import beer.hapibrew.app.R
import beer.hapibrew.app.prefs
import timber.log.Timber

object RingtoneHelper {
    internal val ringtoneIds = intArrayOf(0, R.raw.modem, R.raw.a_je_to)
    /**
     * Get resource id for ringtone names
     * @return string array resource id
     */
    val names = R.array.alarms

    /**
     * Get ringtone names
     * @param context context for resources
     * *
     * @return array of ringtone names
     */
    fun getNames(context: Context): Array<String> {
        return context.resources.getStringArray(names)
    }

    /**
     * Return resource id from [.ringtoneIds].
     * @param index index
     * *
     * @return resource id or 0 for system default.
     */
    private fun getResourceId(index: Int): Int {
        return ringtoneIds[index]
    }

    /**
     * Get saved ringtone. It can be empty string for system default or
     * in format returned by [android.content.res.Resources.getResourceName].
     * @param context context necessary for preferences
     * *
     * @return saved ringtone
     */
    private fun getSavedRingtone(context: Context): String {
        return prefs.alarm
    }

    /**
     * Get ringtone index from [.ringtoneIds] based on saved name.
     * @param context context necessary for resources
     * *
     * @param savedRingtone saved ringtone from [.getSavedRingtone]
     * *
     * @return index
     */
    private fun getRingtoneIndex(context: Context, savedRingtone: String): Int {
        if (TextUtils.isEmpty(savedRingtone))
            return 0

        val resId = context.resources.getIdentifier(savedRingtone, null, null)
        if (resId == 0)
            return 0

        return ringtoneIds.indices.firstOrNull { ringtoneIds[it] == resId }
                ?: 0
    }

    /**
     * Get ringtone index from [.ringtoneIds].
     * @param context context necessary for preferences
     * *
     * @return ringtone index
     */
    fun getSavedRingtoneIndex(context: Context): Int {
        val savedRingtone = getSavedRingtone(context)
        Timber.d("Saved name %s", savedRingtone)
        return getRingtoneIndex(context, savedRingtone)
    }

    /**
     * Get saved ringtone. Name is one of [.ringtoneNames]
     * @param context context necessary for preferences
     * *
     * @return ringtone name
     */
    fun getSavedRingtoneName(context: Context): String {
        return getRingtoneName(context, getSavedRingtoneIndex(context))
    }

    /**
     * Get ringtone name. Name is one of [.ringtoneNames].
     * @param context context necessary for resources
     * *
     * @param index index from [.ringtoneIds]
     * *
     * @return ringtone name
     */
    fun getRingtoneName(context: Context, index: Int): String {
        val ringtoneNames = getNames(context)
        return ringtoneNames[index]
    }

    /**
     * Save selected ringtone from [.ringtoneIds] to preferences.
     * @param context context for preferences
     * *
     * @param index index from [.ringtoneIds]
     */
    fun saveRingtone(context: Context, index: Int) {
        if (index == 0) {
            prefs.alarm = ""
        } else {
            prefs.alarm = context.resources.getResourceName(getResourceId(index))
        }
    }

    /**
     * Get uri for media player from [.ringtoneIds]
     * @param context context necessary for resources
     * *
     * @param index index from [.ringtoneIds]
     * *
     * @return ringtone Uri for playback
     */
    fun getRingtoneUri(context: Context, index: Int): Uri {
        if (index == 0) {
            return Settings.System.DEFAULT_ALARM_ALERT_URI
        }
        val res = context.resources
        val resId = getResourceId(index)
        return Uri.parse(
                ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        res.getResourcePackageName(resId) + "/" +
                        res.getResourceTypeName(resId) + "/" +
                        res.getResourceEntryName(resId)
        )
    }

    /**
     * Get uri for media player from [.ringtoneIds]
     * @param context context necessary for resources
     * *
     * @return ringtone Uri for playback
     */
    fun getSavedRingtoneUri(context: Context): Uri {
        return getRingtoneUri(context, getSavedRingtoneIndex(context))
    }

    /**
     * Check if selected ringtone is default system.
     * @param context context for preferences
     * *
     * @return true for system ringtone
     */
    fun isSavedDefaultSystem(context: Context): Boolean {
        return TextUtils.isEmpty(getSavedRingtone(context))
    }

    /**
     * Check if selected ringtone is missing. Could be default system.
     * @param context context for preferences
     * *
     * @return true if misssing or default system.
     */
    fun isSavedRingtoneMissing(context: Context): Boolean {
        return getRingtoneIndex(context, getSavedRingtone(context)) == 0
    }

    /**
     * Change to default local ringtone. Use in case of system ringtone unavailable.
     * @param context context
     * *
     * @return ringtone index
     */
    fun switchToDefaultInternal(context: Context): Int {
        saveRingtone(context, 1)
        return 1
    }

    /**
     * Change to default system ringtone.
     * @param context context
     * *
     * @return ringtone index
     */
    fun switchToDefaultSystem(context: Context): Int {
        saveRingtone(context, 0)
        return 0
    }
}
