package beer.hapibrew.app.service

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.annotation.DrawableRes
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import beer.hapibrew.app.R
import beer.hapibrew.app.activity.SessionActivity
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.database.SessionDatabase
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.ringer
import com.evernote.android.job.Job
import com.evernote.android.job.JobProxy.Common.completeWakefulIntent
import org.jetbrains.anko.ctx
import org.jetbrains.anko.notificationManager
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


class NotificationService : Service() {

    companion object {
        val ACTION_SHOW_NOTIFICATION = "ACTION_SHOW_NOTIFICATION"
        val ACTION_REMOVE_NOTIFICATION = "ACTION_REMOVE_NOTIFICATION"
        val ACTION_CANCEL_PROGRESS = "ACTION_CANCEL_PROGRESS"
        val ACTION_ALARM = "ACTION_ALARM"
    }

    private val progressScheduler = Executors.newSingleThreadScheduledExecutor()
    private var progressFuture: ScheduledFuture<*>? = null
    private var isFutureRunning: Boolean = false

    private val notificationIds = HashMap<Long, Int>()
    private val progressNotifications
            = HashMap<Long, NotificationCompat.Builder>()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_SHOW_NOTIFICATION -> showNotification(intent.extras)
            ACTION_REMOVE_NOTIFICATION -> removeNotification(intent.extras)
            ACTION_CANCEL_PROGRESS -> cancelProgress(intent.extras)
            ACTION_ALARM -> showAlarm(intent)
        }
        return START_NOT_STICKY
    }

    private fun showNotification(b: Bundle?) {
        val sessionData = getSessionData(b) ?: return
        Timber.d("Show notification %d", sessionData.id)

        notificationManager.notify(getNotificationId(sessionData), generateNotification(sessionData))
    }

    private fun removeNotification(b: Bundle?) {
        val sessionData = getSessionData(b) ?: return
        Timber.d("Remove notification %d", sessionData.id)
        notificationManager.cancel(getNotificationId(sessionData))

        progressNotifications.remove(sessionData.id)
        monitorProgress()
    }

    private fun cancelProgress(b: Bundle?) {
        val sessionData = getSessionData(b) ?: return
        Timber.d("Cancel progress %d", sessionData.id)

        progressNotifications.remove(sessionData.id)
        monitorProgress()

        stopRinging()
    }

    private fun showAlarm(intent: Intent) {
        try {
            val sessionData = getSessionData(intent.extras) ?: return
            Timber.d("Show alarm %d", sessionData.id)

            notificationManager.notify(getNotificationId(sessionData), generateNotification(sessionData, true))
        } finally {
            completeWakefulIntent(intent)
        }
    }

    private fun stopRinging() {
        ringer.stopRing()
    }

    private fun generateNotification(sessionData: SessionData, alarm:Boolean = false): Notification {
        val recipeStep = sessionData.buildRecipeStep(ctx)

        val sessionIntent = Intent(ctx, SessionActivity::class.java)
        val deleteIntent = Intent(ACTION_CANCEL_PROGRESS, null, ctx, NotificationService::class.java)

        sessionIntent.putExtra(BUNDLE_SESSION_ID, sessionData.id)
        deleteIntent.putExtra(BUNDLE_SESSION_ID, sessionData.id)

        val builder = NotificationCompat.Builder(ctx)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(recipeStep.title)
                .setContentText(recipeStep.subtitle)
                .setContentIntent(PendingIntent.getActivity(ctx, getNotificationId(sessionData), sessionIntent, PendingIntent.FLAG_ONE_SHOT))
                .setDeleteIntent(PendingIntent.getService(ctx, getNotificationId(sessionData), deleteIntent, PendingIntent.FLAG_ONE_SHOT))
                .setAutoCancel(true)

        if (alarm) {
            builder.setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
//            builder.setDefaults(Notification.DEFAULT_ALL)
            builder.priority = Notification.PRIORITY_HIGH
            ringer.ring()
        } else {
            builder.priority = Notification.PRIORITY_LOW
            ringer.stopRing()
        }

        builder.setGroup(sessionData.name)

        when (sessionData.phase) {
            SessionData.Phase.PRE_MASH,
            SessionData.Phase.SPARGE,
            SessionData.Phase.PRE_FERMENTATION,
            SessionData.Phase.COMPLETED
            -> {
                progressNotifications.remove(sessionData.id)
                builder.setProgress(0, 0, false)
//                builder.addAction()
            }
            SessionData.Phase.MASH,
            SessionData.Phase.BOIL,
            SessionData.Phase.FERMENTATION
            -> {
                val progress = sessionData.getStepProgress()
                if (progress >= 1.0) {
                    builder.setLargeIcon(generateLargeIcon(R.drawable.ic_notification_done))
                } else {
                    if (sessionData.stepStartEpoch == 0L || sessionData.stepPauseEpoch > 0L) {
                        builder.setLargeIcon(generateLargeIcon(R.drawable.ic_notification_pause))
                    } else {
                        progressNotifications.put(sessionData.id, builder)
                        builder.setLargeIcon(generateLargeIcon(R.drawable.ic_notification_play))
                    }
                }
                builder.setProgress(100, (progress * 100).toInt(), false)
//                builder.addAction()
            }
        }
        monitorProgress()

        return builder.build()
    }

    private fun monitorProgress() {
        if (progressNotifications.isEmpty()) {
            Timber.d("Nothing to monitor. Canceling future")
            progressFuture?.cancel(true)
            isFutureRunning = false
        } else {
            if (!isFutureRunning) {
                Timber.d("Starting progress monitoring")
                progressFuture = progressScheduler.scheduleAtFixedRate(updateProgress, 10, 10, TimeUnit.SECONDS)
                isFutureRunning = true
            } else {
                Timber.d("Monitoring already running")
            }
        }
    }

    val updateProgress = Runnable {
        progressNotifications.forEach { (id, builder) ->
            SessionDatabase.getSession(ctx, id)?.let { sessionData ->
                builder.setProgress(100, (sessionData.getStepProgress() * 100).toInt(), false)
                notificationManager.notify(getNotificationId(sessionData), builder.build())
            }
        }
    }

    private fun getSessionData(b: Bundle?): SessionData? {
        val sessionId = b?.getLong(BUNDLE_SESSION_ID, -1) ?: return null
        return SessionDatabase.getSession(ctx, sessionId)
    }

    private fun getNotificationId(sessionData: SessionData): Int {
        return notificationIds[sessionData.id] ?: run {
            val id: Int = sessionData.id.toInt()
            notificationIds.put(sessionData.id, id)
            id
        }
    }

    fun generateLargeIcon(@DrawableRes resId: Int): Bitmap {
        val sizeI = resources.getDimensionPixelSize(android.R.dimen.notification_large_icon_height)
        val sizeF = sizeI.toFloat()
        val center = sizeF / 2f

        val icon = BitmapFactory.decodeResource(resources, resId, null)

        val outBitmap = Bitmap.createBitmap(sizeI, sizeI, Bitmap.Config.ARGB_8888)

        val paint = Paint(ANTI_ALIAS_FLAG)
        paint.color = ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark)

        val canvas = Canvas(outBitmap)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawCircle(center, center, center, paint)
        } else {
            canvas.drawRect(0f, 0f, sizeF, sizeF, paint)
        }

        canvas.drawBitmap(icon, center - icon.width / 2, center - icon.height / 2, Paint(Color.WHITE))

        return outBitmap
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        ringer.stop()

        super.onDestroy()
    }
}