package beer.hapibrew.app.dialog

import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import beer.hapibrew.app.R
import kotlinx.android.synthetic.main.dialog_simple.view.*

open class SimpleDialog : BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        val metrics = DisplayMetrics()
        dialog.window!!.windowManager.defaultDisplay.getMetrics(metrics)

        dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        return dialog
    }

    protected fun setInitialPeekHeight(dialog: Dialog, view: View) {
        val parent = view.parent as View

        val params = parent.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior

        val metrics = DisplayMetrics()
        dialog.window!!.windowManager.defaultDisplay.getMetrics(metrics)

        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                behavior.setPeekHeight((metrics.heightPixels * 0.6).toInt())
            } else {
                behavior.setPeekHeight(metrics.heightPixels)
            }
        }
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
//        super.setupDialog(dialog, style)

        val view = View.inflate(context, R.layout.dialog_simple, null)

        dialog.setContentView(view)

        val b = arguments ?: return

        with (view.dialog_title) {
            text = b.getString(EXTRA_TITLE, "")
        }


        with (view.dialog_description) {
            text = b.getString(EXTRA_DESCRIPTION, "")
            if (TextUtils.isEmpty(text)) {
                visibility = View.GONE
            }
        }

        with (view.dialog_btn_left) {
            text = b.getString(EXTRA_LEFT_BUTTON_TEXT, "")
            setOnClickListener { view -> onLeftButtonClick(view) }
            if (TextUtils.isEmpty(text)) {
                visibility = View.GONE
            }
        }

        with (view.dialog_description) {
            text = b.getString(EXTRA_RIGHT_BUTTON_TEXT, "")
            setOnClickListener { view -> onRightButtonClick(view) }
            if (TextUtils.isEmpty(text)) {
                visibility = View.GONE
            }
        }

        with (view.dialog_warning) {
            if (b.getBoolean(EXTRA_WARNING, false)) {
                visibility = View.VISIBLE
            }
        }
    }

    protected fun onLeftButtonClick(view: View) {
        dismiss()
    }

    protected fun onRightButtonClick(view: View) {
        dismiss()
    }

    class BundleBuilder {
        protected val result: Bundle = Bundle()

        fun build(): Bundle {
            return result
        }

        fun setWarning(warning: Boolean): BundleBuilder {
            result.putBoolean(EXTRA_WARNING, warning)
            return this
        }

        fun setTitle(title: String): BundleBuilder {
            result.putString(EXTRA_TITLE, title)
            return this
        }

        fun setTitle(context: Context, res: Int): BundleBuilder {
            result.putString(EXTRA_TITLE, context.getString(res))
            return this
        }

        fun setDescription(description: String): BundleBuilder {
            result.putString(EXTRA_DESCRIPTION, description)
            return this
        }

        fun setDescription(context: Context, res: Int): BundleBuilder {
            result.putString(EXTRA_DESCRIPTION, context.getString(res))
            return this
        }

        fun setLeftButtonText(text: String): BundleBuilder {
            result.putString(EXTRA_LEFT_BUTTON_TEXT, text)
            return this
        }

        fun setLeftButtonText(context: Context, res: Int): BundleBuilder {
            result.putString(EXTRA_LEFT_BUTTON_TEXT, context.getString(res))
            return this
        }

        fun setRightButtonText(text: String): BundleBuilder {
            result.putString(EXTRA_RIGHT_BUTTON_TEXT, text)
            return this
        }

        fun setRightButtonText(context: Context, res: Int): BundleBuilder {
            result.putString(EXTRA_RIGHT_BUTTON_TEXT, context.getString(res))
            return this
        }
    }

    companion object {
        internal val EXTRA_TITLE = "title"
        internal val EXTRA_DESCRIPTION = "description"
        internal val EXTRA_LEFT_BUTTON_TEXT = "btnLeftText"
        internal val EXTRA_RIGHT_BUTTON_TEXT = "btnRightText"
        internal val EXTRA_WARNING = "warning"
    }
}
