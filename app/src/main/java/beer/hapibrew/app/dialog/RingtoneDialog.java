package beer.hapibrew.app.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;

import beer.hapibrew.app.R;
import beer.hapibrew.app.service.RingtoneHelper;
import timber.log.Timber;

public class RingtoneDialog {
    Context mContext;
    OnRingtoneChangedListener mListener;

    boolean mClosing = false;

    private MediaPlayer mLocalPlayer;

    private int mCurrentIndex;

    MaterialDialog mDialog;

    public RingtoneDialog(Context context, OnRingtoneChangedListener listener) {
        mContext = context;
        mListener = listener;
        mCurrentIndex = RingtoneHelper.INSTANCE.getSavedRingtoneIndex(mContext);
    }

    public void show() {
        mDialog = new MaterialDialog.Builder(mContext)
                .title(R.string.settings_alarm_title)
                .items(RingtoneHelper.INSTANCE.getNames())
                .itemsCallbackSingleChoice(mCurrentIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        return playRingtone(which);
                    }
                })
                .alwaysCallSingleChoiceCallback()
                .positiveText(mContext.getString(R.string.dialog_button_choose).toUpperCase())
                .negativeText(mContext.getString(R.string.dialog_button_cancel).toUpperCase())
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mClosing = true;
                        int index = dialog.getSelectedIndex();
                        saveRingtone(index);
                        if (mListener != null) {
                            mListener.onRingtoneChanged(RingtoneHelper.INSTANCE.getRingtoneName(mContext, index));
                        }
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        stop();
                    }
                })
                .show();
    }

    boolean playRingtone(int index) {
        if (mClosing)
            return false;

        Timber.d("Play %d", index);
        return play(index);
    }

    boolean play(int index) {
        preparePlayer();
        boolean ok = setDataSource(index);
        if (mLocalPlayer != null) {
            mLocalPlayer.start();
        }
        return ok;
    }

    void preparePlayer() {
        if (mLocalPlayer == null) {
            mLocalPlayer = new MediaPlayer();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AudioAttributes mAudioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build();
                mLocalPlayer.setAudioAttributes(mAudioAttributes);
            } else {
                mLocalPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            }
            mLocalPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    destroyLocalPlayer();
                    return false;
                }
            });
            mLocalPlayer.setLooping(false);
        } else {
            mLocalPlayer.reset();
        }
    }
    boolean setDataSource(int index) {
        Uri uri = RingtoneHelper.INSTANCE.getRingtoneUri(mContext, index);
        try {
            mLocalPlayer.setDataSource(mContext, uri);
            mLocalPlayer.prepare();
            mCurrentIndex = index;
            return true;
        } catch (SecurityException | IOException e) {
            destroyLocalPlayer();
            showMissingPermission();
            return false;
        }
    }

    void stop() {
        if (mLocalPlayer != null) {
            if (mLocalPlayer.isPlaying())
                mLocalPlayer.stop();
        }
        destroyLocalPlayer();
    }
    private void destroyLocalPlayer() {
        if (mLocalPlayer != null) {
            mLocalPlayer.reset();
            mLocalPlayer.release();
            mLocalPlayer = null;
        }
    }

    private void showMissingPermission() {
        //TODO: show toast for missing permission

//        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
//            @Override
//            public void onPermissionResult(Permiso.ResultSet resultSet) {
//                if (resultSet.areAllPermissionsGranted()) {
//                    mCurrentIndex = RingtoneHelper.switchToDefaultSystem(mContext);
//                    mDialog.setSelectedIndex(mCurrentIndex);
//                } else {
//                    mCurrentIndex = RingtoneHelper.switchToDefaultInternal(mContext);
//                    mDialog.setSelectedIndex(mCurrentIndex);
//                }
//                if (mListener != null) {
//                    mListener.onRingtoneChanged(RingtoneHelper.getRingtoneName(mContext, mCurrentIndex));
//                }
//            }
//
//            @Override
//            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
//                //TODO:
//                //Permiso.getInstance().showRationaleInDialog("Title", "Message", null, callback);
//
//                callback.onRationaleProvided();
//            }
//        }, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void saveRingtone(int index) {
        RingtoneHelper.INSTANCE.saveRingtone(mContext, index);
    }

    public interface OnRingtoneChangedListener {
        void onRingtoneChanged(String name);
    }
}
