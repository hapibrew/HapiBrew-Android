package beer.hapibrew.app.dialog

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import beer.hapibrew.app.R

class StepDurationDialog : SimpleDialog() {
    internal var mHandler = Handler()

    private var tvDuration: TextView? = null

    private var closeListener: OnStepDurationDialogListener? = null

    private var mDuration: Int = 0

    internal var mMin = 0
    internal var mMax: Int = 0

    override fun onDetach() {
        mHandler.removeCallbacks(null)

        super.onDetach()
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        val view = View.inflate(context, R.layout.dialog_step_duration, null)

        dialog.setContentView(view)

        tvDuration = view.findViewById(R.id.duration)

        view.findViewById<View>(R.id.durationIncrease).setOnTouchListener(mTouchListener)
        view.findViewById<View>(R.id.durationDecrease).setOnTouchListener(mTouchListener)

        view.findViewById<View>(R.id.buttonCancel).setOnClickListener(mButtonCancelClickListener)
        view.findViewById<View>(R.id.buttonDone).setOnClickListener(mButtonDoneClickListener)


        val args = arguments ?: return

        mDuration = args.getInt("duration", 0)
        if (args.getBoolean("isDays", false)) {
            mMin = 1
            mMax = 365
            (view.findViewById<TextView>(R.id.durationUnit)).setText(R.string.unit_short_days)
        } else {
            mMin = 1
            mMax = 300
            (view.findViewById<TextView>(R.id.durationUnit)).setText(R.string.unit_short_minutes)
        }

        if (mDuration < mMin)
            mDuration = mMin
        else if (mDuration > mMax)
            mDuration = mMax

        setInitialPeekHeight(dialog, view)

        refreshGui()
    }

    private fun refreshGui() {
        tvDuration!!.text = if (mDuration < 10) "0" + mDuration else mDuration.toString()
    }

    internal var mTouchListener: View.OnTouchListener = View.OnTouchListener { view, motionEvent ->
        val tag = view.tag as String
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                mHandler.post(ChangeTime(tag))
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                mHandler.removeCallbacksAndMessages(tag)
            }
        }
        false
    }

    internal var mButtonDoneClickListener: View.OnClickListener = View.OnClickListener {
        closeListener?.updateDuration(mDuration)

        dismiss()
    }

    internal var mButtonCancelClickListener: View.OnClickListener = View.OnClickListener { dismiss() }

    private inner class ChangeTime internal constructor(internal var mTag: String) : Runnable {
        private val CHANGE_DELAY = 200

        internal var mStep: Int = 0

        init {
            when (mTag) {
                "inc" -> {
                    mStep = 1
                }
                "dec" -> {
                    mStep = -1
                }
            }
        }

        override fun run() {
            mDuration += mStep
            if (mDuration < mMin)
                mDuration = mMax
            else if (mDuration > mMax)
                mDuration = mMin

            refreshGui()
            mHandler.removeCallbacksAndMessages(mTag)
            mHandler.postAtTime(this, mTag, SystemClock.uptimeMillis() + CHANGE_DELAY)
        }
    }

    fun setCallback(listener:OnStepDurationDialogListener) {
        this.closeListener = listener
    }

    interface OnStepDurationDialogListener {
        fun updateDuration(value: Int)
    }

    companion object {

        fun newInstance(duration: Int, isDays: Boolean): StepDurationDialog {
            val d = StepDurationDialog()
            val bundle = Bundle()
            bundle.putInt("duration", duration)
            bundle.putBoolean("isDays", isDays)
            d.arguments = bundle
            return d
        }
    }
}
