package beer.hapibrew.app.verticalstepperform.interfaces;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface VerticalStepperFormStep {

    /**
     * The content of the layout of the corresponding step must be generated here. The system will
     * automatically call this method for every step
     * @param stepNumber the number of the step
     * @return The view that will be automatically added as the content of the step
     */
    View createStepContentView(LayoutInflater inflater, ViewGroup container, int stepNumber);

    /**
     * This method will be called every time a certain step is open
     * @param stepNumber the number of the step
     */
    void bindStepContentView(View view, int stepNumber);

}
