package beer.hapibrew.app.session

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.FermentableItem
import beer.hapibrew.app.adapter.items.recipe.FermentableItemHeader
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.app.view.CountDownTextView
import beer.hapibrew.app.view.SessionButton
import beer.hapibrew.beerxml2proto.proto.Fermentable
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.DividerItemDecoration
import org.jetbrains.anko.sdk25.coroutines.onClick


class FormMashStep(val controller: SessionStepController) : VerticalStepperFormStep {
    private lateinit var ctx: Context
    private var stepNumber:Int = 0
    private lateinit var recyclerView: RecyclerView
    private lateinit var countDown: CountDownTextView
    private lateinit var startBtn: SessionButton

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_mash, container, false)
        this.ctx = inflater.context
        this.stepNumber = stepNumber
        this.recyclerView = v.findViewById(R.id.recyclerView)
        this.countDown = v.findViewById(R.id.mashCountDown)
        this.startBtn = v.findViewById(R.id.startMash)

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let {
            val isStepCompleted = controller.isStepCompleted(stepNumber)

            if (controller.recipeSteps[stepNumber].step == 0) {
                recyclerView.visibility = View.VISIBLE
                showFermentables(Wire.get(it.recipe.fermentables, emptyList()))
            } else {
                recyclerView.visibility = View.GONE
            }

            countDown.run {
                setTimer(isStepCompleted, it, this@FormMashStep::onStepFinish, controller::updateProgress)
                (controller.activity as? AppCompatActivity)?.let { activity ->
                    setStepDurationCallback(activity, it, controller)
                }
            }

            startBtn.run {
                setState(it, isStepCompleted)
                onClick {
                    onButtonClick()
                }
            }
        }
    }

    private fun onButtonClick() {
        controller.sessionData.let {
            if (it.stepStartEpoch == 0L) {
                val duration = controller.startStep()
                countDown.startTimer(duration, it, this@FormMashStep::onStepFinish, controller::updateProgress)
            } else if (it.getStepProgress() >= 1.0) {
                controller.stepDone(stepNumber)
            } else {
                if (it.stepPauseEpoch != 0L) {
                    val duration = controller.resumeStep()
                    countDown.startTimer(duration, it, this@FormMashStep::onStepFinish, controller::updateProgress)
                } else {
                    controller.pauseStep()
                    countDown.stop()
                }
            }
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }

    private fun onStepFinish() {
        controller.sessionData.let {
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }

    private fun showFermentables(fermentables: List<Fermentable>) {
        if (fermentables.isNotEmpty()) {
            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.addItemDecoration(DividerItemDecoration(ctx, 0, 16))
            val header = FermentableItemHeader()
            val adapter = FlexibleAdapter<FermentableItem>(fermentables.map {
                FermentableItem(it, header)
            })
            adapter.setDisplayHeadersAtStartUp(true)
            recyclerView.adapter = adapter
        } else {
            recyclerView.visibility = View.GONE
        }
    }
}