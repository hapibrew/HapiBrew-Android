package beer.hapibrew.app.session

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.app.view.CountDownTextView
import beer.hapibrew.app.view.SessionButton
import org.jetbrains.anko.sdk25.coroutines.onClick

class FormFermentStep(val controller: SessionStepController) : VerticalStepperFormStep {
    private lateinit var ctx: Context
    private var stepNumber:Int = 0
    private lateinit var countDown: CountDownTextView
    private lateinit var startBtn: SessionButton

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_fermentation, container, false)
        this.ctx = inflater.context
        this.stepNumber = stepNumber
        this.countDown = v.findViewById(R.id.fermentCountDown)
        this.startBtn = v.findViewById(R.id.startFermentation)

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let {
            val isStepCompleted = controller.isStepCompleted(stepNumber)

            countDown.run {
                setTimer(isStepCompleted, it, this@FormFermentStep::onStepFinish, controller::updateProgress)
                (controller.activity as? AppCompatActivity)?.let { activity ->
                    setStepDurationCallback(activity, it, controller)
                }
            }

            startBtn.run {
                setState(it, isStepCompleted)
                onClick {
                    onButtonClick()
                }
            }
        }
    }

    private fun onButtonClick() {
        controller.sessionData.let {
            if (it.stepStartEpoch == 0L) {
                val duration = controller.startStep()
                countDown.startTimer(duration, it, this@FormFermentStep::onStepFinish, controller::updateProgress)
            } else if (it.getStepProgress() >= 1.0) {
                controller.stepDone(stepNumber)
            } else {
                if (it.stepPauseEpoch != 0L) {
                    val duration = controller.resumeStep()
                    countDown.startTimer(duration, it, this@FormFermentStep::onStepFinish, controller::updateProgress)
                } else {
                    controller.pauseStep()
                    countDown.stop()
                }
            }
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }

    private fun onStepFinish() {
        controller.sessionData.let {
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }
}