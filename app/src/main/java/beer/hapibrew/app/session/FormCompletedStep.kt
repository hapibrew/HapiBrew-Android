package beer.hapibrew.app.session

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.brixToSg
import beer.hapibrew.app.calculators.sgToBrix
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.squareup.wire.Wire
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.textChangedListener

class FormCompletedStep(val controller: SessionStepController) : VerticalStepperFormStep {
    lateinit var ctx: Context
    lateinit var updateValues: Button
    lateinit var switchGravityUnit: Button
    lateinit var originalGravity: TextInputLayout
    lateinit var originalGravityValue: TextInputEditText
    lateinit var finalGravity: TextInputLayout
    lateinit var finalGravityValue: TextInputEditText
    var useSg:Boolean = true

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_completed, container, false)
        ctx = inflater.context
        updateValues = v.findViewById(R.id.updateValues)
        switchGravityUnit = v.findViewById(R.id.switchGravityUnit)
        originalGravity = v.findViewById(R.id.originalGravity)
        originalGravityValue = v.findViewById(R.id.originalGravityValue)
        finalGravity = v.findViewById(R.id.finalGravity)
        finalGravityValue = v.findViewById(R.id.finalGravityValue)
        originalGravityValue.textChangedListener {
            afterTextChanged {
                enableButton()
            }
        }
        finalGravityValue.textChangedListener {
            afterTextChanged {
                enableButton()
            }
        }
        switchGravityUnit.setOnClickListener {
            useSg = !useSg
            setUnitButtonText(switchGravityUnit, originalGravity, finalGravity)
            convertGravityUnit(originalGravityValue, originalGravity, useSg)
            convertGravityUnit(originalGravityValue, finalGravity, useSg)
        }

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let { sessionData ->
            updateValues.run {
                onClick {
                    controller.updateGravityValues(
                            if (useSg) originalGravityValue.toDouble() else originalGravityValue.toDouble().bxToSg(),
                            if (useSg) finalGravityValue.toDouble() else finalGravityValue.toDouble().bxToSg()
                    )
                }
            }

            setUnitButtonText(switchGravityUnit, originalGravity, finalGravity)

            val og:Double
            if (sessionData.og == 0.0) {
                og = Wire.get(sessionData.recipe.est_og, Recipe.DEFAULT_EST_OG)!!
            } else {
                og = sessionData.og
            }
            val fg:Double
            if (sessionData.fg == 0.0) {
                fg = Wire.get(sessionData.recipe.est_fg, Recipe.DEFAULT_EST_FG)!!
            } else {
                fg = sessionData.fg
            }
            originalGravityValue.setText(og.toSgFormat(), TextView.BufferType.EDITABLE)
            finalGravityValue.setText(fg.toSgFormat(), TextView.BufferType.EDITABLE)
        }

        enableButton()
    }

    private fun enableButton() {
        updateValues.isEnabled = isValidGravity(ctx, originalGravityValue.toDouble(), useSg, originalGravity) &&
                isValidGravity(ctx, finalGravityValue.toDouble(), useSg, finalGravity)
    }

    private fun setUnitButtonText(button: TextView, hintOg:TextInputLayout, hintFg:TextInputLayout) {
        val currentUnit = if (useSg) R.string.unit_sg else R.string.unit_brix
        val inverseUnit = if (useSg) R.string.unit_brix else R.string.unit_sg
        button.text = button.context.getString(R.string.use_unit, button.context.getString(inverseUnit))
        hintOg.hint = hintOg.context.getString(R.string.original_gravity, hintOg.context.getString(currentUnit))
        hintFg.hint = hintFg.context.getString(R.string.final_gravity, hintFg.context.getString(currentUnit))
    }

    private fun convertGravityUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toSg:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidGravity(valueView.context, currentValue, !toSg, wrapperView)) {
            if (toSg) {
                valueView.setText(brixToSg(currentValue).toSgFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(sgToBrix(currentValue).toBrixFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }
}