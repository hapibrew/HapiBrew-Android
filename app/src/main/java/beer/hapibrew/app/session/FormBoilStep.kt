package beer.hapibrew.app.session

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.HopItem
import beer.hapibrew.app.adapter.items.recipe.HopItemHeader
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.app.view.CountDownTextView
import beer.hapibrew.app.view.SessionButton
import beer.hapibrew.beerxml2proto.proto.Hop
import beer.hapibrew.beerxml2proto.proto.HopUse
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.DividerItemDecoration
import org.jetbrains.anko.sdk25.coroutines.onClick

class FormBoilStep(val controller: SessionStepController) : VerticalStepperFormStep {
    private lateinit var ctx: Context
    private var stepNumber:Int = 0
    private lateinit var recyclerView: RecyclerView
    private lateinit var countDown: CountDownTextView
    private lateinit var startBtn: SessionButton

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_boil, container, false)
        this.ctx = inflater.context
        this.stepNumber = stepNumber
        this.recyclerView = v.findViewById(R.id.recyclerView)
        this.countDown = v.findViewById(R.id.boilCountDown)
        this.startBtn = v.findViewById(R.id.startBoil)

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let {
            val isStepCompleted = controller.isStepCompleted(stepNumber)

            val hops = Wire.get(it.recipe.hops, emptyList())
            showHops(
                    hops.filter { it.use == HopUse.BOIL }.sortedByDescending(Hop::time),
                    hops.filter { it.use == HopUse.AROMA }.sortedByDescending(Hop::time)
            )

            countDown.run {
                setTimer(isStepCompleted, it, this@FormBoilStep::onStepFinish, controller::updateProgress)
                (controller.activity as? AppCompatActivity)?.let { activity ->
                    setStepDurationCallback(activity, it, controller)
                }
            }

            startBtn.run {
                setState(it, isStepCompleted)
                onClick {
                    onButtonClick()
                }
            }
        }
    }

    private fun onButtonClick() {
        controller.sessionData.let {
            if (it.stepStartEpoch == 0L) {
                val duration = controller.startStep()
                countDown.startTimer(duration, it, this@FormBoilStep::onStepFinish, controller::updateProgress)
            } else if (it.getStepProgress() >= 1.0) {
                controller.stepDone(stepNumber)
            } else {
                if (it.stepPauseEpoch != 0L) {
                    val duration = controller.resumeStep()
                    countDown.startTimer(duration, it, this@FormBoilStep::onStepFinish, controller::updateProgress)
                } else {
                    controller.pauseStep()
                    countDown.stop()
                }
            }
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }

    private fun onStepFinish() {
        controller.sessionData.let {
            startBtn.setState(it, controller.isStepCompleted(stepNumber))
        }
    }

    private fun showHops(boilHops: List<Hop>, aromaHops: List<Hop>) {
        if (boilHops.isNotEmpty() || aromaHops.isNotEmpty()) {
            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.addItemDecoration(DividerItemDecoration(ctx, 0, 16))
            val boilHeader = HopItemHeader(HopUse.BOIL, true)
            val boilItems = boilHops.map {
                HopItem(it, boilHeader)
            }
            val aromaHeader = HopItemHeader(HopUse.AROMA, true)
            val aromaItems = aromaHops.map {
                HopItem(it, aromaHeader)
            }
            val adapter = FlexibleAdapter<HopItem>(boilItems + aromaItems)
            adapter.setDisplayHeadersAtStartUp(true)
            recyclerView.adapter = adapter
        } else {
            recyclerView.visibility = View.GONE
        }
    }
}