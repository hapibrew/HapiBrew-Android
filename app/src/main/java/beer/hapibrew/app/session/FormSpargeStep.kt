package beer.hapibrew.app.session

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.adapter.items.recipe.HopItem
import beer.hapibrew.app.adapter.items.recipe.HopItemHeader
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.extensions.displayTemperature
import beer.hapibrew.app.extensions.displayVolume
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.beerxml2proto.proto.Hop
import beer.hapibrew.beerxml2proto.proto.HopUse
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.DividerItemDecoration
import org.jetbrains.anko.sdk25.coroutines.onClick

class FormSpargeStep(val controller: SessionStepController) : VerticalStepperFormStep {
    private lateinit var ctx: Context
    private lateinit var firstWortText: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var spargeText: TextView
    private lateinit var spargeContinue: Button

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_sparge, container, false)
        this.ctx = inflater.context
        this.firstWortText = v.findViewById(R.id.firstWortText)
        this.recyclerView = v.findViewById(R.id.recyclerView)
        this.spargeText = v.findViewById(R.id.spargeText)
        this.spargeContinue = v.findViewById(R.id.spargeContinue)

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let {
            spargeText.text = ctx.getString(R.string.session_step_sparge_text,
                    displayVolume(view.context, it.spargeWater, true, forceLarge = true),
                    it.recipe.mash!!.steps[0].displayTemperature(ctx, true))

            showHops(Wire.get(it.recipe.hops, emptyList())
                    .filter { it.use == HopUse.FIRST_WORT }
                    .sortedByDescending(Hop::time)
            )

            spargeContinue.run {
                text = ctx.getString(R.string.session_step_sparge_continue)
                isEnabled = !controller.isStepCompleted(stepNumber)
                onClick {
                    controller.stepDone(stepNumber)
                    isEnabled = false
                }
            }
        }
    }

    private fun showHops(hops: List<Hop>) {
        if (hops.isNotEmpty()) {
            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.addItemDecoration(DividerItemDecoration(ctx, 0, 16))
            val header = HopItemHeader(HopUse.FIRST_WORT, false)
            val items = hops.map {
                HopItem(it, header)
            }
            val adapter = FlexibleAdapter<HopItem>(items)
            adapter.setDisplayHeadersAtStartUp(true)
            recyclerView.adapter = adapter

            firstWortText.visibility = View.VISIBLE
            recyclerView.visibility = View.VISIBLE
        } else {
            firstWortText.visibility = View.GONE
            recyclerView.visibility = View.GONE
        }
    }
}