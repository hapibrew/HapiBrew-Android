package beer.hapibrew.app.session

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.extensions.displayInfuseTemperature
import beer.hapibrew.app.extensions.displayVolume
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import org.jetbrains.anko.sdk25.coroutines.onClick

class FormPreMashStep(val controller:SessionStepController) : VerticalStepperFormStep {
    lateinit var ctx: Context
    lateinit var premashText: TextView
    lateinit var premashContinue: Button

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_premash, container, false)
        ctx = inflater.context
        premashText = v.findViewById(R.id.premashText)
        premashContinue = v.findViewById(R.id.premashContinue)

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        controller.sessionData.let {
            premashText.text = ctx.getString(R.string.session_step_premash_text,
                    displayVolume(view.context, it.mashWater, true, forceLarge = true))

            premashContinue.run {
                text = ctx.getString(R.string.session_step_premash_continue,
                        it.recipe.mash!!.steps[0].displayInfuseTemperature(ctx, true))
                isEnabled = !controller.isStepCompleted(stepNumber)
                onClick {
                    controller.stepDone(stepNumber)
                    isEnabled = false
                }
            }
        }
    }
}