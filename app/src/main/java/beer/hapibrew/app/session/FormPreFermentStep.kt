package beer.hapibrew.app.session

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.brixToSg
import beer.hapibrew.app.calculators.sgToBrix
import beer.hapibrew.app.controllers.session.SessionStepController
import beer.hapibrew.app.extensions.*
import beer.hapibrew.app.verticalstepperform.interfaces.VerticalStepperFormStep
import beer.hapibrew.beerxml2proto.proto.Recipe
import com.squareup.wire.Wire
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.textChangedListener

class FormPreFermentStep(val controller: SessionStepController) : VerticalStepperFormStep {
    lateinit var ctx: Context
    lateinit var prefermentContinue: Button
    lateinit var switchGravityUnit: Button
    lateinit var originalGravity: TextInputLayout
    lateinit var originalGravityValue: TextInputEditText

    var useSg:Boolean = true
    var isStepCompleted:Boolean = false

    override fun createStepContentView(inflater: LayoutInflater, container: ViewGroup, stepNumber: Int): View {
        val v = inflater.inflate(R.layout.session_step_preferment, container, false)
        ctx = inflater.context
        prefermentContinue = v.findViewById(R.id.prefermentContinue)
        switchGravityUnit = v.findViewById(R.id.switchGravityUnit)
        originalGravity = v.findViewById(R.id.originalGravity)
        originalGravityValue = v.findViewById(R.id.originalGravityValue)
        originalGravityValue.textChangedListener {
            afterTextChanged {
                enableButton()
            }
        }
        switchGravityUnit.setOnClickListener {
            useSg = !useSg
            setUnitButtonText(switchGravityUnit, originalGravity)
            convertGravityUnit(originalGravityValue, originalGravity, useSg)
        }

        return v
    }

    override fun bindStepContentView(view: View, stepNumber: Int) {
        isStepCompleted = controller.isStepCompleted(stepNumber)
        controller.sessionData.let { sessionData ->
            prefermentContinue.run {
                text = ctx.getString(R.string.session_step_preferment_continue)
                onClick {
                    controller.updateGravityValues(og = if (useSg) originalGravityValue.toDouble() else originalGravityValue.toDouble().bxToSg())
                    controller.stepDone(stepNumber)
                    isEnabled = false
                }
            }

            setUnitButtonText(switchGravityUnit, originalGravity)

            val og:Double
            if (sessionData.og == 0.0) {
                og = Wire.get(sessionData.recipe.est_og, Recipe.DEFAULT_EST_OG)!!
            } else {
                og = sessionData.og
            }
            originalGravityValue.setText(og.toSgFormat(), TextView.BufferType.EDITABLE)
        }

        originalGravity.isErrorEnabled = !isStepCompleted
        originalGravityValue.isEnabled = !isStepCompleted

        enableButton()
    }

    private fun enableButton() {
        prefermentContinue.isEnabled = !isStepCompleted &&
                isValidGravity(ctx, originalGravityValue.toDouble(), useSg, originalGravity)
    }

    private fun setUnitButtonText(button: TextView, hintOg:TextInputLayout) {
        val currentUnit = if (useSg) R.string.unit_sg else R.string.unit_brix
        val inverseUnit = if (useSg) R.string.unit_brix else R.string.unit_sg
        button.text = button.context.getString(R.string.use_unit, button.context.getString(inverseUnit))
        hintOg.hint = hintOg.context.getString(R.string.original_gravity, hintOg.context.getString(currentUnit))
    }

    private fun convertGravityUnit(valueView: TextInputEditText, wrapperView: TextInputLayout, toSg:Boolean) {
        val currentValue = valueView.toDouble()
        if (isValidGravity(valueView.context, currentValue, !toSg, wrapperView)) {
            if (toSg) {
                valueView.setText(brixToSg(currentValue).toSgFormat(), TextView.BufferType.EDITABLE)
            } else {
                valueView.setText(sgToBrix(currentValue).toBrixFormat(), TextView.BufferType.EDITABLE)
            }
        }
    }
}