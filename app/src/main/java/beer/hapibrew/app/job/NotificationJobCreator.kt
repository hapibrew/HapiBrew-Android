package beer.hapibrew.app.job

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator
import timber.log.Timber

object NotificationJobCreator : JobCreator {
    override fun create(tag: String): Job? {
        Timber.d("tag %s", tag)

        if (tag.startsWith(AlarmJob.TAG)) {
            return AlarmJob()
        }

        return null
    }
}