package beer.hapibrew.app.job

import android.content.Intent
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.extensions.BUNDLE_SESSION_ID
import beer.hapibrew.app.extensions.currentEpoch
import beer.hapibrew.app.service.NotificationService
import beer.hapibrew.beerxml2proto.proto.Hop
import beer.hapibrew.beerxml2proto.proto.HopUse
import com.evernote.android.job.Job
import com.evernote.android.job.JobManager
import com.evernote.android.job.JobProxy.Common.startWakefulService
import com.evernote.android.job.JobRequest
import com.evernote.android.job.util.support.PersistableBundleCompat
import com.squareup.wire.Wire
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AlarmJob : Job() {

    companion object {
        val TAG = "JOB_ALARM_"

        fun scheduleJob(sessionData: SessionData) {
            val tag = TAG + sessionData.getAlarmTag()
            val extras = PersistableBundleCompat()
            extras.putLong(BUNDLE_SESSION_ID, sessionData.id)

            scheduleStepEnd(sessionData, tag, extras)

            scheduleHops(sessionData, tag, extras)
        }

        private fun scheduleStepEnd(sessionData: SessionData, tag:String, extras:PersistableBundleCompat) {
            if (sessionData.phase == SessionData.Phase.FERMENTATION) {
                val interval = TimeUnit.MILLISECONDS.convert(4L, TimeUnit.HOURS)
                val fireDelay = maxOf(1L, (sessionData.stepEndEpoch - currentEpoch()))
                JobRequest.Builder(tag)
                        .setExecutionWindow(fireDelay, fireDelay + interval)
                        .setExtras(extras)
                        .setUpdateCurrent(false)
                        .build()
                        .schedule()
            } else {
                val fireDelay = maxOf(1L, sessionData.stepEndEpoch - currentEpoch() - 5_000)
                JobRequest.Builder(tag)
                        .setExact(fireDelay)
                        .setExtras(extras)
                        .setUpdateCurrent(false)
                        .build()
                        .schedule()
            }
        }

        private fun scheduleHops(sessionData: SessionData, tag:String, extras:PersistableBundleCompat) {
            if (sessionData.phase == SessionData.Phase.FERMENTATION) {
                val hops = Wire.get(sessionData.recipe.hops, emptyList())
                hops.forEach {
                    val type = Wire.get(it.type, Hop.DEFAULT_USE)
                    if (type == HopUse.DRY_HOP /*|| type == HopUse.AROMA*/) {

                    }
                }
            } else if (sessionData.phase == SessionData.Phase.BOIL) {
                val hops = Wire.get(sessionData.recipe.hops, emptyList())
                hops.forEach {
                    val type = Wire.get(it.type, Hop.DEFAULT_USE)
                    if (type == HopUse.BOIL /*|| type == HopUse.AROMA*/) {

                    }
                }
            }
        }

        fun cancelJobs(sessionData: SessionData) {
            JobManager.instance().cancelAllForTag(sessionData.getAlarmTag())
        }
    }

    override fun onRunJob(params: Params): Result {
        Timber.d("onRunJob")
        val serviceIntent = Intent(NotificationService.ACTION_ALARM, null, context, NotificationService::class.java)
        params.extras.let {
            Timber.d("onRunJob %d", it[BUNDLE_SESSION_ID] as Long)
            serviceIntent.putExtra(BUNDLE_SESSION_ID, it[BUNDLE_SESSION_ID] as Long)
        }

        startWakefulService(context, serviceIntent)

        return Result.SUCCESS
    }
}