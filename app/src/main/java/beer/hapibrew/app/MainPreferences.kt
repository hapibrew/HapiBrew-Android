package beer.hapibrew.app

import android.content.Context
import android.content.SharedPreferences
import org.jetbrains.anko.defaultSharedPreferences

class MainPreferences(context: Context) {
    private val DEFAULT_METRIC = "default_metric"
    private val GF_220V = "gf_220v"
    private val INITIAL_IMPORT = "initial_import"
    private val ALARM = "alarm"

    private val _prefs: SharedPreferences = context.defaultSharedPreferences

    var useMetric: Boolean
        get() = _prefs.getBoolean(DEFAULT_METRIC, true)
        set(value) = _prefs.edit().putBoolean(DEFAULT_METRIC, value).apply()

    var isGF220V:Boolean
        get() = _prefs.getBoolean(GF_220V, true)
        set(value) = _prefs.edit().putBoolean(GF_220V, value).apply()

    var hasInitialImport:Boolean
        get() = _prefs.getBoolean(INITIAL_IMPORT, false)
        set(value) = _prefs.edit().putBoolean(INITIAL_IMPORT, value).apply()

    var alarm: String
        get() = _prefs.getString(ALARM, "")
        set(value) = _prefs.edit().putString(ALARM, value).apply()
}