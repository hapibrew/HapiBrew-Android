package beer.hapibrew.app.view

import android.content.Context
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import beer.hapibrew.app.R
import beer.hapibrew.app.data.SessionData

class SessionButton : AppCompatButton {
    var initialText: String = ""

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val set = intArrayOf(android.R.attr.text)
        val a = context.obtainStyledAttributes(attrs, set)
        initialText = a.getText(0).toString()
        a.recycle()
    }

    fun setState(sessionData: SessionData, isStepCompleted: Boolean) {
        if (isStepCompleted ||sessionData.getStepProgress() >= 1.0) {
            isEnabled = !isStepCompleted
            text = context.getString(R.string.session_step_continue)
        } else {
            if (sessionData.stepStartEpoch == 0L) {
                text = initialText
            } else {
                if (sessionData.stepPauseEpoch != 0L) {
                    text = context.getString(R.string.session_step_resume)
                } else {
                    text = context.getString(R.string.session_step_pause)
                }
            }
        }
    }
}
