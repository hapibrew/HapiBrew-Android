package beer.hapibrew.app.view

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Rect
import android.os.Parcel
import android.os.Parcelable
import android.support.design.internal.ParcelableSparseArray
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.ScrollView
import beer.hapibrew.app.R
import eu.davidea.flexibleadapter.utils.DrawableUtils
import kotlinx.android.synthetic.main.expandable_cardview.view.*
import org.jetbrains.anko.childrenSequence
import org.jetbrains.anko.dip
import timber.log.Timber

class ExpandableCardView : CardView {

    lateinit var expandableHolder: View
    var arrow: View? = null

    var expanded: Boolean = false

    var animSet: AnimatorSet = AnimatorSet()

    var expandableHolderHeight = 0

    var scrollView:ScrollView? = null

    var animateScrollY:Int = 0
    var lastScrollFraction:Float = 0f
    var animationPadding = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val v = inflate(context, R.layout.expandable_cardview, this)

        setContentPadding(0, 0, 0, 0)

        arrow = v.cardArrow

        v.cardTitleHolder.setOnClickListener {
            if (scrollView == null)
                scrollView = findScrollView(v)

            toggle()
        }

        if (attrs !== null) {
            animationPadding = context.dip(16)

            val a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableCardView)
            val titleRes = a.getResourceId(R.styleable.ExpandableCardView_ecv_title, -1)
            val contentRes = a.getResourceId(R.styleable.ExpandableCardView_ecv_content_layout, -1)
            val buttonsRes = a.getResourceId(R.styleable.ExpandableCardView_ecv_buttons_layout, -1)
            expanded = a.getBoolean(R.styleable.ExpandableCardView_ecv_expanded, expanded)

            if (titleRes != -1) {
                v.cardTitle.text = context.getString(titleRes)
            }


            if (contentRes != -1) {
                inflate(context, contentRes, v.cardContentHolder)
            }

            if (buttonsRes != -1) {
                val bv = inflate(context, buttonsRes, v.buttonsContentHolder) as ViewGroup
                if (bv.getChildAt(0) is ViewGroup) {
                    bv.getChildAt(0).childrenSequence()
                            .filterIsInstance(Button::class.java)
                            .forEach {
                                DrawableUtils.setBackgroundCompat(it, DrawableUtils.getSelectableBackgroundCompat(
                                        ContextCompat.getColor(context, R.color.card_button_background),
                                        ContextCompat.getColor(context, R.color.card_button_selected),
                                        ContextCompat.getColor(context, R.color.card_button_selected)))
                            }
                } else {
                    bv.childrenSequence()
                            .filterIsInstance(Button::class.java)
                            .forEach {
                                DrawableUtils.setBackgroundCompat(it, DrawableUtils.getSelectableBackgroundCompat(
                                        ContextCompat.getColor(context, R.color.card_button_background),
                                        ContextCompat.getColor(context, R.color.card_button_selected),
                                        ContextCompat.getColor(context, R.color.card_button_selected)))
                            }
                }
            }

            with(v.cardExpandableHolder) {
                expandableHolder = this
                measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
                expandableHolderHeight = measuredHeight
            }

            a.recycle()
        }

        showExpanded(false)
    }

    private fun findScrollView(v:View):ScrollView? {
        if (v is ScrollView)
            return v

        var p: ViewParent? = v.parent
        var i = 10
        while (p != null && i > 0) {
            if (p is ScrollView) {
                return p
            }
            p = p.parent
            i--
        }

        return null
    }

    private fun toggle() {
        expanded = !expanded
        showExpanded(true)
    }

    fun showExpanded(animate: Boolean) {
        if (animate) {
            with(animSet) {
                removeAllListeners()
                cancel()
            }
            animSet = AnimatorSet()
            val rotationAnim: ObjectAnimator
            val expandAnim: ValueAnimator
            if (expanded) {
                rotationAnim = ObjectAnimator.ofFloat(arrow, "rotation", 180f, 0f)
                expandAnim = ValueAnimator.ofInt(0, expandableHolderHeight)
            } else {
                rotationAnim = ObjectAnimator.ofFloat(arrow, "rotation", 0f, 180f)
                expandAnim = ValueAnimator.ofInt(expandableHolder.height, 0)
            }
            animateScrollY = calculateScroll(true)
            lastScrollFraction = 0f
            expandAnim.addUpdateListener(mAnimatorUpdateListener)
            with(animSet) {
                addListener(mAnimatorListener)
                duration = 200
                interpolator = LinearInterpolator()
                playTogether(rotationAnim, expandAnim)
                start()
            }
        } else {
            showExpandLayout(expanded)
        }
    }

    private fun calculateScroll(fixBottom:Boolean):Int {
        val scrollRect = Rect()
        val itemRect = Rect()
        scrollView!!.getDrawingRect(scrollRect)
        this@ExpandableCardView.getHitRect(itemRect)

        if (fixBottom)
            itemRect.bottom += expandableHolderHeight // bottom value on end

        if (itemRect.bottom > scrollRect.bottom) {
            var value = itemRect.bottom - scrollRect.bottom + animationPadding
            itemRect.top -= value // top value after animation
            // check if need to correct top value; don't hide header
            if (itemRect.top < scrollRect.top)
                value -= scrollRect.top - itemRect.top

            return value
        } else {
            return 0
        }
    }
    var mAnimatorListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationStart(animation: Animator) {
            if (expanded) {
                with(expandableHolder) {
                    val newLayoutParams = layoutParams
                    newLayoutParams.height = 0
                    layoutParams = newLayoutParams
                    visibility = View.VISIBLE
                }
            }
        }

        override fun onAnimationEnd(animation: Animator) {
            if (!expanded) {
                expandableHolder.visibility = View.GONE
            } else if (animateScrollY > 0) {
                val newScroll = calculateScroll(false)
                Timber.d("Fix scroll %d", newScroll)
                scrollView?.smoothScrollBy(0, calculateScroll(false))
            }
        }

        override fun onAnimationCancel(animation: Animator) {
        }

        override fun onAnimationRepeat(animation: Animator) {
        }
    }
    var mAnimatorUpdateListener: ValueAnimator.AnimatorUpdateListener = ValueAnimator.AnimatorUpdateListener { animation ->
        val value = animation.animatedValue as Int
        with(expandableHolder) {
            val newLayoutParams = layoutParams
            newLayoutParams.height = value
            layoutParams = newLayoutParams

            if (expanded && animateScrollY > 0) {
                val fraction = animation.animatedFraction
                val animateBy = ((fraction - lastScrollFraction) * animateScrollY).toInt()
                Timber.d("Animate by diff=%d for fraction now=%.2f, last=%.2f", animateBy, fraction, lastScrollFraction)
                scrollView?.scrollBy(0, animateBy)
                lastScrollFraction = fraction
            }
        }
    }

    private fun showExpandLayout(expand: Boolean) {
        arrow?.rotation = (if (expand) 0 else 180).toFloat()
        expandableHolder.visibility = if (expand) View.VISIBLE else View.GONE
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.expanded = expanded
        ss.childrenStates = ParcelableSparseArray()
        for (i in 0 until childCount) {
            getChildAt(i).saveHierarchyState(ss.childrenStates)
        }
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        expanded = ss.expanded
        for (i in 0 until childCount) {
            getChildAt(i).restoreHierarchyState(ss.childrenStates)
        }
        showExpanded(false)
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>) {
        dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>) {
        dispatchThawSelfOnly(container)
    }

    internal class SavedState : View.BaseSavedState {
        var expanded: Boolean = false
        var childrenStates: ParcelableSparseArray? = null

        constructor(superState: Parcelable) : super(superState)

        private constructor(`in`: Parcel, classLoader: ClassLoader) : super(`in`) {
            expanded = `in`.readInt() == 1
            childrenStates = `in`.readParcelable(classLoader)
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(if (expanded) 1 else 0)
            out.writeParcelable(childrenStates, flags)
        }

        companion object {
            @JvmField val CREATOR: Parcelable.ClassLoaderCreator<SavedState> = object : Parcelable.ClassLoaderCreator<SavedState> {
                override fun createFromParcel(source: Parcel, loader: ClassLoader): SavedState = SavedState(source, loader)
                override fun createFromParcel(source: Parcel?): SavedState = createFromParcel(null)
                override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
            }
        }
    }
}