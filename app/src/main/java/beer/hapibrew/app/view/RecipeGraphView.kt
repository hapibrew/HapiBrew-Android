package beer.hapibrew.app.view

import beer.hapibrew.app.R
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.RectF
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import beer.hapibrew.app.calculators.calculateAbvPercent
import beer.hapibrew.app.calculators.calculateFgPercent
import beer.hapibrew.app.calculators.calculateIbuPercent
import beer.hapibrew.app.calculators.calculateOgPercent


class RecipeGraphView : View {

    private var barRect = arrayOfNulls<RectF>(4)
    private var barValueRect = arrayOfNulls<RectF>(4)

    private var sizeInitialized = false
    private var haveValues = false

    private var barWidth:Float = 0f
    private var barHeight:Float = 0f
    private var barSpace:Float = 0f

    private var strokeWidth:Float = 0f

    private val barBackgroundPaint = Paint(ANTI_ALIAS_FLAG)
    private val barBackgroundStrokePaint = Paint()
    private val barForegroundPaint = Paint(ANTI_ALIAS_FLAG)

    val percents = doubleArrayOf(0.0, 0.0, 0.0, 0.0)

    constructor(context: Context?) : super(context) {
        init(context!!)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context!!)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context!!)
    }

    fun dip(value:Int):Float {
        return (value * resources.displayMetrics.density);
    }
    private fun init(context: Context) {
        strokeWidth = dip(1)

        barWidth = dip(10) + 2 * strokeWidth
        barSpace = dip(5)


        barBackgroundPaint.style = Paint.Style.FILL
        barBackgroundPaint.color = ContextCompat.getColor(context, R.color.white_smoke)

        barBackgroundStrokePaint.style = Paint.Style.FILL_AND_STROKE
        barBackgroundStrokePaint.strokeWidth = strokeWidth
        barBackgroundStrokePaint.color = ContextCompat.getColor(context, R.color.grey_whisper)

        barForegroundPaint.style = Paint.Style.FILL
        barForegroundPaint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
    }

    fun setData(og:Double, fg:Double, ibu:Double, abv:Double) {
        percents[0] = calculateOgPercent(og)
        percents[1] = calculateFgPercent(fg)
        percents[2] = calculateIbuPercent(ibu)
        percents[3] = calculateAbvPercent(abv)

        haveValues = true

        if (sizeInitialized)
            calculateValueBars()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        barHeight = h.toFloat()

        for (i in 0..(barRect.size - 1)) {
            val leftOffset = (barSpace * i) + (i * barWidth)
            barRect[i] = RectF(leftOffset, 0f, (leftOffset + barWidth), barHeight)
        }

        sizeInitialized = true

        if (haveValues)
            calculateValueBars()
    }

    fun calculateValueBars() {
        val topOffsets:List<Double> = percents.map {
            barHeight - (barHeight * it)
        }

        for (i in 0..(barValueRect.size - 1)) {
            val leftOffset = (barSpace * i) + (i * barWidth)
            barValueRect[i] = RectF(leftOffset, topOffsets[i].toFloat(), (leftOffset + barWidth), barHeight)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (!sizeInitialized)
            return

        canvas?.let {
            barRect.forEach {
                it?.let {
                    canvas.drawRect(it, barBackgroundStrokePaint)
                    it.inset(strokeWidth, strokeWidth)
                    canvas.drawRect(it, barBackgroundPaint)
                    it.inset(-strokeWidth, -strokeWidth) //reset inseting for another drawing
                }
            }

            if (haveValues) {
                barValueRect.forEach {
                    it?.let {
                        it.inset(strokeWidth, strokeWidth)
                        canvas.drawRect(it, barForegroundPaint)
                        it.inset(-strokeWidth, -strokeWidth) //reset inseting for another drawing
                    }
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val numOfBars = barRect.size
        val desiredWidth = (barWidth * numOfBars + barSpace * (numOfBars - 1) + 0.5f).toInt()
        val desiredHeight = 1000

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width: Int
        val height: Int

        if (widthMode == View.MeasureSpec.EXACTLY) {
            width = widthSize
        } else if (widthMode == View.MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize)
        } else {
            width = desiredWidth
        }

        if (heightMode == View.MeasureSpec.EXACTLY) {
            height = heightSize
        } else if (heightMode == View.MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize)
        } else {
            height = desiredHeight
        }

        setMeasuredDimension(width, height)
    }
}