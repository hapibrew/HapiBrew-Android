package beer.hapibrew.app.view

import beer.hapibrew.beerxml2proto.proto.Style
import beer.hapibrew.app.R
import beer.hapibrew.app.calculators.*
import beer.hapibrew.app.extensions.toOneDecimalFormat
import beer.hapibrew.app.extensions.toSgFormat
import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Paint.SUBPIXEL_TEXT_FLAG
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import com.squareup.wire.Wire


class RecipeDetailedGraphView : View {

    private var maxWidth = 2000

    private var barRect = arrayOfNulls<RectF>(5)
    private var barStyleRect = arrayOfNulls<RectF>(5)
    private var barValueLines = arrayOfNulls<RectF>(5)

    private var sizeInitialized = false
    private var haveValues = false
    private var haveStyle = false

    private var fullWidth:Int = 0
    private var barWidth:Float = 0f
    private var barHeight:Float = 0f
    private var barSpace:Float = 0f
    private var barLeftOffset:Float = 0f
    private var strokeWidth:Float = 0f

    private var textPadding = 20f
    private var textX = 0f
    private val textY = floatArrayOf(0f, 0f, 0f, 0f, 0f)
    private val textData = arrayOfNulls<String>(5)

    private val barBackgroundPaint = Paint(ANTI_ALIAS_FLAG)
    private val barBackgroundStrokePaint = Paint()
    private val barStylePaint = Paint(ANTI_ALIAS_FLAG)
    private val barValuePaint = Paint(ANTI_ALIAS_FLAG)
    private val textPaint = Paint(SUBPIXEL_TEXT_FLAG or ANTI_ALIAS_FLAG)

    private val valuePercents = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0)
    private val stylePercents = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    constructor(context: Context?) : super(context) {
        init(context!!)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context!!)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context!!)
    }

    fun dip(value:Int):Float {
        return (value * resources.displayMetrics.density);
    }
    fun sp(value:Int):Float {
        return (value * resources.displayMetrics.scaledDensity);
    }

    private fun init(context: Context) {
        maxWidth = resources.displayMetrics.widthPixels

        strokeWidth = dip(1)

        barHeight = dip(18) + 2 * strokeWidth
        barSpace = dip(4)
        textPadding = dip(10)

        barBackgroundPaint.style = Paint.Style.FILL
        barBackgroundPaint.color = ContextCompat.getColor(context, R.color.white_smoke)

        barBackgroundStrokePaint.style = Paint.Style.FILL_AND_STROKE
        barBackgroundStrokePaint.strokeWidth = dip(1)
        barBackgroundStrokePaint.color = ContextCompat.getColor(context, R.color.grey_whisper)

        barStylePaint.style = Paint.Style.FILL
        barStylePaint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
        barStylePaint.alpha = 64

        barValuePaint.style = Paint.Style.STROKE
        barValuePaint.strokeWidth = dip(1)
        barValuePaint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)

        textPaint.typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)
        textPaint.color = ContextCompat.getColor(context, R.color.textColorPrimary)
        textPaint.textSize = sp(12)
        textPaint.textAlign = Paint.Align.RIGHT
    }

    fun setData(og:Double, fg:Double, ibu:Double, srm:Double, abv:Double, style: Style?) {
        valuePercents[0] = calculateOgPercent(og)
        valuePercents[1] = calculateFgPercent(fg)
        valuePercents[2] = calculateIbuPercent(ibu)
        valuePercents[3] = calculateSrmPercent(srm)
        valuePercents[4] = calculateAbvPercent(abv)

        textData[0] = context.getString(R.string.beer_og, og.toSgFormat())
        textData[1] = context.getString(R.string.beer_fg, fg.toSgFormat())
        textData[2] = context.getString(R.string.beer_ibu, ibu.toInt())
        textData[3] = context.getString(R.string.beer_srm, srm.toInt())
        textData[4] = context.getString(R.string.beer_abv, abv.toOneDecimalFormat())

        if (style != null)
            calculateStylePercents(style)

        haveValues = true

        if (sizeInitialized) {
            calculateStyleBars()
            calculateValueLines()
        }
    }

    fun calculateStylePercents(style: Style) {
        stylePercents[0] = calculateOgPercent(Wire.get(style.og_min, Style.DEFAULT_OG_MIN)!!)
        stylePercents[1] = calculateOgPercent(Wire.get(style.og_max, Style.DEFAULT_OG_MAX)!!)
        stylePercents[2] = calculateFgPercent(Wire.get(style.fg_min, Style.DEFAULT_FG_MIN)!!)
        stylePercents[3] = calculateFgPercent(Wire.get(style.fg_max, Style.DEFAULT_FG_MAX)!!)
        stylePercents[4] = calculateIbuPercent(Wire.get(style.ibu_min, Style.DEFAULT_IBU_MIN)!!)
        stylePercents[5] = calculateIbuPercent(Wire.get(style.ibu_max, Style.DEFAULT_IBU_MAX)!!)
        stylePercents[6] = calculateSrmPercent(Wire.get(style.srm_min, Style.DEFAULT_SRM_MIN)!!)
        stylePercents[7] = calculateSrmPercent(Wire.get(style.srm_max, Style.DEFAULT_SRM_MAX)!!)
        stylePercents[8] = calculateAbvPercent(Wire.get(style.abv_min, Style.DEFAULT_ABV_MIN)!!)
        stylePercents[9] = calculateAbvPercent(Wire.get(style.abv_max, Style.DEFAULT_ABV_MAX)!!)

        haveStyle = true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (!sizeInitialized)
            return

        canvas?.let {

            barRect.forEach {
                it?.let {
                    canvas.drawRect(it, barBackgroundStrokePaint)
                    it.inset(strokeWidth, strokeWidth)
                    canvas.drawRect(it, barBackgroundPaint)
                    it.inset(-strokeWidth, -strokeWidth) //reset inseting for another drawing
                }
            }

            if (haveStyle) {
                barStyleRect.forEach {
                    it?.let {
                        it.inset(strokeWidth, strokeWidth)
                        canvas.drawRect(it, barStylePaint)
                        it.inset(-strokeWidth, -strokeWidth)
                    }
                }
            }

            if (haveValues) {
                textData.forEachIndexed { i, s ->
                    canvas.drawText(s, textX, textY[i], textPaint)
                }

                barValueLines.forEach {
                    it?.let {
                        canvas.drawLine(it.left, it.top, it.right, it.bottom, barValuePaint)
                    }
                }
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        fullWidth = w

        val r = Rect()
        val text = context.getString(R.string.beer_abv, 88.8.toOneDecimalFormat())
        textPaint.getTextBounds(text, 0, text.length, r)

        barLeftOffset = r.width() + textPadding
        barWidth = w - barLeftOffset

        textX = r.width().toFloat()
        val textYCenter = ((textPaint.descent() + textPaint.ascent()) / 2)

        for (i in 0..(barRect.size - 1)) {
            val topOffset = (barSpace * i) + (i * barHeight)
            barRect[i] = RectF(barLeftOffset, topOffset, w.toFloat(), (topOffset + barHeight))
            textY[i] = (barRect[i]!!.top + barRect[i]!!.bottom) / 2 - textYCenter
        }

        sizeInitialized = true

        if (haveValues) {
            calculateStyleBars()
            calculateValueLines()
        }
    }

    fun calculateStyleBars() {
        if (!haveStyle)
            return

        val barOffsets:List<Double> = stylePercents.map {
            barWidth * it
        }

        for (i in 0..(barStyleRect.size - 1)) {
            val leftOffset = barLeftOffset + barOffsets[i*2]
            val rightOffset = barLeftOffset + barOffsets[i*2+1]
            val topOffset = ((barSpace * i) + (i * barHeight))
            barStyleRect[i] = RectF(leftOffset.toFloat(), topOffset, rightOffset.toFloat(), (topOffset + barHeight))
        }
    }

    fun calculateValueLines() {
        val leftOffsets:List<Double> = valuePercents.map {
            barWidth * it
        }

        for (i in 0..(barValueLines.size - 1)) {
            val leftOffset = maxOf((2 * strokeWidth), minOf(leftOffsets[i].toFloat(), (barWidth - (2 * strokeWidth))))
            val barValue:Float = barLeftOffset + leftOffset
            val topOffset = (barSpace * i) + (i * barHeight)
            barValueLines[i] = RectF(barValue, topOffset, barValue, (topOffset + barHeight))
            barValueLines[i]?.inset(0f, 2 * strokeWidth)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val numOfBars = barRect.size
        val desiredWidth = (maxWidth + 0.5f).toInt()
        val desiredHeight = (barHeight * numOfBars + barSpace * (numOfBars - 1) + 0.5f).toInt()

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width: Int
        val height: Int

        if (widthMode == View.MeasureSpec.EXACTLY) {
            width = widthSize
        } else if (widthMode == View.MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize)
        } else {
            width = desiredWidth
        }

        if (heightMode == View.MeasureSpec.EXACTLY) {
            height = heightSize
        } else if (heightMode == View.MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize)
        } else {
            height = desiredHeight
        }

        setMeasuredDimension(width, height)
    }
}