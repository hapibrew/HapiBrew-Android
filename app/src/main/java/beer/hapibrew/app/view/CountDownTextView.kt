package beer.hapibrew.app.view

import android.content.Context
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.widget.TextView
import beer.hapibrew.app.R
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.dialog.StepDurationDialog
import beer.hapibrew.app.extensions.currentEpoch
import beer.hapibrew.app.extensions.millisecondsAsMinSec
import beer.hapibrew.app.extensions.millisecondsAsTime
import org.joda.time.DateTimeConstants
import org.joda.time.Period
import org.joda.time.PeriodType

class CountDownTextView : TextView {

    var countdown: CountDownTimer? = null
    private var currentValue: Long = 0

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun startTimer(remaining:Long, sessionData: SessionData, onFinish:() -> Unit, onTick:(() -> Unit)?) {
        start(remaining, (sessionData.phase != SessionData.Phase.FERMENTATION), onFinish, onTick)
    }

    fun setTimer(isStepCompleted:Boolean, sessionData: SessionData, onFinish:() -> Unit, onTick:(() -> Unit)?) {
        if (isStepCompleted || sessionData.getStepProgress() >= 1.0) {
            text = context.getString(R.string.session_step_time_done)
            stop()
            setOnClickListener(null)
        } else {
            if (sessionData.stepStartEpoch == 0L) {
                showTime(sessionData.getStepDuration(), sessionData.phase)
            } else {
                if (sessionData.stepPauseEpoch != 0L) {
                    showTime(sessionData.stepEndEpoch - sessionData.stepPauseEpoch, sessionData.phase)
                } else {
                    startTimer(sessionData.stepEndEpoch - currentEpoch(), sessionData, onFinish, onTick)
                }
            }
        }
    }

    fun setStepDurationCallback(activity: AppCompatActivity, sessionData: SessionData, callback: StepDurationDialog.OnStepDurationDialogListener) {
        setOnClickListener {
            val ft = activity.supportFragmentManager.beginTransaction()
            val prev = activity.supportFragmentManager.findFragmentByTag("StepDurationDialog")

            if (prev != null && StepDurationDialog::class.java.isInstance(prev)) {
                ft.remove(prev)
            }

            val f: StepDurationDialog = if (sessionData.phase == SessionData.Phase.FERMENTATION)
                StepDurationDialog.newInstance(getRemainingDays(), true)
            else
                StepDurationDialog.newInstance(getRemainingMinutes(), false)
            f.setCallback(callback)
            f.show(ft, "StepDurationDialog")
        }
    }

    fun start(time: Long, forceMinSec:Boolean = false, onFinish:() -> Unit, onTick:(() -> Unit)?) {
        stop()
        countdown = null
        currentValue = time
        val interval:Int
        if (!forceMinSec) {
            val period = Period(time).normalizedStandard(PeriodType.dayTime())
            if (period.days > 0)
                interval = DateTimeConstants.MILLIS_PER_HOUR
            else if (period.hours > 0)
                interval = DateTimeConstants.MILLIS_PER_MINUTE
            else
                interval = DateTimeConstants.MILLIS_PER_SECOND
        } else {
            interval = 1000
        }

        countdown = object : CountDownTimer(time, interval.toLong()) {
            override fun onFinish() {
                text = context.getString(R.string.session_step_time_done)
                onFinish()
            }

            override fun onTick(millisUntilFinished: Long) {
                if (forceMinSec) {
                    text = millisecondsAsMinSec(millisUntilFinished)
                } else {
                    text = millisecondsAsTime(context, millisUntilFinished)
                }
                currentValue = millisUntilFinished
                if (onTick != null)
                    onTick()
            }
        }

        countdown?.start()
    }

    fun showTime(duration:Long, phase: SessionData.Phase) {
        currentValue = duration
        text = when (phase) {
            SessionData.Phase.FERMENTATION -> millisecondsAsTime(context, duration)
            SessionData.Phase.MASH, SessionData.Phase.BOIL -> millisecondsAsMinSec(duration)
            else ->  context.getString(R.string.session_step_time_waiting)
        }
        stop()
    }

    fun stop() {
        countdown?.cancel()
    }

    fun getRemainingDays(): Int {
        val period = Period(currentValue).normalizedStandard(PeriodType.days())
        return period.days
    }

    fun getRemainingMinutes(): Int {
        val period = Period(currentValue).normalizedStandard(PeriodType.minutes())
        return period.minutes
    }
}
