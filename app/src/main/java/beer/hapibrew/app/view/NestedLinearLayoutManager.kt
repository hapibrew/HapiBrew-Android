package beer.hapibrew.app.view

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet

class NestedLinearLayoutManager : LinearLayoutManager {

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, orientation: Int, reverseLayout: Boolean) : super(context, orientation, reverseLayout)  {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        isAutoMeasureEnabled = true
    }

    override fun canScrollVertically(): Boolean {
        return false
    }
}
