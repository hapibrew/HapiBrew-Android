package beer.hapibrew.app.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet

class NestedRecyclerView : RecyclerView {
    constructor(context: Context?) : super(context) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        isNestedScrollingEnabled = false
        setHasFixedSize(true)
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        isNestedScrollingEnabled = false
        isLayoutFrozen = true
    }
}
