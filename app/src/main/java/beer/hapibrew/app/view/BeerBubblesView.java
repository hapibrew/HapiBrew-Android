package beer.hapibrew.app.view;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

/**
 * $desc
 *
 * @author zxb
 * @author Andy Barber
 * @date 15/10/28 下午5:46
 * @date 21/04/2016
 */
public class BeerBubblesView extends View {

    private static final String TAG = "BeerProgressView";
    private static final String STATE_INSTANCE = "state_instance";
    private static final String STATE_MAX = "state_max";
    private static final String STATE_PROGRESS = "state_progress";
    private static final String STATE_WAVE_COLOR = "state_wave_color";
    private static final String STATE_BUBBLE_COLOR = "state_bubble_color";
    private static final String STATE_BUBBLE_COUNT = "state_bubble_count";
    private static final int BEER_DEFAULT_COLOR = Color.parseColor("#EFA601");
    private static final int BUBBLE_DEFAULT_COLOR = Color.parseColor("#FFFFFF");
    private static final int BUBBLE_DEFAULT_COUNT = 20;
    private static final int BUBBLE_FPS = 30;

    private RectF mBorderRectF;

    private float mBorderRadius = dp2px(2);
    private float mHaftBorderRadius = mBorderRadius / 2;
    private float mBeerProgressHeight = 50;
    private int mBorderWidth = dp2px(3);
    private int mBeerWidth;
    private int mBeerHeight;
    private int mAmplitude = dp2px(3);
    private int mAngle = 0;
    private int mMax = 100;
    private int mBeerProgress = 0;
    private int mBeerColor = BEER_DEFAULT_COLOR;
    private int mBubbleCount = BUBBLE_DEFAULT_COUNT;

    //bubble vars
    private Runnable mDrawBubblesRunnable;
    private Runnable mIndeterminateRunnable;
    private Bubble[] mBubbles;
    private Handler handler = new Handler();
    private long mStartMilli;
    private int mBubbleHeight;
    private int mBubbleTopMargin;
    private int mBubbleWidth;
    private int mBubbleColor = BUBBLE_DEFAULT_COLOR;

    public BeerBubblesView(Context context) {
        this(context, null);
    }

    public BeerBubblesView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BeerBubblesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        /*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BeerProgressView);

        mBorderWidth = a.getDimensionPixelSize(R.styleable.BeerProgressView_waveBorderWidth, mBorderWidth);
        mAmplitude = a.getDimensionPixelSize(R.styleable.BeerProgressView_waveAmplitude, mAmplitude);
        mBorderRadius = a.getDimensionPixelSize(R.styleable.BeerProgressView_waveBorderRadius, (int)mBorderRadius);
        mHaftBorderRadius = mBorderRadius / 2;

        mBeerColor = a.getColor(R.styleable.BeerProgressView_beerColor, mBeerColor);
        mMax = a.getInt(R.styleable.BeerProgressView_waveMax, 100);
        mBeerProgress = a.getInteger(R.styleable.BeerProgressView_beerProgress, 0);
        mBubbleColor = a.getColor(R.styleable.BeerProgressView_bubbleColor, mBubbleColor);
        mBubbleCount = a.getInteger(R.styleable.BeerProgressView_bubbleCount, BUBBLE_DEFAULT_COUNT);

        a.recycle();*/

        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBeerHeight < 50)
            return;

        drawBubbles(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mBeerWidth = (int) (getMeasuredWidth() - mHaftBorderRadius);
        mBeerHeight = (int) (getMeasuredHeight() - mHaftBorderRadius);

        mBorderRectF.set(mHaftBorderRadius, mHaftBorderRadius, mBeerWidth, mBeerHeight);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_INSTANCE, super.onSaveInstanceState());
        bundle.putInt(STATE_MAX, mMax);
        bundle.putInt(STATE_PROGRESS, mBeerProgress);
        bundle.putInt(STATE_WAVE_COLOR, mBeerColor);
        bundle.putInt(STATE_BUBBLE_COLOR, mBubbleColor);
        bundle.putInt(STATE_BUBBLE_COUNT, mBubbleCount);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle){
            Bundle bundle = (Bundle) state;
            mMax = bundle.getInt(STATE_MAX, 100);
            mBeerProgress = bundle.getInt(STATE_PROGRESS, 0);
            mBeerColor = bundle.getInt(STATE_WAVE_COLOR, BEER_DEFAULT_COLOR);
            mBubbleColor = bundle.getInt(STATE_BUBBLE_COLOR, BUBBLE_DEFAULT_COLOR);
            mBubbleCount = bundle.getInt(STATE_BUBBLE_COUNT, BUBBLE_DEFAULT_COUNT);
            super.onRestoreInstanceState(bundle.getParcelable(STATE_INSTANCE));
            return ;
        }
        super.onRestoreInstanceState(state);
    }

    private void init() {
        mBorderRectF = new RectF();

        mDrawBubblesRunnable = new Runnable() {
            public void run() {
                invalidate();
            }
        };
        mIndeterminateRunnable = new Runnable() {
            @Override
            public void run() {
//                Log.d(TAG, "progress " + mBeerProgress);
                setBeerProgress(mBeerProgress + 1);
                handler.postDelayed(mIndeterminateRunnable, 500);
            }
        };
    }

    private void drawBubbles(Canvas canvas) {
        mStartMilli = SystemClock.uptimeMillis();
        mBubbleHeight = mBeerHeight;
        mBubbleTopMargin = mBubbleHeight - ((int) mBeerProgressHeight) + 20;
        mBubbleWidth = canvas.getWidth();

        if (mBubbles == null || mBubbles.length != mBubbleCount) {
            createBubbles(mBubbleWidth, mBubbleHeight, mBubbleTopMargin);
        }

        /* Draw each Bubble */
        for (Bubble bubble : mBubbles) {
            /* Move the Bubble */
            bubble.update(BUBBLE_FPS, 0);

            /* Draw circle */
            bubble.draw(canvas);

            if (bubble.popped(mBubbleWidth, mBubbleHeight, mBubbleTopMargin)) {
                bubble.recycle(false, mBubbleWidth, mBubbleHeight, mBubbleTopMargin);
            }
        }

        long duration = SystemClock.uptimeMillis() - mStartMilli;
        handler.postDelayed(mDrawBubblesRunnable, (1000 / BUBBLE_FPS) - duration);
    }

    private void createBubbles(int width, int height, int topMargin) {
        mBubbles = new Bubble[mBubbleCount];
        for (int i = 0; i < mBubbleCount; i++) {
            this.mBubbles[i] = new Bubble(width, height, topMargin, mBubbleColor);
        }
    }

    private boolean isViewVisiable(){
        return getVisibility() == VISIBLE && getAlpha()*255>0;
    }

    private static int dp2px(int dp){
        return (int) (Resources.getSystem().getDisplayMetrics().density * dp);
    }


    /* public methods */
    public void start() {
        handler.post(mIndeterminateRunnable);
    }

    /**
     * get maximum progress value of view
     *
     * @return
     */
    public int getMax() {
        return mMax;
    }

    /**
     * set maximum value of progress view
     *
     * @param max
     */
    public void setMax(int max) {
        mMax = max;
    }

    /**
     * get the wave amplitude
     *
     * @return
     */
    public int getAmplitude() {
        return mAmplitude;
    }

    /**
     * set the wave amplitude
     *
     * @param amplitude
     */
    public void setAmplitude(int amplitude) {
        mAmplitude = amplitude;
    }

    /**
     * get current progress value of view
     *
     * @return
     */
    public int getBeerProgress() {
        return mBeerProgress;
    }

    /**
     * set the current beerProgress of the view
     *
     * @param beerProgress
     */
    public void setBeerProgress(int beerProgress) {
        mBeerProgress = beerProgress;
        if (mBeerProgress > mMax){
            mBeerProgress = mMax;
        }
        if (mBeerProgress < 0){
            mBeerProgress = 0;
        }
//        float pecent = mBeerProgress * 1.0f / mMax;
        float pecent = 1.0f;
        mBeerProgressHeight = pecent * mBeerHeight;
        invalidate();
    }

    /**
     * get the colour value of the view
     *
     * @return
     */
    public int getBeerColor() {
        return mBeerColor;
    }

    /**
     * set the colour value of the view
     *
     * @param beerColor
     */
    public void setBeerColor(int beerColor) {
        mBeerColor = beerColor;
    }

    /**
     * get the colour value of the bubbles
     *
     * @return
     */
    public int getBubbleColor() {
        return mBubbleColor;
    }

    /**
     * set the colour value of the bubbles
     *
     * @param bubbleColor
     */
    public void setBubbleColor(int bubbleColor) {
        mBubbleColor = bubbleColor;
        mBubbles = null;
    }

    /**
     * set the angle of the wave
     *
     * @param angle
     */
    public void setAngle(int angle){
        this.mAngle = angle;
        invalidate();
    }

    public static class Bubble {

        //standard beer size, speed & fps
        private static final int BUBBLE_SIZE = 20;
        private static final int SPEED = 30;
        private static final int FPS = 30;

        private int step;
        private double amp, freq, skew;
        private float x, y, radius, maxRadius;
        public boolean popped;
        private Paint paint;

        /**
         * Simple function for getting a random range.
         *
         * @param min The minimum int.
         * @param max The maximum int.
         * @return The random value.
         */
        public static int randRange(int min, int max) {
            int mod = max - min;
            double val = Math.ceil(Math.random() * 1000000) % mod;
            return (int)val + min;
        }

        /**
         * Create a bubble, passing in width & height of view
         *
         * @param width
         * @param height
         */
        public Bubble(int width, int height, int topMargin, int bubbleColour) {
            popped = false;
            paint = new Paint();
            paint.setColor(bubbleColour);
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeWidth(dp2px(1));
            paint.setAntiAlias(true);

            recycle(true, width, height, topMargin);
        }

        /**
         * Re-initialises the Bubble properties so that it appears to be a new
         * bubble.
         *
         * Although a bit of elegance is sacrificed this seemed to result in a
         * performance boost during initial testing.
         *
         * @param initial
         * @param width
         * @param height
         */
        public void recycle(boolean initial, int width, int height, int topMargin) {
            if(initial) {
                y = randRange(topMargin, height);
            } else {
                // Start at the bottom if not initial
                y = height + (randRange(0, 21) - 10 );
            }
            x = randRange(0, width);
            radius = 1;
            maxRadius = randRange(3, BUBBLE_SIZE);
            paint.setAlpha(randRange(100, 250));
            popped = false;
            step = 0;
            amp = Math.random() * 3;
            freq = Math.random() * 2;
            skew = Math.random() - 0.5;
        }

        /**
         * Update the size and position of a Bubble.
         *
         * @param fps The current FPS
         * @param angle The angle of the device
         */
        public void update(int fps, float angle) {
            double speed = (SPEED / FPS) * Math.log(radius);
            y -= speed;
            x += amp * Math.sin(freq * (step++ * speed)) + skew;
            if(radius < maxRadius) {
                radius += maxRadius / (((float)fps / SPEED) * radius);
                if(radius > maxRadius) radius = maxRadius;
            }
        }

        /**
         * Test whether a bubble is no longer visible.
         *
         * @param width Canvas width
         * @param height Canvas height
         * @param topMargin offset from top of view that bubble will be popped
         * @return A boolean indicating that the Bubble has drifted off allowable area
         */
        public boolean popped(int width, int height, int topMargin) {
            if(y + radius <= -20 ||
                    y - radius >= height ||
                    x + radius <= 0 ||
                    x - radius >= width ||
                    y - radius <= topMargin) {
                return true;
            }
            return false;
        }

        /**
         * Unified method for drawing the bubble.
         *
         * @param canvas The canvas to draw on
         */
        public void draw(Canvas canvas) {
            canvas.drawCircle(x, y, radius, paint);
        }

    }
}

