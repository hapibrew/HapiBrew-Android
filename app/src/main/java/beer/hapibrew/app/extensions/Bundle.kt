package beer.hapibrew.app.extensions

import android.os.Bundle

val BUNDLE_RECIPE_DATA: String
    get() = "recipeData"

val BUNDLE_SESSION_ID: String
    get() = "sessionId"

val BUNDLE_SESSION_DATA: String
    get() = "sessionData"

val BUNDLE_HIDE_FAB: String
    get() = "hideFab"

/**
 * User: mcxiaoke
 * Date: 16/1/26
 * Time: 16:43
 */

inline fun Bundle(body: Bundle.() -> Unit): Bundle {
    val bundle = Bundle()
    bundle.body()
    return bundle
}

inline fun Bundle(loader: ClassLoader, body: Bundle.() -> Unit): Bundle {
    val bundle = Bundle(loader)
    bundle.body()
    return bundle
}

inline fun Bundle(capacity: Int, body: Bundle.() -> Unit): Bundle {
    val bundle = Bundle(capacity)
    bundle.body()
    return bundle
}

inline fun Bundle(b: Bundle?, body: Bundle.() -> Unit): Bundle {
    val bundle = Bundle(b)
    bundle.body()
    return bundle
}