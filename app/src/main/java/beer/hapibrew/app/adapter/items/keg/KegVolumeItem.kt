package beer.hapibrew.app.adapter.items.keg

import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.keg_pressure_item_view.view.*

class KegVolumeItem(val value: String) : AbstractFlexibleItem<KegVolumeItem.ViewHolder>() {

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is String) {
            return this.value == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.keg_pressure_item_view
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(value)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(value: String) {
            itemView.kegPressureText.text = value
        }
    }
}