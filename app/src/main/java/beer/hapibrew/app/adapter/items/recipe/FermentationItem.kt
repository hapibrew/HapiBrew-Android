package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Fermentation
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayName
import beer.hapibrew.app.extensions.displayTemperature
import beer.hapibrew.app.extensions.displayTime
import beer.hapibrew.app.extensions.setAlternateBackground
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.fermentation_list_item.view.*

class FermentationItem(val fermentation: Fermentation, header: FermentationItemHeader?) : AbstractSectionableItem<FermentationItem.ViewHolder, FermentationItemHeader>(header) {

    override fun hashCode(): Int {
        return fermentation.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fermentation) {
            return this.fermentation == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.fermentation_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(fermentation)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(fermentation: Fermentation) {
            val context = itemView.context
            itemView.temp.text = fermentation.displayTemperature(context)
            itemView.time.text = fermentation.displayTime(context)
            itemView.stage.text = fermentation.displayName(context)
        }
    }
}