package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.MashStep
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayTemperature
import beer.hapibrew.app.extensions.displayTime
import beer.hapibrew.app.extensions.setAlternateBackground
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.mashstep_list_item.view.*

class MashStepItem(val mashStep: MashStep, header: MashStepItemHeader?) : AbstractSectionableItem<MashStepItem.ViewHolder, MashStepItemHeader>(header) {

    override fun hashCode(): Int {
        return mashStep.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is MashStep) {
            return this.mashStep == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.mashstep_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(mashStep)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(mashStep: MashStep) {
            val context = itemView.context
            itemView.temp.text = mashStep.displayTemperature(context)
            itemView.time.text = mashStep.displayTime(context)
            itemView.name.text = Wire.get(mashStep.name, MashStep.DEFAULT_NAME)
        }
    }
}