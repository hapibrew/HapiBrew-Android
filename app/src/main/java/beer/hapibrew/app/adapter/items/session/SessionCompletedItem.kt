package beer.hapibrew.app.adapter.items.session

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.app.extensions.toSgFormat
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.utils.DrawableUtils
import kotlinx.android.synthetic.main.session_list_completed_item.view.*

class SessionCompletedItem(session: SessionData, header: SessionItemHeader?) : SessionItem(session, header) {

    override fun getLayoutRes(): Int {
        return R.layout.session_list_completed_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<out IFlexible<*>>?, holder: SessionItem.ViewHolder?, position: Int, payloads: MutableList<Any?>?) {
        holder?.let {
            val ctx = holder.itemView.context
            holder.bindData(ctx, session)
            DrawableUtils.setBackgroundCompat(holder.itemView, DrawableUtils.getSelectableBackgroundCompat(
                    ContextCompat.getColor(ctx, R.color.card_recipe_background),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected)))
            holder.itemView.isActivated = adapter?.isSelected(position) ?: false
        }
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : SessionItem.ViewHolder(v, adapter) {
        override fun bindData(context: Context, session: SessionData) {
            itemView.sessionName.text = session.name
            itemView.sessionDate.text = session.getSessionDurationString(context)
            itemView.sessionOg.text = context.getString(R.string.beer_og, session.og.toSgFormat())
            itemView.sessionFg.text = context.getString(R.string.beer_fg, session.fg.toSgFormat())
        }
    }
}