package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder

class YeastItemHeader : AbstractHeaderItem<YeastItemHeader.ViewHolder>() {

    override fun equals(other: Any?): Boolean {
        return this == other
    }

    override fun hashCode(): Int {
        return hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.yeast_list_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
    }
}