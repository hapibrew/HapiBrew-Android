package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Misc
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayAmount
import beer.hapibrew.app.extensions.displayTime
import beer.hapibrew.app.extensions.displayType
import beer.hapibrew.app.extensions.setAlternateBackground
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.misc_list_item.view.*

class MiscItem(val misc: Misc, header: MiscItemHeader?) : AbstractSectionableItem<MiscItem.ViewHolder, MiscItemHeader>(header) {

    override fun hashCode(): Int {
        return misc.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Misc) {
            return this.misc == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.misc_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(misc)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(misc: Misc) {
            val context = itemView.context
            itemView.amount.text = misc.displayAmount(context)
            itemView.name.text = Wire.get(misc.name, Misc.DEFAULT_NAME)
            itemView.time.text = misc.displayTime(context)
            itemView.type.text = misc.displayType(context)
        }
    }
}