package beer.hapibrew.app.adapter.items.session

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import beer.hapibrew.app.R
import beer.hapibrew.app.data.SessionData
import beer.hapibrew.beerxml2proto.proto.Recipe
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.utils.DrawableUtils
import eu.davidea.viewholders.FlexibleViewHolder

abstract class SessionItem(val session: SessionData, header: SessionItemHeader?) : AbstractSectionableItem<SessionItem.ViewHolder, SessionItemHeader>(header) {

    override fun hashCode(): Int {
        return session.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Recipe) {
            return this.session == other
        }

        return false
    }


    override fun bindViewHolder(adapter: FlexibleAdapter<out IFlexible<*>>?, holder: ViewHolder?, position: Int, payloads: MutableList<Any?>?) {
        holder?.let {
            val ctx = holder.itemView.context
            holder.bindData(ctx, session)
            DrawableUtils.setBackgroundCompat(holder.itemView, DrawableUtils.getSelectableBackgroundCompat(
                    ContextCompat.getColor(ctx, R.color.card_recipe_background),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected)))
            holder.itemView.isActivated = adapter?.isSelected(position) ?: false
        }
    }

    abstract class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        abstract fun bindData(context: Context, session: SessionData)
    }
}