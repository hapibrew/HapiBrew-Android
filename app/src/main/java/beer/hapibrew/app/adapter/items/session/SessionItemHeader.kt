package beer.hapibrew.app.adapter.items.session

import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.session_header_item.view.*

class SessionItemHeader(val title: String) : AbstractHeaderItem<SessionItemHeader.ViewHolder>() {

    override fun equals(other: Any?): Boolean {
        if (other is String) {
            return this.title == other
        }

        return false
    }

    override fun hashCode(): Int {
        return title.hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.session_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(title)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(title: String) {
            with (itemView) {
                headerTitle.text = title
            }
        }
    }
}