package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.HopUse
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayUse
import beer.hapibrew.app.extensions.forceShortUnit
import beer.hapibrew.app.prefs
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.hop_list_header_item.view.*

class HopItemHeader(val hopUse: HopUse) : AbstractHeaderItem<HopItemHeader.ViewHolder>() {

    var showUse:Boolean = true

    constructor(hopUse: HopUse, showUse:Boolean) : this(hopUse) {
        this.showUse = showUse
    }

    override fun equals(other: Any?): Boolean {
        if (other is HopUse) {
            return this.hopUse == other
        }

        return false
    }

    override fun hashCode(): Int {
        return hopUse.ordinal
    }

    override fun getLayoutRes(): Int {
        return R.layout.hop_list_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(hopUse, showUse)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(hopUse: HopUse, showUse:Boolean) {
            with (itemView) {
                if (showUse) {
                    hopAddition.text = hopUse.displayUse(context)
                    hopAddition.visibility = View.VISIBLE
                } else {
                    hopAddition.visibility = View.GONE
                }

                amount.text = if (prefs.useMetric) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_grams))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_ounces))
                }

                time.text = if (hopUse.forceShortUnit()) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_minutes))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_days))
                }
            }
        }
    }
}