package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Recipe
import beer.hapibrew.app.R
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.data.RecipeData
import beer.hapibrew.app.extensions.toOneDecimalFormat
import beer.hapibrew.app.extensions.toSgFormat
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.utils.DrawableUtils
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.recipe_list_item.view.*

class RecipeItem(val recipe: RecipeData) : AbstractFlexibleItem<RecipeItem.ViewHolder>() {

    override fun hashCode(): Int {
        return recipe.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Recipe) {
            return this.recipe == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.recipe_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.let {
            val ctx = holder.itemView.context
            holder.bindData(ctx, recipe)
            DrawableUtils.setBackgroundCompat(holder.itemView, DrawableUtils.getSelectableBackgroundCompat(
                    ContextCompat.getColor(ctx, R.color.card_recipe_background),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected),
                    ContextCompat.getColor(ctx, R.color.card_recipe_selected)))
            holder.itemView.isActivated = adapter?.isSelected(position) ?: false
        }

    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(context: Context, recipe: RecipeData) {
            itemView.beerName.text = recipe.name
            itemView.beerOg.text = context.getString(R.string.beer_og, recipe.og.toSgFormat())
            itemView.beerFg.text = context.getString(R.string.beer_fg, recipe.fg.toSgFormat())
            itemView.beerIbu.text = context.getString(R.string.beer_ibu, recipe.ibu.toInt())
            itemView.beerAbv.text = context.getString(R.string.beer_abv, recipe.abv.toOneDecimalFormat())
            itemView.recipeGraph.setData(recipe.og, recipe.fg, recipe.ibu, recipe.abv)
        }
    }
}