package beer.hapibrew.app.adapter.items.session

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.R
import beer.hapibrew.app.data.SessionData
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.session_list_active_item.view.*
import org.jetbrains.anko.imageResource

class SessionActiveItem(session: SessionData, header: SessionItemHeader?) : SessionItem(session, header) {

    override fun getLayoutRes(): Int {
        return R.layout.session_list_active_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : SessionItem.ViewHolder(v, adapter) {
        override fun bindData(context: Context, session: SessionData) {
            itemView.sessionName.text = session.name
            itemView.sessionStepText.text = context.resources.getStringArray(R.array.session_phase)[session.phase.ordinal]
            if (session.stepPauseEpoch != 0L || session.stepStartEpoch == 0L) {
                itemView.sessionPausePlay.imageResource = R.drawable.ic_pause_circle_filled
            } else {
                itemView.sessionPausePlay.imageResource = R.drawable.ic_play_circle_filled
            }
            if (session.phase != SessionData.Phase.COMPLETED) {
                session.setTimer(context, false, itemView.sessionCountDown, null, null)
                itemView.sessionCountDown.visibility = View.VISIBLE
            } else {
                itemView.sessionCountDown.visibility = View.GONE
            }
        }
    }
}