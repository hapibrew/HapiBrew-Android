package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.prefs
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.mashstep_list_header_item.view.*

class MashStepItemHeader : AbstractHeaderItem<MashStepItemHeader.ViewHolder>() {

    override fun equals(other: Any?): Boolean {
        return this == other
    }

    override fun hashCode(): Int {
        return hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.mashstep_list_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData()
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData() {
            with (itemView) {
                temp.text = if (prefs.useMetric) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_celsius))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_fahrenheit))
                }
            }
        }
    }
}