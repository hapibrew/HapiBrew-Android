package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Fermentable
import beer.hapibrew.beerxml2proto.proto.Hop
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayAmount
import beer.hapibrew.app.extensions.displayForm
import beer.hapibrew.app.extensions.displayTime
import beer.hapibrew.app.extensions.setAlternateBackground
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.hop_list_item.view.*

class HopItem(val hop: Hop, header: HopItemHeader?) : AbstractSectionableItem<HopItem.ViewHolder, HopItemHeader>(header) {

    override fun hashCode(): Int {
        return hop.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Hop) {
            return this.hop == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.hop_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(hop)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(hop: Hop) {
            with(itemView) {
                amount.text = hop.displayAmount(context)
                name.text = Wire.get(hop.name, Fermentable.DEFAULT_NAME)
                time.text = hop.displayTime(context)
                form.text = hop.displayForm(context)
            }
        }
    }
}