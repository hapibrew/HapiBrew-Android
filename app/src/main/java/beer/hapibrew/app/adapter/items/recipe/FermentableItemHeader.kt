package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.prefs
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.fermentable_list_header_item.view.*

class FermentableItemHeader : AbstractHeaderItem<FermentableItemHeader.ViewHolder>() {

    override fun equals(other: Any?): Boolean {
        return this == other
    }

    override fun hashCode(): Int {
        return hashCode()
    }

    override fun getLayoutRes(): Int {
        return R.layout.fermentable_list_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData()
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData() {
            with (itemView) {
                amount.text = if (prefs.useMetric) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_kilograms))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_pounds))
                }
            }
        }
    }
}