package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Fermentable
import beer.hapibrew.app.R
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.calculators.lovibondToHex
import beer.hapibrew.app.extensions.displayAmount
import beer.hapibrew.app.extensions.setAlternateBackground
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.fermentable_list_item.view.*
import org.jetbrains.anko.backgroundColor

class FermentableItem(val fermentable: Fermentable, header: FermentableItemHeader?) : AbstractSectionableItem<FermentableItem.ViewHolder, FermentableItemHeader>(header) {

    override fun hashCode(): Int {
        return fermentable.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fermentable) {
            return this.fermentable == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.fermentable_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(fermentable)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(fermentable: Fermentable) {
            val context = itemView.context
            itemView.color.backgroundColor = Color.parseColor(lovibondToHex(Wire.get(fermentable.color, Fermentable.DEFAULT_COLOR)!!))
            itemView.name.text = Wire.get(fermentable.name, Fermentable.DEFAULT_NAME)
            itemView.amount.text = fermentable.displayAmount(context)
        }
    }
}