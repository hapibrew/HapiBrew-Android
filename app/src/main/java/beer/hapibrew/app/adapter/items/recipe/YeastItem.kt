package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.Yeast
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.*
import com.squareup.wire.Wire
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.yeast_list_item.view.*

class YeastItem(val yeast: Yeast, header: YeastItemHeader?) : AbstractSectionableItem<YeastItem.ViewHolder, YeastItemHeader>(header) {

    override fun hashCode(): Int {
        return yeast.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Yeast) {
            return this.yeast == other
        }

        return false
    }

    override fun getLayoutRes(): Int {
        return R.layout.yeast_list_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(yeast)
        holder?.itemView?.setAlternateBackground(position)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(yeast: Yeast) {
            val context = itemView.context
            itemView.name.text = yeast.fullName()
            itemView.attenuation.text = Wire.get(yeast.attenuation, Yeast.DEFAULT_ATTENUATION)!!.toOneDecimalFormat()
            itemView.type.text = yeast.displayType(context)
            itemView.form.text = yeast.displayForm(context)
        }
    }
}