package beer.hapibrew.app.adapter.items.recipe

import beer.hapibrew.beerxml2proto.proto.MiscUse
import beer.hapibrew.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import beer.hapibrew.app.extensions.displayUse
import beer.hapibrew.app.extensions.forceShortUnit
import beer.hapibrew.app.prefs
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.misc_list_header_item.view.*

class MiscItemHeader(val use: MiscUse) : AbstractHeaderItem<MiscItemHeader.ViewHolder>() {

    override fun equals(other: Any?): Boolean {
        if (other is MiscUse) {
            return this.use == other
        }

        return false
    }

    override fun hashCode(): Int {
        return use.ordinal
    }

    override fun getLayoutRes(): Int {
        return R.layout.misc_list_header_item
    }

    override fun createViewHolder(adapter: FlexibleAdapter<*>?, inflater: LayoutInflater?, parent: ViewGroup?): ViewHolder {
        return ViewHolder(inflater!!.inflate(layoutRes, parent, false), adapter!!)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<*>?, holder: ViewHolder?, position: Int, payloads: List<*>?) {
        holder?.bindData(use)
    }

    class ViewHolder(v: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(v, adapter) {
        fun bindData(use: MiscUse) {
            with (itemView) {
                miscAddition.text = use.displayUse(context)

                amount.text = if (prefs.useMetric) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_grams))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_ounces))
                }

                time.text = if (use.forceShortUnit()) {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_minutes))
                } else {
                    context.getString(R.string.header_one_line_unit, context.getString(R.string.unit_short_days))
                }
            }
        }
    }
}